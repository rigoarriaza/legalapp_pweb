<?php

$currentUser = null;
date_default_timezone_set('America/La_Paz'); // CDT

if(!isset($_SESSION)){
	session_start();
}

$domainName = $_SERVER['SERVER_NAME'];
$timezone = "America/La_Paz";

// echo $domainName;
if($domainName == "dev.legalbo.com"){
	$page_url = '';
	$pi1 = 1;
}else{
	$page_url = '/proyectos/LegalApp/web';
	$pi1 = 4;
}

$imageUrl = "http://legalbo.com/legalapp/IMGUSERS/" ;

$_SESSION["lBo"]["page_url"] = $page_url;

if(isset($_SESSION["lBo"]["currentUserID"])){
	$userType	 = isset($_SESSION['lBo']['userType']) ? $_SESSION['lBo']['userType'] : null;
	$userData 	 = isset($_SESSION['lBo']['u_Data']) ? $_SESSION['lBo']['u_Data'] : null;
	if($userData->u_foto == ""){
		$userData->u_foto = 'default_user.png';
	}
	$currentUser = $userData->token_session;


	$current_user = (object) [  
	"id"=> $currentUser,
	"db_id" => (isset($userData->iduserapp)) ? $userData->iduserapp : $userData->idprofesional,
	"photo"=> $page_url . '/assets/default_user.png',
	"name"=> $userData->u_nombre . " " . $userData->u_apellido,
	"userType" => $userType,
	"saldo" => '0',
	"userData"=> $userData];

	// print_r($current_user);
}else{
	$currentUser = null;
	// header("Location: ../pages/login.php");
}

// echo $userData ;

$page_links = (object)
[
	"normal"=> (object) [
		array("page" => "home" , "url" => "$page_url/" , "name"=>"Inicio", "icon"=>"home"),
		array("page" => "history" , "url" => "$page_url/history" , "name"=>"Historial", "icon"=>"clock-o"),
		// array("page" => "notifications" , "url" => "$page_url/notifications" , "name"=>"Notificaciones", "icon"=>"data", "notyBox"=>true),
		array("page" => "user" , "url" => "$page_url/user/me" , "name"=>"Mi perfil", "icon"=>"user", "submenu" => (object) ["page"=> "logout", "url" => "$page_url/logout" , "name"=>"Cerrar Sesión"] )
	],
	"legal"=> (object) [
		array("page" => "home" , "url" => "$page_url/" , "name"=>"Inicio", "icon"=>"home"),
		array("page" => "cases" , "url" => "$page_url/cases" , "name"=>"Privadas", "icon"=>"book"),
		array("page" => "history" , "url" => "$page_url/history" , "name"=>"Historial", "icon"=>"clock-o"),
		// array("page" => "notifications" , "url" => "$page_url/notifications" , "name"=>"Notificaciones", "icon"=>"data", "notyBox"=>true),
		array("page" => "user" , "url" => "$page_url/user/me" , "name"=>"Mi perfil", "icon"=>"user", "submenu" => (object) ["page"=> "logout", "url" => "$page_url/logout" , "name"=>"Cerrar Sesión"] )
	]
];

$css_page_links = array(
    (object)["page" => "home"],
    (object)["page" => "abogado"],
    (object)["page" => "cases"],
    (object)["page" => "user"],
    (object)["page" => "notifications"],
    (object)["page" => "history"],
);

$user_side_menu = (object)
[
	"normal"=> (object) [
		array("page"=> "quotations", "url"=> "$page_url/quotations", "name" => "Mis Cotizaciones", "icon" => "list-alt"),
		array("page"=> "favorites", "url"=> "$page_url/favorites", "name" => "Profesionales Favoritos", "icon" => "star"),
		array("page"=> "favorites", "url"=> "$page_url/favorites", "name" => "Profesionales Favoritos", "icon" => "star"),
		array("page"=> "map", "url"=> "$page_url/map", "name" => "Mapa", "icon" => "map-marker"),
		array("page"=> "pasante", "url"=> "$page_url/pasante", "name" => "Pansantia", "icon" => "map-marker"),
		array("page"=> "terminos", "url"=> "$page_url/terminos", "name" => "Terminos", "icon" => "map-marker")
		
	],
	"legal"=> (object) [
		array("page"=> "quotations", "url"=> "$page_url/quotations", "name" => "Cotizaciones", "icon" => "list-alt"),
		array("page"=> "examenes", "url"=> "$page_url/examenes", "name" => "Ex&aacute;menes", "icon" => "file"),
		array("page"=> "files", "url"=> "$page_url/files", "name" => "Archivos", "icon" => "paperclip"),
		array("page"=> "internships", "url"=> "$page_url/internships", "name" => "Pasantias", "icon" => "graduation-cap")
	]
];

$colors = (object) ["blue" => "#636e72", //051D4D",
					"blue2"=> "#3668C4", 
					"bghead" => "#95999a",
					"white" => "#FEFEFE",
					"grey"=> "#f0f1f4", //EDEBEC
					"darkblue" => "#203236",
					"red" => "#C1262E",
					"gold"=> "#ffd700",
					"silver"=> "#D3D3D3",
					"bronze"=> "#6C541E"];
					
$linkColor  = "#555555";
// $imageUrl   = "/resources/img/";
$pageWidth  = "980px";
?>