          
          <div class="list-group">
              <a class="card" href="#">
                  <div class="row no-gutters">
                      <div class="col-auto">
                          <img style="width: 80px !important; height: 80px !important;" src="<?=$imageUrl ?>/<?=$userData->u_foto; ?>"  class="img-fluid imglist"  alt="">
                      </div>
                      <div class="col" >
                          <div class="card-block px-2">

                              <h4 class="title-list"><?=$userData->u_nombre; ?> <?=$userData->u_apellido; ?><label class="detail-list"> </label></h4>
                                
                          </div>
                      </div>
                  </div>
                  
              </a>
          </div>
          
  
  <?php if( $current_user->userType == 1){ ?>
          
          <div class="list-group menu-side">
            <a href="<?= $page_url ?>/favoritos" class="list-group-item <?php echo $activemenu = ($page_name == 'favoritos') ? 'active': ' '; ?>">
              <i class="fa fa-heart"></i> Abogados Favoritos</a>
            <a href="<?= $page_url ?>/preguntas" class="list-group-item <?php echo $activemenu = ($page_name == 'preguntas') ? 'active': ' '; ?>">
              <i class="fa fa-comment"></i> Mis Preguntas
            </a>
            
            <a href="<?= $page_url ?>/cuenta" class="list-group-item <?php echo $activemenu = ($page_name == 'cuenta') ? 'active': ' '; ?>">
              <i class="fa fa-credit-card"></i> Mi cuenta
            </a>
            <a href="<?= $page_url ?>/mapa" class="list-group-item <?php echo $activemenu = ($page_name == 'mapa') ? 'active': ' '; ?>">
              <i class="fa fa-map-marker"></i> Notarías
            </a>
            <a href="<?= $page_url ?>/pasantia" class="list-group-item <?php echo $activemenu = ($page_name == 'pasantia') ? 'active': ' '; ?>">
              <i class="fa fa-graduation-cap"></i> Pasantías
            </a>
            <a href="<?= $page_url ?>/terminos" target="_blank" class="list-group-item <?php echo $activemenu = ($page_name == 'terminos') ? 'active': ' '; ?>">
              <i class="fa fa-book"></i> Términos y condiciones
            </a>
          </div>

  <?php } else if( $current_user->userType == 2){ ?>

          <div class="list-group menu-side">
            <a href="<?= $page_url ?>/casos" class="list-group-item <?php echo $activemenu = ($page_name == 'casos') ? 'active': ' '; ?>">
              <i class="fa fa-comment"></i> Mis Preguntas 
            </a>
            <a href="<?= $page_url ?>/archivos" class="list-group-item <?php echo $activemenu = ($page_name == 'archivos') ? 'active': ' '; ?>">
              <i class="fa fa-file"></i> Archivos</a>
            
            <a href="<?= $page_url ?>/examenes" class="list-group-item <?php echo $activemenu = ($page_name == 'examenes') ? 'active': ' '; ?>">
              <i class="fa fa-search"></i> Examenes</a>
            
            <a href="<?= $page_url ?>/wallet" class="list-group-item <?php echo $activemenu = ($page_name == 'wallet') ? 'active': ' '; ?>">
              <i class="fa fa-credit-card"></i> Mi cuenta
            </a>
            <a href="<?= $page_url ?>/mapa" class="list-group-item <?php echo $activemenu = ($page_name == 'mapa') ? 'active': ' '; ?>">
              <i class="fa fa-map-marker"></i> Notarías
            </a>
            <a href="<?= $page_url ?>/pasantia" class="list-group-item <?php echo $activemenu = ($page_name == 'pasantia') ? 'active': ' '; ?>">
              <i class="fa fa-graduation-cap"></i> Pasantías
            </a>
            <a href="<?= $page_url ?>/terminos" target="_blank" class="list-group-item <?php echo $activemenu = ($page_name == 'terminos') ? 'active': ' '; ?>">
              <i class="fa fa-book"></i> Términos y condiciones
            </a>
          </div>
  <?php }  ?>