<?php
if ( ! defined("elementPATH")) {
    $elementPATH = realpath(__DIR__ . '/..');
    define("elementPATH", $elementPATH);
}
// include_once(elementPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "template.php");
// $metatags = new Template(elementPATH . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR . "metatags.tpl");

// $metatags = ( object );
// $metatags->set("title",         $page_data->title);
// $metatags->set("description",   $page_data->description);
// $metatags->set("url",           $page_data->url);
// $metatags->set("image",         $page_data->image);
// $metatags->set("autor",         $page_data->autor);

// echo $metatags->output();

?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--<meta name="viewport" content="width=device-width, initial-scale=1.0">    -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="<?= $page_data->description ?>">
<meta name="author" content="<?= $page_data->autor ?>">

<TITLE><?=$page_data->title?></TITLE>

<meta property="og:title"       content="<?=$page_data->title?>" />
<meta property="og:image"       content="<?=$page_data->image?>" />
<meta property="og:description" content="<?=$page_data->description?>" />
<meta property="og:url"         content="<?=$page_data->url?>" />