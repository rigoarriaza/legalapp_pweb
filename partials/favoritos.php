<?php include_once("head.php"); ?>

<?php include_once("navbar.php"); ?>


<?php
    if ( ! isset($elementPATH)) {
        $elementPATH = realpath(__DIR__ . '/..');
        define("elementPATH", $elementPATH);
    }
    
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . '/../../actions');
		define("actionPATH", $actionPATH);
	}

	if(!isset($_SESSION)){
		session_start();
	}
	
	include_once(actionPATH . DIRECTORY_SEPARATOR . "a_getdata.php");


  $h_members_list_a = getServiceData('getAbogadosFavorito', $userData->token_session, $userData->iduserapp, (array)[0,10]);
	
  if($h_members_list_a != 'no_data'){
    if($h_members_list_a != false && count($h_members_list_a) > 0 ){
      foreach ($h_members_list_a as $key => $h_member_a) {
        $m_photo = (empty($h_member_a->p_foto)) ? $page_url . '/assets/default_user.png' : $h_member_a->p_foto;
        $member_a = 	 array ("name" => $h_member_a->p_nombre . " " . $h_member_a->p_apellido,
                    "rate"=> $h_member_a->calificacion,
                    "user_url"=> "$page_url/abogado/" . $h_member_a->idprofesional,
                    "photo"=> getUserPhoto($m_photo)
                                      );
        $members_a->lawyer[] = (object) $member_a;
      }
    }
  }


?>


    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <!-- Sidebar Widgets Column -->
        <div class="col col-md-4">

          <?php include_once("sidemenu.php"); ?>

        </div>

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          
          

          <h3 class="subtitulo">Listado de abogados</h3>

           <div class="list-group">

          <?php

          if(isset($h_members_list_a) && $h_members_list_a == 'no_data'){?>

            <div class="error" >
              <label > <i class="fa fa-search"></i> No hay favoritos</label>
            </div>

          <?	
          }else if(isset($members_a->lawyer) && count($members_a->lawyer) > 0){
            foreach ($members_a->lawyer as $member_a) {
              
            
              foreach ($member_a as $key => $value) {
                  // print "$key => $value\n";
                  
                
                if($key == "name"){
                  $nombre = $value;
                }
                if($key == "rate"){
                  $rate = $value;
                }if($key == "user_url"){
                  $url = $value;
                }if($key == "photo"){
                  $foto = $value;
                }
              }
              ?>
              <a class="card" href="<?=$url; ?>">
                  <div class="row no-gutters">
                      <div class="col-auto">
                          <img src="<?=$foto; ?>" style="width:75px !important; height:75px !important;" class="img-fluid imglist"  alt="">
                      </div>
                      <div class="col">
                          <div class="card-block px-2">

                              <h4 class="title-list">
                             
                                <?=$nombre; ?> <label class="detail-list"><i class="fa fa-star"></i> <?=$rate; ?></label></h4>
                              
                              <p class="card-text"></p>
                          </div>
                      </div>
                  </div>
                 
              </a>


               
              <?
            }
          }

          ?>   

           
          </div>

          

        </div>


      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include_once("foot.php"); ?>

    <!-- Bootstrap core JavaScript -->
    <!--<script src="vendor/jquery/jquery.min.js"></script>-->
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script type="text/javascript">


  </script>


  </body>

</html>
