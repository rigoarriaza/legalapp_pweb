<?php include_once("head.php"); ?>

<?php include_once("navbar.php"); ?>

<?php
    if ( ! isset($elementPATH)) {
        $elementPATH = realpath(__DIR__ . '/..');
        define("elementPATH", $elementPATH);
    }
    
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . '/../../actions');
		define("actionPATH", $actionPATH);
	}

	if(!isset($_SESSION)){
		session_start();
	}
	
	include_once(actionPATH . DIRECTORY_SEPARATOR . "a_getdata.php");

    $especialidades = getServiceDataCommun('getEspecialidades', (array)[0,20]);

    $user = getServiceDataCommun('getInformacionProfesionalApp', (array)[$userData->idprofesional, $userData->token_session]);
	
	// var_dump($user);


  // var_dump($especialidades);

?>


    
    <!-- Page Content -->
    <div class="container">


      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          
          
          <div class="list-group">
              <a class="card" href="#">
                  <div class="row no-gutters">
                      <div class="col-4">
                          <img id="fperfil" style="width: 150px !important; height: 150px !important;" src="<?=$imageUrl ?>/<?=$userData->u_foto; ?>" class="img-fluid imglist"  alt="">
                          <br>
                          <label for="fileToUpload" class="btn" style="color:grey;"><i class="fa fa-camera"></i> Actualizar foto</label>
                          <input onchange="updateFoto()" name="fileToUpload" id="fileToUpload" style="visibility:hidden;" type="file">
                        
                      </div>
                      <div class="col-8" >
                          <div class="card-block px-2">

                              <h4 class="subtitulo"><?=$userData->u_nombre; ?> <?=$userData->u_apellido; ?><label class="detail-list"> </label></h4>
                                <?=$user->u_descripcion ?>
                          </div>
                      </div>
                  </div>
                  <div class="card-footer w-100 text-muted" >
                    <div class="row" style="text-align:center;">
                      <div class="col-6"><button class="btn lgapp-secondary btnlg-s" type="button"><i class="fa fa-star"></i> <?=$user->u_calificacion ?> </button> Calificación</div>
                      <div class="col-6"><button class="btn lgapp-secondary btnlg-s" type="button"><?=$user->u_cconsulta ?> </button> Respuestas</div>
                      
                    </div>
                  </div>
              </a>
          </div>



          

                                    <form id="update-form"  novalidate="novalidate">

                                        <h3 class="subtitulo">Datos Personales </h3>
                                        <input class="legal-input" id="u_nombre" name="u_nombre" placeholder="Nombre" value="<?=$userData->u_nombre; ?>"/>
                                        <br>
                                        <input class="legal-input"  id="u_apellido" name="u_apellido" placeholder="Apellido" value="<?=$userData->u_apellido; ?>"/>
                                        <br>
                                        <input  class="legal-input" id="u_celular" name="u_celular" placeholder="Celular"  value="<?=$userData->u_celular; ?>"/>
                                        <br>
                                        <input class="legal-input"  id="u_carnet" name="u_carnet" placeholder="carnet"  value="<?=$userData->u_carnet; ?>" />
                                        <br><br>
                                    
                                        
                                        <h3 class="subtitulo">Datos Profesionales </h3>
                                        
                                        <!--<label for="fileToUpload" class="btn" style="color:grey;"><i class="fa fa-camera"></i> </label>-->
                                        <img id="fregistro" style="width: 150px !important; height: 150px !important;" src="<?= $page_url ?>/assets/tour0.jpg" class="img-fluid imglist"  alt="">
                                        <label for="fileToUploadReg" class="btn" style="color:grey;"><i class="fa fa-camera"></i> Imagen de tu matrícula de registro</label>
                                        
                                        <input  class="legal-input" id="u_numero_registro" name="u_numero_registro" placeholder="Numero de registro"  value="<?=$user->u_numero_registro; ?>"/>
                                        <br>
                                        <input  class="legal-input" id="u_valorpregunta" name="u_valorpregunta" placeholder="Valor de sus servicios"  value="<?=$user->u_valorpregunta; ?>"/>
                                        <br>
                                        <textarea  class="legal-input" maxlength="500" id="u_descripcion" name="u_descripcion" placeholder="Ingresa el detalle de tu perfil y tus servicios (500 carateres)"  ><?=$user->u_descripcion; ?></textarea>
                                        <br>
                                        
                                        <input onchange="updateFotoReg()" name="fileToUploadReg" id="fileToUploadReg" style="visibility:hidden;" type="file">
                                        


                                        <button onClick="update();" class="btn lgapp-secondary" type="button"><i class="fa fa-edit"></i> Actualizar </button> 
                                        <br>

                                    
                                    </form>


                 


        

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col col-md-4">

          


          <div class="list-group menu-side">
           <h5 class="card-header">Suscripción</h5>
                                    <!--<label> Suscripción </label>-->
                                    <div class="">
                                        <div class="list-group menu-side">
                                            <a href="#" class="list-group-item ">
                                                Suscripción: <?=$user->user_plan->plan ?>  
                                                <?php if($user->user_plan->plan == "FREE"){ ?>
                                                    <button onClick="planes()" class="btn  lgapp-secondary btn-sm" >Ver Planes</button>
                                                <?php } ?>
                                            </a>
                                            <a href="#" class="list-group-item ">
                                                Fecha de Compra: <?=(isset($user->user_plan->fecha_compra))? formatDate($user->user_plan->fecha_compra) : '-'; ?>
                                            </a>
                                            <a href="#" class="list-group-item ">
                                                Fecha de vencimiento: <?=(isset($user->user_plan->fecha_expiracion))? formatDate($user->user_plan->fecha_expiracion) : '-'; ?>
                                            </a>
                                            
                                        
                                        </div> 
          </div>

          <br>
          <div class="list-group menu-side">
           <h5 class="card-header">Especialidades</h5>
                                    <!--<label> Especialidades </label>-->
                                    <div class="panel-body">
                                        <form id="esp-form"  novalidate="novalidate">
                                        <div class="list-group menu-side">
                                        <?php
                                            foreach ($especialidades as $key => $h_esp) {
                                                $selected = "";
                                                if(count($user->user_especialidad)>0){
                                                    foreach($user->user_especialidad as $key => $id){
                                                        if($h_esp->id_especialidad == $id->id){
                                                            $selected = "checked='checked'";
                                                            break;
                                                        }
                                                    }
                                                }
                                                ?>
                                                <div class="list-group-item ">
                                                    <input type="checkbox" name="esp[]" <?=$selected?>  id="chk<?=$h_esp->id_especialidad; ?>" value="<?=$h_esp->id_especialidad; ?>" <?=(isset($user->user_plan->id))? '' : 'disabled' ;?> /> <?=$h_esp->e_nombre ?>
                                                </div>
                                            
                                        <?    
                                            }
                                        ?> 
                                        </div> 
                                        </form>
                                    </div> 
          </div>
          <br>
          <div class="row">
              <div class="col-12">
                <button id="btn-favorito" class="btn-lgapp-secondary  btn btn-block " type="button" onClick="setClave()">
                  <i class="fa fa-lock "></i> Cambia tu contraseña</button>
              </div>
                      
          </div>
          

        </div>

      </div>
      <!-- /.row -->

    </div>

      
        
      </div>


    <!-- Footer -->
    <?php include_once("foot.php"); ?>

  <script type="text/javascript">
    const scope_url = "<?php echo $page_url; ?>";
 $(document).ready(function() {
    $("#pago").hide();

    var textarea = document.querySelector('textarea');

    textarea.addEventListener('keydown', autosize);
                
    function autosize(){
    var el = this;
    setTimeout(function(){
        el.style.cssText = 'height:auto; padding:0';
        // for box-sizing other than "content-box" use:
        // el.style.cssText = '-moz-box-sizing:content-box';
        el.style.cssText = 'height:' + el.scrollHeight + 'px';
    },0);
    }

 });
    function pagoshow(){
        $("#pago").show();
        $("#btnpago").hide();
        
    }

	function update(){

        var form = $("#update-form").serializeArray();
		var fesp = $("#esp-form").serializeArray();
		
        // for (var index = 0; index < fesp.length; index++) {
        //     item = {
        //         especialidad
        //     }
        // }
        
    	var datos = objectifyForm(form);
		var edatos = objectifyForm(fesp);
        console.log(fesp);
		$.ajax({
			url: scope_url + "/actions/a_update.php",
			type:'POST',
            data: { data: JSON.stringify(datos), tipo: 2, esp: JSON.stringify(fesp) },
            success: function(data){
				console.log(data);
				if (data === "OK") {
					window.location.replace(scope_url + "/profesional");
				}else{
					var errormsj= errorMessage(data);
					$.toast({
						heading: 'Error',
						text: '<span style="font-size:18px;"></span> '+errormsj,
						position: 'top-center',
						loaderBg: '#C1262E',
						bgColor: 'white',
    					textColor: 'grey',
						stack: false,
						hideAfter: 6000
					});	
				}
            }

		});
    }     
	    
	function updateFoto(){

        // var form = $("#foto-form").serializeArray();
		// var base64 = getBase64Image(document.getElementById("imageid"));
		// var base64 = getBase64Image($("#files").val());
        // var ruta = $("#files").val();
    	
        // console.log(base64);
        var preview = document.getElementById("fperfil");
        var file    = document.querySelector('input[type=file]').files[0];
        var reader  = new FileReader();

        reader.addEventListener("load", function () {
            preview.src = reader.result;
            setFoto(reader.result);
        }, false);

        if (file) {
            reader.readAsDataURL(file);

            // console.log(datos);
        }


    }

    function setFoto(img){
        $.ajax({
			url: scope_url + "/actions/a_foto.php",
			type:'POST',
            data: { foto: img , tipo: 2 },
            success: function(data){
				console.log(data);
				if (data === "OK") {
					window.location.replace(scope_url + "/profesional");
				}else{
					var errormsj= errorMessage(data);
					$.toast({
						heading: 'Error',
						text: '<span style="font-size:18px;"></span> '+errormsj,
						position: 'bottom-center',
						loaderBg: '#C1262E',
						bgColor: 'white',
    					textColor: 'grey',
						stack: false,
						hideAfter: 6000
					});	
				}
            }

		});
    }	    
	function updateFotoReg(){

        var preview = document.getElementById("fregistro");
        var file    = document.querySelector('#fileToUploadReg').files[0];
        var reader  = new FileReader();

    
        reader.addEventListener("load", function () {
            preview.src = reader.result;
            setFotoReg(reader.result);
        }, false);

        if (file) {
            reader.readAsDataURL(file);

            console.log(datos);
        }


    }

    function setFotoReg(img){
        $.ajax({
			url: scope_url + "/actions/a_foto.php",
			type:'POST',
            data: { foto: img , tipo: 3 },
            success: function(data){
				console.log(data);
				if (data === "OK") {
					// window.location.replace(scope_url + "/profesional");
				}else{
					var errormsj= errorMessage(data);
					$.toast({
						heading: 'Error',
						text: '<span style="font-size:18px;"></span> '+errormsj,
						position: 'bottom-center',
						loaderBg: '#C1262E',
						bgColor: 'white',
    					textColor: 'grey',
						stack: false,
						hideAfter: 6000
					});	
				}
            }

		});
    }

    function setClave(){

		$.confirm({
			title: 'Cambiar contraseña',
			content: '' +
			'<form action="" class="formClave">' +
			'<div class="form-group">' +
			'<input type="password" placeholder="Nueva Contraseña" class="pass form-control" required /><br>' +
			'<input type="password" placeholder="Confirmar Contraseña" class="pass2 form-control" required />' +
			'</div>' +
			'</form>',
			buttons: {
				formSubmit: {
					text: 'Enviar',
					btnClass: 'lgapp-secondary',
					action: function () {
						var pass = this.$content.find('.pass').val();
						var pass2 = this.$content.find('.pass2').val();
						if(!pass || !pass2){
							$.alert('Ingresa ambos campos.');
							return false;
						}else if(pass != pass2){
							$.alert('Las contraseñas no coinciden');
							return false;
						}else{
							$.ajax({
								url: scope_url + "/actions/a_passchange.php",
								type:'POST',
								data: { password: pass, tipo:2 },
								success: function(data){
									console.log(data);
									if (data === "OK") {
										$.toast({
											heading: 'Contraseña actualizada',
											text: "La contraseña se cambio correctamente.",
											position: 'top-center',
											loaderBg: '#C1262E',
											bgColor: 'white',
											textColor: 'grey',
											stack: false,
											hideAfter: 6000
										});	
									}else{
										var errormsj= errorMessage(data);
										$.toast({
											heading: 'Error',
											text: '<span style="font-size:18px;"></span> '+errormsj,
											position: 'bottom-center',
											loaderBg: '#C1262E',
											bgColor: 'white',
											textColor: 'grey',
											stack: false,
											hideAfter: 6000
										});	
									}
								}
							});	
						}		
					}
				},
				cancel: {
					text: 'Cancelar',
					function () {
					//close
				},
				}
			},
			onContentReady: function () {
				// bind to events
				var jc = this;
				this.$content.find('form').on('submit', function (e) {
					// if the user submits the form by pressing enter in the field.
					e.preventDefault();
					jc.$$formSubmit.trigger('click'); // reference the button and click it
				});
			}
		});
    } 

    function planes(){
        window.location.replace(scope_url + "/planes");
    }

  </script>

  </body>

</html>
