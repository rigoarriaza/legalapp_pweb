<?php include_once("head.php"); ?>

<?php include_once("navbar.php"); ?>


<?php
    if ( ! isset($elementPATH)) {
        $elementPATH = realpath(__DIR__ . '/..');
        define("elementPATH", $elementPATH);
    }
    
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . '/../../actions');
		define("actionPATH", $actionPATH);
	}

	if(!isset($_SESSION)){
		session_start();
	}
	
	include_once(actionPATH . DIRECTORY_SEPARATOR . "a_getdata.php");

    //get Preguntas express
    $h_members_list_a = getServiceData('getHistorialPreguntasContestadas', $userData->token_session, $userData->idprofesional, (array)[0,10]);
	
	if($h_members_list_a != false && count($h_members_list_a) > 0 && $h_members_list_a != "no_data"){
		foreach ($h_members_list_a as $key => $h_member_a) {
			$member_a = 	 array (
                                 	"estado"=> getEstado($h_member_a->p_estado),
									"estadoclase"=> getEstadoClase($h_member_a->p_estado),
									"titulo"=> $h_member_a->p_titulo,
									"caso_url"=> "$page_url/caso/" . $h_member_a->idpreguntae,
									"fecha"=> formatDate($h_member_a->p_fecha)
                                    );
			$members_a->lawyer[] = (object) $member_a;
		}
	}

    //get Preguntas directas
    $list_directas = getServiceData('getHistorialPreguntasDirectas', $userData->token_session, $userData->idprofesional, (array)[0,5]);
	
	if($list_directas != false && count($list_directas) > 0  && $list_directas != "no_data"){
		foreach ($list_directas as $key => $h_member) {
			$pregunta = 	 array (
                                 	"estado"=> getEstado($h_member->p_estado),
									"estadoclase"=> getEstadoClase($h_member->p_estado),
									"titulo"=> $h_member->p_titulo,
									"caso_url"=> "$page_url/caso/" . $h_member->idpreguntad,
									"fecha"=> formatDate($h_member->p_datetime)
                                    );
			$preguntas_dir->preg[] = (object) $pregunta;
		}
	}

    //get Preguntas cotizaciones
    $list_cotizaciones = getServiceData('getHistorialdeCotizacionesProfesional', $userData->token_session, $userData->idprofesional, (array)[0,5]);
	
	if($list_cotizaciones != false && count($list_cotizaciones) > 0  && $list_cotizaciones != "no_data"){
		foreach ($list_cotizaciones as $key => $h_member) {
			$pregunta = 	 array (
                                 	"estado"=> getEstadoCot($h_member->c_estado),
									"estadoclase"=> getEstadoClaseCot($h_member->c_estado),
									"titulo"=> $h_member->c_titulo,
									"caso_url"=> "$page_url/caso/" . $h_member->idpreguntad,
									"fecha"=> formatDate($h_member->c_datetime)
                                    );
			$preguntas_cot->preg[] = (object) $pregunta;
		}
	}

    // var_dump($list_directas);



?>


    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <!-- Sidebar Widgets Column -->
        <div class="col col-md-4">



          <?php include_once("sidemenu.php"); ?>

        </div>

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          
  <!--show express       -->

          <h3 class="subtitulo">Preguntas Express</h3>

           <div class="list-group" id="result_express">

            <?php

            
            if(isset($h_members_list_a) && $h_members_list_a== 'no_data'){?>

				<div class="error" >
					<label > <i class="fa fa-search"></i> No hay datos</label>
				</div>

			<?	
			}else if(isset($members_a->lawyer) && count($members_a->lawyer) > 0){
                foreach ($members_a->lawyer as $member_a) {      
                
                foreach ($member_a as $key => $value) {
                    // print "$key => $value\n";
                    
                    
                    if($key == "titulo"){
                        $titulo = $value;
                    }
                    if($key == "estado"){
                      $estado = $value;
                    }if($key == "caso_url"){
                      $caso = $value;
                    }if($key == "fecha"){
                      $fecha = $value;
                    }if($key == "estadoclase"){
                      $estadoclase = $value;
                    }
                }
                ?>
                <a class="card" href="<?=$caso; ?>/1">
                    <div class="row no-gutters">
                        <!--<div class="col-auto">
                            <img src="" style="width:75px !important; height:75px !important;" class="img-fluid imglist"  alt="">
                        </div>-->
                        <div class="col">
                            <div class="card-block px-2">
                                <div class="row">
                                    <div class="col-8">
                                        <h5 class="title-list"><?=$titulo; ?> </h5>
                                        <label class="detail-list"><?=$fecha; ?></label>
                                    </div>
                                    <div class="col-4" style="padding: 5px;">
                                        <button type="button" class="btn btn-sm btnlg-l <?=$estadoclase?>">
													<?=$estado; ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </a>


                
                <?
                }

                
            }

            ?>      

          </div>

          <?php if(isset($members_a->lawyer) && count($members_a->lawyer) > 0){ ?>
          <button  id="load" class="btn btn-lgapp-primary"><i class="fa fa-plus"></i> Cargar más</button>
          <input type="hidden" id="result_no" value="5">
          <? } ?>

        
          
  <!--show Directas       -->

          <h3 class="subtitulo">Preguntas Directas</h3>

           <div class="list-group" id="result_directa">

            <?php

            
            if(isset($list_directas) && $list_directas== 'no_data'){?>

				<div class="error" >
					<label > <i class="fa fa-search"></i> No hay datos</label>
				</div>

			<?	
			}else if(isset($preguntas_dir->preg) && count($preguntas_dir->preg) > 0){
                foreach ($preguntas_dir->preg as $member_a) {      
                
                foreach ($member_a as $key => $value) {
                    // print "$key => $value\n";
                    
                    
                    if($key == "titulo"){
                        $titulo = $value;
                    }
                    if($key == "estado"){
                      $estado = $value;
                    }if($key == "caso_url"){
                      $caso = $value;
                    }if($key == "fecha"){
                      $fecha = $value;
                    }if($key == "estadoclase"){
                      $estadoclase = $value;
                    }
                }
                ?>
                <a class="card" href="<?=$caso; ?>/2">
                    <div class="row no-gutters">
                        <!--<div class="col-auto">
                            <img src="" style="width:75px !important; height:75px !important;" class="img-fluid imglist"  alt="">
                        </div>-->
                        <div class="col">
                            <div class="card-block px-2">
                                <div class="row">
                                    <div class="col-8">
                                        <h5 class="title-list"><?=$titulo; ?> </h5>
                                        <label class="detail-list"><?=$fecha; ?></label>
                                    </div>
                                    <div class="col-4" style="padding: 5px;">
                                        <button type="button" class="btn btn-sm btnlg-l <?=$estadoclase?>">
													<?=$estado; ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </a>


                
                <?
                }

                
            }

            ?>      

          </div>

          <?php if(isset($preguntas_dir->preg) && count($preguntas_dir->preg) > 0){ ?>
          <button  id="load_dir" class="btn btn-lgapp-primary"><i class="fa fa-plus"></i> Cargar más</button>
          <input type="hidden" id="result_dir_no" value="5">
          <? } ?>        
          
  <!--show Cotizaciones       -->

          <h3 class="subtitulo">Cotizaciones</h3>

           <div class="list-group" id="result_cotizacion">

            <?php

            
            if(isset($list_cotizaciones) && $list_cotizaciones== 'no_data'){?>

				<div class="error" >
					<label > <i class="fa fa-search"></i> No hay datos</label>
				</div>

			<?	
			}else if(isset($preguntas_cot->preg) && count($preguntas_cot->preg) > 0){
                foreach ($preguntas_cot->preg as $member_a) {      
                
                foreach ($member_a as $key => $value) {
                    // print "$key => $value\n";
                    
                    
                    if($key == "titulo"){
                        $titulo = $value;
                    }
                    if($key == "estado"){
                      $estado = $value;
                    }if($key == "caso_url"){
                      $caso = $value;
                    }if($key == "fecha"){
                      $fecha = $value;
                    }if($key == "estadoclase"){
                      $estadoclase = $value;
                    }
                }
                ?>
                <a class="card" href="<?=$caso; ?>/3">
                    <div class="row no-gutters">
                        <!--<div class="col-auto">
                            <img src="" style="width:75px !important; height:75px !important;" class="img-fluid imglist"  alt="">
                        </div>-->
                        <div class="col">
                            <div class="card-block px-2">
                                <div class="row">
                                    <div class="col-8">
                                        <h5 class="title-list"><?=$titulo; ?> </h5>
                                        <label class="detail-list"><?=$fecha; ?></label>
                                    </div>
                                    <div class="col-4" style="padding: 5px;">
                                        <button type="button" class="btn btn-sm btnlg-l <?=$estadoclase?>">
													<?=$estado; ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </a>


                
                <?
                }

                
            }

            ?>      

          </div>

          <?php if(isset($preguntas_dir->preg) && count($preguntas_dir->preg) > 0){ ?>
          <button  id="load_dir" class="btn btn-lgapp-primary"><i class="fa fa-plus"></i> Cargar más</button>
          <input type="hidden" id="result_dir_no" value="5">
          <? } ?>



        </div>


      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include_once("foot.php"); ?>

    <!-- Bootstrap core JavaScript -->
    <!--<script src="vendor/jquery/jquery.min.js"></script>-->
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script type="text/javascript">
    const scope_url = "<?php echo $page_url; ?>";
    
    $(document).ready(function(){
        $("#load").click(function(){
            loadmore(1);
        });
        $("#load_dir").click(function(){
            loadmore(2);
        });
        $("#load_cot").click(function(){
            loadmore(3);
        });
    });

    function loadmore(tipo)
    {
        if(tipo == 1){
            var val = document.getElementById("result_no").value;
        }else if(tipo == 2){
            var val = document.getElementById("result_dir_no").value;
        }else if(tipo == 3){
            var val = document.getElementById("result_cot_no").value;
        }
        $.ajax({
            type: 'POST',
            url: scope_url + "/actions/a_getmore.php",
            data: { valor:val, tipo: 'loadmore', consulta: tipo },
            success: function (response) {

                if(tipo == 1){
                    var content = document.getElementById("result_express");
                    document.getElementById("result_no").value = Number(val)+5;
                }else if(tipo == 2){
                    var content = document.getElementById("result_directa");
                    document.getElementById("result_dir_no").value = Number(val)+5;
                }else if(tipo == 3){
                    var content = document.getElementById("result_cotizacion");
                    document.getElementById("result_cot_no").value = Number(val)+5;
                }
        
                content.innerHTML = content.innerHTML+response;

                // We increase the value by 2 because we limit the results by 2
                
            }
        });
    }

  </script>


  </body>

</html>
