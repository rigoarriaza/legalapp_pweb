<?php include_once("head.php"); ?>

<?php include_once("navbar.php"); ?>


<?php
    if ( ! isset($elementPATH)) {
        $elementPATH = realpath(__DIR__ . '/..');
        define("elementPATH", $elementPATH);
    }
    
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . '/../../actions');
		define("actionPATH", $actionPATH);
	}

	if(!isset($_SESSION)){
		session_start();
	}
	
	include_once(actionPATH . DIRECTORY_SEPARATOR . "a_getdata.php");

    if($slug){
      $h_members_list = getServiceData('getAbogadosDestacadosFiltroEspecialidad', $userData->token_session, $userData->iduserapp, (array)[0,10,$slug,getRandom()]);
    }else{
      $h_members_list = getServiceData('getAbogadosDestacados', $userData->token_session, $userData->iduserapp, (array)[0,10,getRandom()]);
    }
      
    if($h_members_list != false && count($h_members_list) > 0 ){
      foreach ($h_members_list as $key => $h_member) {
        $m_photo = (empty($h_member->p_foto)) ? $page_url . '/assets/default_user.png' : $h_member->p_foto;
        $member = 	 array ("name" => $h_member->p_nombre . " " . $h_member->p_apellido,
                    "title"=> (isset($h_member->p_especialidad)) ? $h_member->p_especialidad : "",
                    "rate"=> $h_member->calificacion,
                    "user_url"=> "$page_url/abogado/" . $h_member->idprofesional,
                    "answers" =>$h_member->n_contestadas,
                    "comment" =>$h_member->n_comentarios,
                    "photo"=>$m_photo);
        $members->highlighted[] = (object) $member;
      }
    }

    if($slug){
      $h_members_list_a = getServiceData('getAbogadosFiltroEspecialidad', $userData->token_session, $userData->iduserapp, (array)[$slug,0,10,getRandom()]);
    }else{
      $h_members_list_a = getServiceData('getAbogados', $userData->token_session, $userData->iduserapp, (array)[0,10,getRandom()]);
    }
    
    if($h_members_list_a != false && count($h_members_list_a) > 0 ){
      foreach ($h_members_list_a as $key => $h_member_a) {
        $m_photo = (empty($h_member_a->p_foto)) ? $page_url . '/assets/default_user.png' : $imageUrl.'/'.$h_member_a->p_foto;
        $member_a = 	 array ("name" => $h_member_a->p_nombre . " " . $h_member_a->p_apellido,
                    "answers" =>$h_member_a->n_contestadas,
                    "rate"=> $h_member_a->calificacion,
                    "plan"=> $h_member_a->idplan,
                    "user_url"=> "$page_url/abogado/" . $h_member_a->idprofesional,
                    "comment" =>$h_member_a->n_comentarios,
                    "detail" =>$h_member_a->p_descripcion,
                    "photo"=>$m_photo);
        $members_a->lawyer[] = (object) $member_a;
      }
    }

    $especialidades = getServiceDataCommun('getEspecialidades', (array)[0,20]);

  // var_dump($member_a);

	  
?>


    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <!-- Sidebar Widgets Column -->
        <div class="col col-md-4">

 
          <?php include_once("sidemenu.php"); ?>


          <div class="card my-4 " style="width:100%;" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingOne">
                <h5 class="card-header subtitulo">
                  <div role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <label>Filtrar abogados  </label>
                      <p style="font-size:14px; font-weight:100"> Seleccione una especialidad </p>
                  </div>
                </h5>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                  <div class="list-group menu-side">
                  <?php
                      foreach ($especialidades as $key => $h_esp) {
                        ?>
                        <a href="<?= $page_url ?>/home/<?=$h_esp->id_especialidad ?>" class="list-group-item ">
                          <img src="<?=$h_esp->e_imagen ?>" style="width:40px;"/><?=$h_esp->e_nombre ?>
                        </a>
                     
                  <?    
                      }
                  ?> 
                  </div> 
                </div>
              </div>
            </div>
              
          </div>
        </div>

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          
          <!--acceso a consulta express-->
          <div class="list-group">
              <a class="card" href="#">
                  <div class="row no-gutters">
                      <div class="col-auto">
                          <img style="width: 80px !important; height: 80px !important;" src="<?=$imageUrl ?>/<?=$userData->u_foto; ?>"  class="img-fluid imglist"  alt="">
                      </div>
                      <div class="col" onClick="preguntaExpress()">
                          <div class="card-block px-2">

                              <h4 class="title-list"><?=$userData->u_nombre; ?><?=$userData->u_apellido; ?><label class="detail-list"> </label></h4>
                                <i class="fa fa-edit"></i> ¿Cuál es tu consulta?
                          </div>
                      </div>
                  </div>
                  <div class="card-footer w-100 text-muted" onClick="preguntaExpress()">
                    <div class="row">
                      <div class="col-12"><button class="btn lgapp-secondary" type="button">Consultar</button></div>
                      
                    </div>
                  </div>
              </a>
            </div>
                <!--<a href="#" class="card destacado" style="width: 12rem;">
                  <img class="card-img-top" class="img-circle" src="" alt="Card image cap">
                  <div class="card-body">
                    <h6 class="card-title" style="white-space:initial">
                      Rigo Arriaza
                    </h6>
                    <div class="row">
                      <div class="col-6"><i class="fa fa-edit"></i> Cuál es tu consulta?</div>
                      <div class="col-6"></div>
                    </div>
                    
                  </div>
                </a>-->

          <!--profesionales destacados-->
          <h3 class="subtitulo">Destacados</h3>

          <div class="scrolling-wrapper">
          <?php

          if(isset($members->highlighted) && count($members->highlighted) > 0){
            foreach ($members->highlighted as $member) {
              
            
              foreach ($member as $key => $value) {  
                
                if($key == "name"){
                  $nombre = $value;
                }if($key == "rate"){
                  $rate = $value;
                }if($key == "user_url"){
                  $url = $value;
                }if($key == "photo"){
                  $foto = $value;
                }if($key == "answers"){
                  $answers = $value;
                }if($key == "comment"){
                  $comment = $value;
                }
              }
              ?>
                <a href="<?=$url; ?>" class="card destacado " style="width: 12rem;">

                  <img class="card-img-top imgperfil" class="img-circle" src="<?=$imageUrl?>/<?=$foto?>" alt="">
                  <div class="imgpremium"><img  src="<?=$page_url;?>/assets/premium.png"  ></div>
                  <div class="card-body">
                    <h6 class="card-title" style="white-space:initial">
                      <?=$nombre; ?>
                    </h6>
                    <p class="card-text"><?=$answers; ?> respuestas </p>
                    <div class="row">
                      <div class="col-6"><i class="fa fa-star"></i> <?=$rate; ?></div>
                      <div class="col-6"><i class="fa fa-comment"></i> <?=$comment; ?></div>
                    </div>
                    
                  </div>
                </a>
              <?
            }
          }

          ?>   
            
          </div>

          <h3 class="subtitulo">Listado de abogados</h3>

           <div class="list-group" id="result_">

          <?php

          if(isset($members_a->lawyer) && count($members_a->lawyer) > 0){
            foreach ($members_a->lawyer as $member_a) {
              
            
              foreach ($member_a as $key => $value) {
                  // print "$key => $value\n";
                  

                if($key == "name"){
                  $nombre = $value;
                }
                if($key == "rate"){
                  $rate = $value;
                }if($key == "user_url"){
                  $url = $value;
                }if($key == "photo"){
                  $foto = $value;
                }if($key == "answers"){
                  $answers = $value;
                }if($key == "comment"){
                  $comment = $value;
                }if($key == "detail"){
                  $detail = $value;
                }if($key == "plan"){
                  $plan = $value;
                }
              }
              ?>
              <a class="card <?=($plan != 0 )? 'borderPremium': ' '; ?>" href="<?=$url; ?>">
                  <div class="row no-gutters">
                      <div class="col-auto">
                          <img src="<?=$foto; ?>" class="img-fluid imglist"  alt="">
                      </div>
                      <div class="col">
                          <div class="card-block px-2">

                              <h4 class="title-list">
                              <?php if($plan != 0 ){ ?>
                                <img  src="<?=$page_url;?>/assets/premium.png" >
                              <? } ?>
                      
                                <?=$nombre; ?> <label class="detail-list"><i class="fa fa-star"></i> <?=$rate; ?></label></h4>
                              
                              <p class="card-text"><?=$detail; ?></p>
                          </div>
                      </div>
                  </div>
                  <div class="card-footer w-100 text-muted">
                    <div class="row">
                      <div class="col-6"><i class="fa fa-comments-o"></i> <?=$answers; ?> Respuestas</div>
                      <div class="col-6"><i class="fa fa-comment"></i> <?=$comment; ?> Comentarios</div>
                    </div>
                  </div>
              </a>

              <?
            }
          }

          ?>   
            
           
            
          </div>
          <?if(count($h_members_list_a) > 9 ){ ?>
          <button  id="load" class="btn btn-lgapp-primary"><i class="fa fa-plus"></i> Cargar más</button>
          <input type="hidden" id="result_no" value="10">
          <? } ?>
         
        </div>


      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include_once("foot.php"); ?>

    <!-- Bootstrap core JavaScript -->
    <!--<script src="vendor/jquery/jquery.min.js"></script>-->
    <script src="<?= $page_url ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script type="text/javascript">
    const scope_url = "<?php echo $page_url; ?>";

    $(document).ready(function(){
        $("#load").click(function(){
            loadmore();
        });
    });

    function loadmore(tipo)
    {
        var val = document.getElementById("result_no").value;
        
        console.log(val);
        $.ajax({
            type: 'POST',
            url: scope_url + "/actions/a_getmore.php",
            data: { valor:val, tipo: 'loadmore', consulta: 0 },
            success: function (response) {

                var content = document.getElementById("result_");
                document.getElementById("result_no").value = Number(val)+10;
                
        
                content.innerHTML = content.innerHTML+response;

                // We increase the value by 2 because we limit the results by 2
                
            }
        });
    }

    var refobj = [];
    function aliasConfirm(option){
         refobj.push($.confirm(option));
    }
    function closeAll(){
       $.each(refobj, function(i, a){ 
            a.close(); 
            // you can pop the closed ones from refobj array here.
       })
    }

    function preguntaExpress(){

      aliasConfirm({
        title: 'Consulta Express',
        theme: 'material', // 'material', 
        columnClass: 'col-md-6 col-md-offset-6',
        content: '' +
          '<form id="formExpress"  novalidate="novalidate">' +
          '<div class="form-group">' +
          '<input maxlength="70" placeholder="Título de tu consulta" class="msj1 form-control" required /><br>' +
          '<textarea rows="3" maxlength="1000" placeholder="Ingresa tu consulta..." class="msj2 form-control" required /></textarea>' +
          '</div>' +
          '<div class="modal-msj">'+
          '<div class="info"><span > límite 1000 caracteres </span></div>'+
          '<p>Te sugerimos que al momento de elaborar el texto de tu pregunta, evites escribir nombres propios y/o empresas. '+
          '<br><span class="negrita">El precio de la consulta express es de Bs. 35</span></p>'+
          '</div>'+ 
          '<div class="form-group">'+
            '<div class="col-sm-offset-2 col-sm-10">'+
              '<div class="checkbox">'+
                '<label><input type="checkbox" id="msj3" > Consulta Anónima </label>'+
              '</div>'+
            '</div>'+
          '</div>'+
          '</form>',
        buttons: {
         cancel: {
            text: 'Cancelar',
              function () {
              //close
            },
          },
          formSubmit: {
            text: 'Enviar',
            btnClass: 'lgapp-secondary',
            action: function () {
              var mensaje = this.$content.find('.msj1').val();
              var mensaje2 = this.$content.find('.msj2').val();
              var elm = document.getElementById('msj3');
              // console.log(elm.checked);
              // var ckb = this.$content.find('.msj3').val();

              if(!mensaje || !mensaje2){
                $.alert('Ingrese el título y detalle de su consulta.');
                return false;
              }else{

                aliasConfirm({
                  title: '¿Haz finalizado su pregunta?',
                  content: 'Recuerde que no podrá realizar cambios una vez enviada su pregunta.<br> Asegúrese que contiene todo lo necesario.',
                  buttons: {
                      cancel: {
                        text: 'No, editar',
                        action: function () {
                            console.log('no,editar');
                          // return false;
                        },
                      },
                      close: {
                        text: 'Si, enviar',
                        btnClass: 'lgapp-secondary',
                        action: function(){
                          console.log('cerrando');
                          // return true;
                            $('body').loading({message: 'Cargando...'});
                            var datos = {
                                title:mensaje,
                                detalle:mensaje2,
                                anonimo: elm.checked
                            };
                            // var form3 = $("#formExpress").serializeArray();
                            console.log(datos);
                            $.ajax({
                              url: scope_url + "/actions/a_ask.php",
                              type:'POST',
                              data: { data: JSON.stringify(datos), tipo: 'express' },
                              success: function(resp){
                                $('body').loading('stop');
                                //aqui va a devolver obj respuesta id
                                console.log(resp);
                                if ($.isNumeric(resp)) {
                                  
                                  //consultar nuevamente
                                  //Pagar Pregunta  //Ir a mis preguntas
                                  aliasConfirm({
                                    title: '¡Pregunta enviada!',
                                    content: 'Tu pregunta ha sido enviada a la comunidad de abogados. <br>Para que la pregunta sea visible, confirma tu saldo y en breve un abogado te responderá. Tienes saldo disponible, ¿quieres pagar tu pregunta ahora?',
                                    buttons: {
                                        cancel: {
                                          text: 'Ir a mis preguntas',
                                          action: function () {
                                             closeAll();
                                             window.location.replace(scope_url + "/preguntas");
                                          },
                                        },
                                        close: {
                                          text: 'Pagar pregunta',
                                          btnClass: 'lgapp-secondary',
                                          action: function(){
                                            
                                              $('body').loading({message: 'Cargando...'});
                                              $.ajax({
                                                url: scope_url + "/actions/a_getmore.php",
                                                type:'POST',
                                                data: { id: resp, tipo: 'setPayExpress' },
                                                success: function(data){
                                                  $('body').loading('stop');
                                                  console.log(data);
                                                  if (data === "OK") {
                                                    $.toast({
                                                      text: "Tu pregunta ha sido enviada a la comunidad de abogados. <br><br>En breve un abogado te responderá. ",
                                                      heading: '¡Pregunta enviada!',
                                                      position: 'top-center',
                                                      loaderBg: '#C1262E',
                                                      bgColor: 'white',
                                                      textColor: 'grey',
                                                      stack: false,
                                                      hideAfter: 7000
                                                    });	                                                    
                                                    closeAll();
                                                  
                                                  }else if (data === "error_saldo") {

                                                    aliasConfirm({
                                                      title: '¡Saldo insuficiente!',
                                                      content: 'Tu saldo actual no es suficiente para pagar la pregunta.',
                                                      buttons: {
                                                          cancel: {
                                                            text: 'Aceptar',
                                                            action: function () {
                                                               closeAll();
                                                            },
                                                          },
                                                          close: {
                                                            text: 'Pagar pregunta',
                                                            btnClass: 'lgapp-secondary',
                                                            action: function(){
                                                              
                                                                window.location.replace(scope_url + "/cuenta");	
                                                              
                                                            }
                                                          }
                                                      }
                                                    });

                                                  }else{
                                                    var errormsj= errorMessage(data);
                                                    // var errormsj= data;
                                                    $.toast({
                                                      text: errormsj,
                                                      position: 'top-center',
                                                      stack: false
                                                    });	
                                                  }
                                                }
                                              });	
                                            
                                          }
                                        }
                                    }
                                  });
                                  return false;
                                  
                                  // closeAll();
                                  // window.location.replace(scope_url + "/home");
                                }else{
                                  var errormsj= errorMessage(resp);
                                  // var errormsj= data;
                                  $.toast({
                                    text: errormsj,
                                    position: 'top-center',
                                    stack: false
                                  });	
                                }
                              }
                            });	
                          
                        }
                      }
                  }
                });
                return false;

              }		
            }
          }
            
        },
        onContentReady: function () {
          // bind to events
          console.log('load')
          var jc = this;
          this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it

            console.log('ejecuta')
          });
        }
      });
    }
    function preguntaExpress2(){
      $.confirm({
        title: 'Consulta Express',
        theme: 'material', // 'material', 
        columnClass: 'col-md-6 col-md-offset-6',
        content: '' +
          '<form id="formExpress"  novalidate="novalidate">' +
          '<div class="form-group">' +
          '<input maxlength="70" placeholder="Título de tu consulta" class="msj1 form-control" required /><br>' +
          '<textarea rows="3" maxlength="1000" placeholder="Ingresa tu consulta..." class="msj2 form-control" required /></textarea>' +
          '</div>' +
          '<div class="modal-msj">'+
          '<div class="info"><span > límite 1000 caracteres </span></div>'+
          '<p>Te sugerimos que al momento de elaborar el texto de tu pregunta, evites escribir nombres propios y/o empresas. '+
          '<br><span class="negrita">El precio de la consulta express es de Bs. 35</span></p>'+
          '</div>'+ 
          '<div class="form-group">'+
            '<div class="col-sm-offset-2 col-sm-10">'+
              '<div class="checkbox">'+
                '<label><input type="checkbox" id="msj3" > Consulta Anónima </label>'+
              '</div>'+
            '</div>'+
          '</div>'+
          '</form>',
        buttons: {
          formSubmit: {
            text: 'Enviar',
            btnClass: 'lgapp-secondary',
            action: function () {
              var mensaje = this.$content.find('.msj1').val();
              var mensaje2 = this.$content.find('.msj2').val();
              var elm = document.getElementById('msj3');
              // console.log(elm.checked);
              // var ckb = this.$content.find('.msj3').val();

              if(!mensaje || !mensaje2){
                $.alert('Ingrese el título y detalle de su consulta.');
                return false;
              }else{

                $.confirm({
                  title: '¿Haz finalizado su pregunta?',
                  content: 'Recuerde que no podrá realizar cambios una vez enviada su pregunta.<br> Asegúrese que contiene todo lo necesario.',
                  buttons: {
                      cancel: {
                        text: 'No, editar',
                        action: function () {
                            console.log('no,editar');
                          // return false;
                        },
                      },
                      close: {
                        text: 'Si, enviar',
                        action: function(){
                          console.log('cerrando');
                          // return true;
                          Close();
                        }
                      }
                  }
              });
              return false;
                var datos = {
                    title:mensaje,
                    detalle:mensaje2,
                    anonimo: elm.checked
                };
                // var form3 = $("#formExpress").serializeArray();
                console.log(datos);
		            $.ajax({
                  url: scope_url + "/actions/a_ask.php",
                  type:'POST',
                  data: { data: JSON.stringify(datos), tipo: 'express' },
                  success: function(data){
                    
                    console.log(data);
                    if (data === "OK") {
                      console.log(data);
                      $.toast({
                        text: "Hemos enviado a tu cuentas las instrucciones para recuperar tu contraseña.",
                        position: 'bottom-center',
                        stack: false
                      });	
                      // window.location.replace(scope_url + "/home");
                    }else{
                      var errormsj= errorMessage(data);
                      // var errormsj= data;
                      $.toast({
                        text: errormsj,
                        position: 'bottom-center',
                        stack: false
                      });	
                    }
                  }
                });	
              }		
            }
          },
          cancel: {
            text: 'Cancelar',
            function () {
            //close
          },
          }
            
        },
        onContentReady: function () {
          // bind to events
          console.log('load')
          var jc = this;
          this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it

            console.log('ejecuta')
          });
        }
      });
    }

  </script>


  </body>

</html>
