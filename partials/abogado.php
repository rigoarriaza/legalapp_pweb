<?php include_once("head.php"); ?>

<?php include_once("navbar.php"); ?>

<?php
    if ( ! isset($elementPATH)) {
        $elementPATH = realpath(__DIR__ . '/..');
        define("elementPATH", $elementPATH);
    }
    
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . '/../../actions');
		define("actionPATH", $actionPATH);
	}

	if(!isset($_SESSION)){
		session_start();
	}
	
	include_once(actionPATH . DIRECTORY_SEPARATOR . "a_getdata.php");


  $lawyer = getServiceData('getPerfilProfesionalApp', $userData->token_session, $userData->iduserapp, (array)[$slug]);
	
	// var_dump($lawyer);

  $user = (object) ["id"=>$lawyer[0]->idprofesional,
                     "photo"=> getUserPhoto($lawyer[0]->p_foto),
                     "name"=> $lawyer[0]->p_nombre . ' ' . $lawyer[0]->p_apellido,
                     "description"=> $lawyer[0]->p_descripcionpro ,
                     "title"=> '',
                     "rate"=> $lawyer[0]->calificacion,
                     "comentarios"=> $lawyer[0]->comentarios,
                     "especialidad"=> $lawyer[0]->especialidad,
                     "answers" => $lawyer[0]->n_contestadas,
                     "plan" => $lawyer[0]->idplan,
                     "favorito" => $lawyer[0]->favorito,
                     "ultimas" => $lawyer[0]->ultima_pregunta,
                     "examenes" => $lawyer[0]->examenes
                     ];


  // var_dump($user);

	if(count($user->ultimas) > 0 ){
		foreach ($user->ultimas as $key => $preg) {
			$preg_o = 	 array (
									"titulo" =>$preg->titulo_p,
									"detalle"=> $preg->contenido_p,
									"valor"=> $preg->calificacion_p,
									"fecha" =>formatDate($preg->fecha_p));
			$preguntas->ultimas[] = (object) $preg_o;
		}
	}
  if(count($user->comentarios) > 0 ){
		foreach ($user->comentarios as $key => $com) {
			$com_o = 	 array (
									"comentario" =>$com->comentario,
									"calificacion"=> $com->calificacion);
			$comentarios->ultimos[] = (object) $com_o;
		}
	}
  if(count($user->especialidad) > 0 ){
		foreach ($user->especialidad as $key => $esp) {
			$esp_o = 	 array (
									"nombre" =>$esp->nombre,
									"imagen"=> $esp->imagen);
			$especialidades->mis[] = (object) $esp_o;
		}
	}if(count($user->examenes) > 0 ){
		foreach ($user->examenes as $key => $exa) {
			$esp_o = 	 array (
									"nombre" =>$exa->nombre,
									"imagen"=> $exa->icono);
			$especialidades->mis[] = (object) $esp_o;
		}
	}
  
  $valorConsulta = getServiceDataCommun('getValorServiciosProfesional', (array)[$user->id]);

?>


      <div class="row divhead">
        <div class="col col-md-1"></div>
        <div class="col-12 col-md-10" style="max-width:1140px !important;">
          <div class="list-group" >
              <div class="" >
                  <div class="row no-gutters">
                      <div class="col-auto">
                          <img style="width: 150px !important; height: 150px !important;" src="<?=$user->photo ?>" class="img-fluid imglist"  alt="">
                      </div>
                      <div class="col" >
                          <div class="card-block px-2">

                              <h4 class="subtitulo"><?=$user->name ?><label class="detail-list"> </label>
                              <?php if($user->plan != 0 ){ ?>
                                <img  src="<?=$page_url;?>/assets/premium.png" > 
                                <span style="font-size: 14px; font-weight: 200;">Usuario Premium</span>
                              <? } ?>
                              </h4>
                                <!--<i class="fa fa-edit"></i> ¿Cuál es tu consulta?-->
                              <p><?=$user->description ?></p>
                              <div class="row">
                                <div class="col-6"><button class="btn lgapp-secondary btnlg-s" type="button"><i class="fa fa-star"></i> <?=$user->rate ?></button> Calificación</div>
                                <div class="col-6"><button class="btn lgapp-secondary btnlg-s" type="button"><?=$user->answers ?></button> Respuestas</div>
                                
                              </div> 
                          </div>
                      </div>
                  </div>
                  <!--<div class="card-footer w-100 text-muted" onClick="preguntaExpress()">
                    <div class="row">
                      <div class="col-12"><button class="btn lgapp-secondary" type="button">Consultar</button></div>
                      
                    </div>
                  </div>-->
              </div>
            </div>
        </div>
        <div class="col col-md-1"></div>
      </div>
    <!-- Page Content -->
    <div class="container">


      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          
          <!--acceso a consultas-->
          <?php if($user->plan !=0){  ?>
          <div class="list-group">
              <a class="card" href="#">
                  <div class="row no-gutters">
                      <div class="col-auto">
                          <img style="width: 80px !important; height: 80px !important;" src="<?=$imageUrl ?>/<?=$userData->u_foto; ?>" class="img-fluid imglist"  alt="">
                      </div>
                      <div class="col" >
                          <div class="card-block px-2">

                              <h4 class="title-list"><?=$userData->u_nombre; ?> <?=$userData->u_apellido; ?><label class="detail-list"> </label></h4>
                                <i class="fa fa-edit"></i> ¿Quieres hacer una consulta directa a <?=$user->name ?>?
                          </div>
                      </div>
                  </div>
                  <div class="card-footer w-100 text-muted" >
                    <div class="row" style="text-align:center;">
                      <div class="col-6"><button onClick="preguntaDirecta()" class="btn lgapp-secondary btnlg-m" type="button">Consultar</button></div>
                      <div class="col-6"><button onClick="preguntaCotizar()" class="btn lgapp-secondary btnlg-m" type="button">Cotizar</button></div>
                      
                    </div>
                  </div>
              </a>
          </div>

          <? } ?>

          <h3 class="subtitulo">Últimas preguntas contestadas</h3>

          <?php

          if( !isset($preguntas->ultimas) || count($preguntas->ultimas) == 0 ){?>

            <div class="error" >
              <label > <i class="fa fa-search"></i> No hay datos</label>
            </div>

          <?	
          }else if(isset($preguntas->ultimas) && count($preguntas->ultimas) > 0){
            foreach ($preguntas->ultimas as $pregunta) {
              
            
              foreach ($pregunta as $key => $value) {  
                
                if($key == "titulo"){
                  $titulo = $value;
                }if($key == "valor"){
                  $valor = $value;
                }if($key == "detalle"){
                  $detalle = $value;
                }if($key == "fecha"){
                  $fecha = $value;
                }
              }
              ?>

              <div class="list-group">
                <a class="card" href="#">
                    <div class="row no-gutters">
                        <div class="col-auto">
                        </div>
                        <div class="col" onClick="preguntaExpress('<?=$titulo?>','<?=$detalle?>')">
                            <div class="card-block px-2">

                                <h4 class="title-list"><?=$titulo?><label class="detail-list"> <i class="fa fa-star"></i>  <?=$valor?> </label></h4>
                                <p style="font-size: 11px;    color: grey; margin:0px;">  <?=$fecha ?> </p>
                                <p style="white-space: initial;    font-size: 13px;    color: grey;">  <?=$detalle ?> </p>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer w-100 text-muted" >
                      <div class="row">
                        <div class="col-12"><button class="btn btn-sm lgapp-secondary" onClick="preguntaExpress('<?=$titulo?>','<?=$detalle?>')" type="button">Consultar sobre este tema</button></div>
                        
                      </div>
                    </div>
                </a>
              </div>
                
              <?
            }
          }

          ?>   
        


          <h3 class="subtitulo">Comentarios</h3>
          
          <div class="list-group">
          <?php

          if( !isset($comentarios->ultimos) || count($comentarios->ultimos) == 0 ){?>

            <div class="error" >
              <label > <i class="fa fa-search"></i> No hay datos</label>
            </div>

          <?	
          }else if(isset($comentarios->ultimos) && count($comentarios->ultimos) > 0){
            foreach ($comentarios->ultimos as $comment) {
              
            
              foreach ($comment as $key => $value) {  
                
                if($key == "comentario"){
                  $comentario = $value;
                }if($key == "calificacion"){
                  $calificacion = $value;
                }
              }
              ?>

              <div class="list-group-item">
                <?=$comentario ?>
                <br>
                <i class="fa fa-star"></i><?=$calificacion ?>
              </div>
                                             
              <?
            }
          }

          ?> 

          </div>

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col col-md-4">

          <!-- Search Widget -->
          <!--<div class="card my-4">
            <h5 class="card-header">Search</h5>
            <div class="card-body">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                  <button class="btn btn-secondary" type="button">Go!</button>
                </span>
              </div>
            </div>
          </div>-->

          <div class="row">
              <div class="col-12">
                <button id="btn-favorito" class="<?php echo ($user->favorito == '0') ? 'btn-lgapp-primary ': 'btn-lgapp-secondary  '; ?>  btn btn-block " type="button" onClick="setfavorito()">
                  <i class="fa fa-heart "></i> Favorito</button>
              </div>
                      
          </div>
          <br>

          <div class="list-group menu-side">
           <h5 class="card-header">Especialidades</h5>
          <?php

          if( !isset($especialidades->mis) || count($especialidades->mis) == 0 ){?>

            <div class="error" >
              <label > <i class="fa fa-search"></i> No hay datos</label>
            </div>

          <?	
          }else if(isset($especialidades->mis) && count($especialidades->mis) > 0){
            foreach ($especialidades->mis as $esp) {
              
            
              foreach ($esp as $key => $value) {  
                
                if($key == "imagen"){
                  $imagen = $value;
                }if($key == "nombre"){
                  $nombre = $value;
                }
              }
              ?>
              <a href="#" class="list-group-item">
              <img  src="<?=$imagen ?>" style="width: 40px;"/> <?=$nombre ?></a>
                                           
              <?
            }
          }

          ?> 
          </div>

          <br>
          <div class="list-group menu-side">
           <h5 class="card-header">Exámenes logrados</h5>
          <?php

          if( !isset($examenes->mis) || count($examenes->mis) == 0 ){?>

            <div class="error" >
              <label > <i class="fa fa-search"></i> No hay datos</label>
            </div>

          <?	
          }else if(isset($examenes->mis) && count($examenes->mis) > 0){
            foreach ($examenes->mis as $esp) {
              
            
              foreach ($esp as $key => $value) {  
                
                if($key == "imagen"){
                  $imagen = $value;
                }if($key == "nombre"){
                  $nombre = $value;
                }
              }
              ?>
              <a href="#" class="list-group-item">
              <img  src="<?=$imagen ?>" style="width: 40px;"/> <?=$nombre ?></a>
                                           
              <?
            }
          }

          ?> 
          </div>


        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include_once("foot.php"); ?>


  <script src="../plugins/input-mask/jquery.inputmask.js" type="text/javascript"></script>
  <script src="../plugins/input-mask/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
  <script src="../plugins/input-mask/jquery.inputmask.extensions.js" type="text/javascript"></script>
    
  <script type="text/javascript">
    const scope_url = "<?php echo $page_url; ?>";


    var refobj = [];
    function aliasConfirm(option){
         refobj.push($.confirm(option));
    }
    function closeAll(){
       $.each(refobj, function(i, a){ 
            a.close(); 
            // you can pop the closed ones from refobj array here.
       })
    }

    function preguntaDirecta(){

      var valorconsulta = "<?php echo $valorConsulta; ?>";

      var abogado = "<?=$user->name ?>";

      aliasConfirm({
        title: 'Pregunta Directa a: '+abogado ,
        theme: 'material', // 'material', 
        columnClass: 'col-md-6 col-md-offset-6',
        content: '' +
          '<form id="formExpress"  novalidate="novalidate">' +
          '<div class="form-group">' +
          '<input maxlength="70" placeholder="Título de tu consulta" class="msj1 form-control" required /><br>' +
          '<textarea rows="3" maxlength="1000" placeholder="Describe tu consulta..." class="msj2 form-control" required /></textarea>' +
          '</div>' +
          '<div class="modal-msj">'+
          '<div class="info"><span > límite 1000 caracteres </span></div>'+
          '<p><span class="negrita">El precio de la consulta de este abogado es de Bs. '+valorconsulta+'</span></p>'+
          '</div>'+ 
          '<div class="form-group">'+
            '<div class="col-sm-offset-2 col-sm-10">'+
              '<div class="checkbox">'+
                '<label><input type="checkbox" id="msj3" > Consulta Anónima </label>'+
              '</div>'+
            '</div>'+
          '</div>'+
          '</form>',
        buttons: {
         cancel: {
            text: 'Cancelar',
              function () {
              //close
            },
          },
          formSubmit: {
            text: 'Enviar',
            btnClass: 'lgapp-secondary',
            action: function () {
              var mensaje = this.$content.find('.msj1').val();
              var mensaje2 = this.$content.find('.msj2').val();
              var elm = document.getElementById('msj3');
              // console.log(elm.checked);
              // var ckb = this.$content.find('.msj3').val();

              if(!mensaje || !mensaje2){
                $.alert('Ingrese el título y detalle de su consulta.');
                return false;
              }else{

                aliasConfirm({
                  title: '¿Has finalizado su pregunta directa?',
                  content: 'Asegúrate de que la misma contenga todos los detalles necesarios para que el abogado pueda darte una respuesta precisa.',
                  buttons: {
                      cancel: {
                        text: 'No, editar',
                        action: function () {
                            console.log('no,editar');
                          // return false;
                        },
                      },
                      close: {
                        text: 'Si, enviar',
                        btnClass: 'lgapp-secondary',
                        action: function(){
                          console.log('cerrando');
                          // return true;
                          var idprofesional = "<?=$user->id ?>";
                          
                            var datos = {
                                title:mensaje,
                                detalle:mensaje2,
                                anonimo: elm.checked,
                                idprofesional: idprofesional
                            };
                            // var form3 = $("#formExpress").serializeArray();
                            console.log(datos);
                            $.ajax({
                              url: scope_url + "/actions/a_ask.php",
                              type:'POST',
                              data: { data: JSON.stringify(datos), tipo: 'directa' },
                              success: function(resp){
                                
                                //aqui va a devolver obj respuesta id
                                console.log(resp);
                                // if ($.isNumeric(resp)) {
                                  
                                  //consultar nuevamente
                                  //Pagar Pregunta  //Ir a mis preguntas
                                  aliasConfirm({
                                    title: '¡Pregunta enviada!',
                                    content: 'Tu pregunta ha sido enviada. Verifica tu saldo y espera tu respuesta.',
                                    buttons: {
                                        cancel: {
                                          text: 'Ir a mis preguntas',
                                          action: function () {
                                             window.location.replace(scope_url + "/preguntas");
                                          },
                                        },
                                        close: {
                                          text: 'Aceptar',
                                          btnClass: 'lgapp-secondary',
                                          action: function(){
                                            
                                             closeAll();
                                              // $.ajax({
                                              //   url: scope_url + "/actions/a_getmore.php",
                                              //   type:'POST',
                                              //   data: { id: resp, tipo: 'setPayExpress' },
                                              //   success: function(data){
                                                  
                                              //     console.log(data);
                                              //     if (data === "OK") {
                                              //       $.toast({
                                              //         text: "Tu pregunta ha sido enviada a la comunidad de abogados. <br><br>En breve un abogado te responderá. ",
                                              //         heading: '¡Pregunta enviada!',
                                              //         position: 'bottom-center',
                                              //         loaderBg: '#C1262E',
                                              //         bgColor: 'white',
                                              //         textColor: 'grey',
                                              //         stack: false,
                                              //         hideAfter: 7000
                                              //       });	                                                    
                                              //       closeAll();
                                                  
                                              //     }else if (data === "error_saldo") {

                                              //       aliasConfirm({
                                              //         title: '¡Saldo insuficiente!',
                                              //         content: 'Tu saldo actual no es suficiente para pagar la pregunta.',
                                              //         buttons: {
                                              //             cancel: {
                                              //               text: 'Aceptar',
                                              //               action: function () {
                                              //                  closeAll();
                                              //               },
                                              //             },
                                              //             close: {
                                              //               text: 'Pagar pregunta',
                                              //               btnClass: 'lgapp-secondary',
                                              //               action: function(){
                                                              
                                              //                   window.location.replace(scope_url + "/cuenta");	
                                                              
                                              //               }
                                              //             }
                                              //         }
                                              //       });

                                              //     }else{
                                              //       var errormsj= errorMessage(data);
                                              //       // var errormsj= data;
                                              //       $.toast({
                                              //         text: errormsj,
                                              //         position: 'bottom-center',
                                              //         stack: false
                                              //       });	
                                              //     }
                                              //   }
                                              // });	
                                            
                                          }
                                        }
                                    }
                                  });
                                  return false;
                                  
                                  // closeAll();
                                  // window.location.replace(scope_url + "/home");
                                // }else{
                                //   var errormsj= errorMessage(resp);
                                //   // var errormsj= data;
                                //   $.toast({
                                //     text: errormsj,
                                //     position: 'bottom-center',
                                //     stack: false
                                //   });	
                                // }
                              }
                            });	
                          
                        }
                      }
                  }
                });
                return false;

              }		
            }
          }
            
        },
        onContentReady: function () {
          // bind to events
          console.log('load')
          var jc = this;
          this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it

            console.log('ejecuta')
          });
        }
      });

    }
    
    function preguntaCotizar(){

      var valorconsulta = "<?php echo $valorConsulta; ?>";

      var abogado = "<?=$user->name ?>";

      aliasConfirm({
        title: 'Cotización a: '+abogado ,
        theme: 'material', // 'material', 
        columnClass: 'col-md-6 col-md-offset-6',
        content: '' +
          '<form id="formCotizar"  novalidate="novalidate">' +
          '<div class="form-group">' +
          '<input maxlength="70" placeholder="Título del servicio a cotizar" class="msj1 form-control" required /><br>' +
          '<textarea rows="3" maxlength="1000" placeholder="Describe el servicio a cotizar..." class="msj2 form-control" required /></textarea>' +
          '</div>' +
          '<div class="modal-msj">'+
          '<div class="info"><span > límite 1000 caracteres </span></div>'+
          '</div>'+ 
          '<div class="form-group"><label >Fecha de inicio : </label><input  id="datestart"  type="text" class="form-control"/></div>'+
          '<div class="form-group"><label >Fecha de Fin : </label><input  id="dateend"  type="text" class="form-control"/></div>'+
          '<div class="form-group"><label >Prioridad : </label><select  id="prioridad"  class="form-control">'+
          '<option value="1">Baja</option>'+
          '<option value="2">Media</option>'+
          '<option value="3">Alta</option></select></div>'+
          '<div class="form-group">'+
            '<div class="col-sm-offset-2 col-sm-10">'+
              '<div class="checkbox">'+
                '<label><input type="checkbox" id="msj3" > Consulta Anónima </label>'+
              '</div>'+
            '</div>'+
          '</div>'+
          '</form>',
        buttons: {
         cancel: {
            text: 'Cancelar',
              function () {
              //close
            },
          },
          formSubmit: {
            text: 'Enviar',
            btnClass: 'lgapp-secondary',
            action: function () {
              var mensaje = this.$content.find('.msj1').val();
              var mensaje2 = this.$content.find('.msj2').val();
              var elm = document.getElementById('msj3');
              // console.log(elm.checked);
              // var ckb = this.$content.find('.msj3').val();

              if(!mensaje || !mensaje2){
                $.alert('Ingrese el título y detalle de su cotización.');
                return false;
              }else{

                aliasConfirm({
                  title: '¿Has finalizado su solicitud?',
                  content: 'Asegúrate de que la misma contenga todos los detalles necesarios para que el abogado pueda darte una respuesta precisa.',
                  buttons: {
                      cancel: {
                        text: 'No, editar',
                        action: function () {
                            console.log('no,editar');
                          // return false;
                        },
                      },
                      close: {
                        text: 'Si, enviar',
                        btnClass: 'lgapp-secondary',
                        action: function(){
                          console.log('cerrando');
                          // return true;
                          $('body').loading({message: 'Cargando...'});
                          var idprofesional = "<?=$user->id ?>";
                          var datestart = $('#datestart').val();
                          var dateend = $('#dateend').val(); 
                          var prioridad = $('#prioridad').val(); 
                          // var prioridad = document.getElementById('prioridad');
                          console.log(datestart);
                            var datos = {
                                title:mensaje,
                                detalle:mensaje2,
                                datestart: datestart,
                                dateend: dateend,
                                prioridad: prioridad,
                                anonimo: elm.checked,
                                idprofesional: idprofesional
                            };
                            // var form3 = $("#formExpress").serializeArray();
                            console.log(datos);
                            $.ajax({
                              url: scope_url + "/actions/a_ask.php",
                              type:'POST',
                              data: { data: JSON.stringify(datos), tipo: 'cotizar' },
                              success: function(resp){
                                $('body').loading('stop');
                                //aqui va a devolver obj respuesta id
                                console.log(resp);
                                // if ($.isNumeric(resp)) {
                                  
                                  //consultar nuevamente
                                  //Pagar Pregunta  //Ir a mis preguntas
                                  aliasConfirm({
                                    title: '¡Solicitud de cotización enviada!',
                                    content: 'Tu solicitud ha sido enviada.',
                                    buttons: {
                                        cancel: {
                                          text: 'Ir a mis preguntas',
                                          action: function () {
                                             window.location.replace(scope_url + "/preguntas");
                                          },
                                        },
                                        close: {
                                          text: 'Aceptar',
                                          btnClass: 'lgapp-secondary',
                                          action: function(){
                                            
                                             closeAll();
                                              // $.ajax({
                                              //   url: scope_url + "/actions/a_getmore.php",
                                              //   type:'POST',
                                              //   data: { id: resp, tipo: 'setPayExpress' },
                                              //   success: function(data){
                                                  
                                              //     console.log(data);
                                              //     if (data === "OK") {
                                              //       $.toast({
                                              //         text: "Tu pregunta ha sido enviada a la comunidad de abogados. <br><br>En breve un abogado te responderá. ",
                                              //         heading: '¡Pregunta enviada!',
                                              //         position: 'bottom-center',
                                              //         loaderBg: '#C1262E',
                                              //         bgColor: 'white',
                                              //         textColor: 'grey',
                                              //         stack: false,
                                              //         hideAfter: 7000
                                              //       });	                                                    
                                              //       closeAll();
                                                  
                                              //     }else if (data === "error_saldo") {

                                              //       aliasConfirm({
                                              //         title: '¡Saldo insuficiente!',
                                              //         content: 'Tu saldo actual no es suficiente para pagar la pregunta.',
                                              //         buttons: {
                                              //             cancel: {
                                              //               text: 'Aceptar',
                                              //               action: function () {
                                              //                  closeAll();
                                              //               },
                                              //             },
                                              //             close: {
                                              //               text: 'Pagar pregunta',
                                              //               btnClass: 'lgapp-secondary',
                                              //               action: function(){
                                                              
                                              //                   window.location.replace(scope_url + "/cuenta");	
                                                              
                                              //               }
                                              //             }
                                              //         }
                                              //       });

                                              //     }else{
                                              //       var errormsj= errorMessage(data);
                                              //       // var errormsj= data;
                                              //       $.toast({
                                              //         text: errormsj,
                                              //         position: 'bottom-center',
                                              //         stack: false
                                              //       });	
                                              //     }
                                              //   }
                                              // });	
                                            
                                          }
                                        }
                                    }
                                  });
                                  return false;
                                  
                                  // closeAll();
                                  // window.location.replace(scope_url + "/home");
                                // }else{
                                //   var errormsj= errorMessage(resp);
                                //   // var errormsj= data;
                                //   $.toast({
                                //     text: errormsj,
                                //     position: 'bottom-center',
                                //     stack: false
                                //   });	
                                // }
                              }
                            });	
                          
                        }
                      }
                  }
                });
                return false;

              }		
            }
          }
            
        },
        onContentReady: function () {
          // bind to events
          console.log('load')
          var jc = this;
          this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it

            console.log('ejecuta')
          });
        }
      });

    }

    function setfavorito(){
      
      var idprofesional = <?=$slug;?>;

    	var datos = { id : idprofesional}
      $.ajax({
        url: scope_url + "/actions/a_favorite.php",
        type:'POST',
              data: { data: JSON.stringify(datos) },
              success: function(data){
          console.log(data);
          if (data === "OK") {
            if($('#btn-favorito').hasClass('btn-lgapp-primary')){
              $('#btn-favorito').removeClass('btn-lgapp-primary');
              $('#btn-favorito').addClass('btn-lgapp-secondary');
              msj = "Abogado ha sido añadido a favoritos";
            }else{
              $('#btn-favorito').addClass('btn-lgapp-primary');
              $('#btn-favorito').removeClass('btn-lgapp-secondary');
              msj = "Se ha quitado de tu lista de favoritos";
            }
            // window.location.replace(scope_url + "/user");
            $.toast({
              heading: 'Favoritos',
              text: '<span style="font-size:18px;"></span> '+msj,
              position: 'top-center',
              loaderBg: '#C1262E',
              bgColor: 'white',
                textColor: 'grey',
              stack: false,
              hideAfter: 3000
            });
          }else{
            var errormsj= errorMessage(data);
            $.toast({
              heading: 'Error',
              text: '<span style="font-size:18px;"></span> '+errormsj,
              position: 'bottom-center',
              loaderBg: '#C1262E',
              bgColor: 'white',
                textColor: 'grey',
              stack: false,
              hideAfter: 6000
            });	
          }
              }

      });
    }


    function preguntaExpress(titulo,detalle){

      aliasConfirm({
        title: 'Consulta Express',
        theme: 'material', // 'material', 
        columnClass: 'col-md-6 col-md-offset-6',
        content: '' +
          '<form id="formExpress"  novalidate="novalidate">' +
          '<div class="form-group">' +
          '<input maxlength="70" value="'+titulo+'" placeholder="Título de tu consulta" class="msj1 form-control" required /><br>' +
          '<textarea rows="3" maxlength="1000" placeholder="Ingresa tu consulta..." class="msj2 form-control" required >'+detalle+'</textarea>' +
          '</div>' +
          '<div class="modal-msj">'+
          '<div class="info"><span > límite 1000 caracteres </span></div>'+
          '<p>Te sugerimos que al momento de elaborar el texto de tu pregunta, evites escribir nombres propios y/o empresas. '+
          '<br><span class="negrita">El precio de la consulta express es de Bs. 35</span></p>'+
          '</div>'+ 
          '<div class="form-group">'+
            '<div class="col-sm-offset-2 col-sm-10">'+
              '<div class="checkbox">'+
                '<label><input type="checkbox" id="msj3" > Consulta Anónima </label>'+
              '</div>'+
            '</div>'+
          '</div>'+
          '</form>',
        buttons: {
         cancel: {
            text: 'Cancelar',
              function () {
              //close
            },
          },
          formSubmit: {
            text: 'Enviar',
            btnClass: 'lgapp-secondary',
            action: function () {
              var mensaje = this.$content.find('.msj1').val();
              var mensaje2 = this.$content.find('.msj2').val();
              var elm = document.getElementById('msj3');
              // console.log(elm.checked);
              // var ckb = this.$content.find('.msj3').val();

              if(!mensaje || !mensaje2){
                $.alert('Ingrese el título y detalle de su consulta.');
                return false;
              }else{

                aliasConfirm({
                  title: '¿Haz finalizado su pregunta?',
                  content: 'Recuerde que no podrá realizar cambios una vez enviada su pregunta.<br> Asegúrese que contiene todo lo necesario.',
                  buttons: {
                      cancel: {
                        text: 'No, editar',
                        action: function () {
                            console.log('no,editar');
                          // return false;
                        },
                      },
                      close: {
                        text: 'Si, enviar',
                        btnClass: 'lgapp-secondary',
                        action: function(){
                          console.log('cerrando');
                          // return true;

                            var datos = {
                                title:mensaje,
                                detalle:mensaje2,
                                anonimo: elm.checked
                            };
                            // var form3 = $("#formExpress").serializeArray();
                            console.log(datos);
                            $.ajax({
                              url: scope_url + "/actions/a_ask.php",
                              type:'POST',
                              data: { data: JSON.stringify(datos), tipo: 'express' },
                              success: function(resp){
                                
                                //aqui va a devolver obj respuesta id
                                console.log(resp);
                                if ($.isNumeric(resp)) {
                                  
                                  //consultar nuevamente
                                  //Pagar Pregunta  //Ir a mis preguntas
                                  aliasConfirm({
                                    title: '¡Pregunta enviada!',
                                    content: 'Tu pregunta ha sido enviada a la comunidad de abogados. <br>Para que la pregunta sea visible, confirma tu saldo y en breve un abogado te responderá. Tienes saldo disponible, ¿quieres pagar tu pregunta ahora?',
                                    buttons: {
                                        cancel: {
                                          text: 'Ir a mis preguntas',
                                          action: function () {
                                             closeAll();
                                             window.location.replace(scope_url + "/preguntas");
                                          },
                                        },
                                        close: {
                                          text: 'Pagar pregunta',
                                          btnClass: 'lgapp-secondary',
                                          action: function(){
                                            
                                              $.ajax({
                                                url: scope_url + "/actions/a_getmore.php",
                                                type:'POST',
                                                data: { id: resp, tipo: 'setPayExpress' },
                                                success: function(data){
                                                  
                                                  console.log(data);
                                                  if (data === "OK") {
                                                    $.toast({
                                                      text: "Tu pregunta ha sido enviada a la comunidad de abogados. <br><br>En breve un abogado te responderá. ",
                                                      heading: '¡Pregunta enviada!',
                                                      position: 'bottom-center',
                                                      loaderBg: '#C1262E',
                                                      bgColor: 'white',
                                                      textColor: 'grey',
                                                      stack: false,
                                                      hideAfter: 7000
                                                    });	                                                    
                                                    closeAll();
                                                  
                                                  }else if (data === "error_saldo") {

                                                    aliasConfirm({
                                                      title: '¡Saldo insuficiente!',
                                                      content: 'Tu saldo actual no es suficiente para pagar la pregunta.',
                                                      buttons: {
                                                          cancel: {
                                                            text: 'Aceptar',
                                                            action: function () {
                                                               closeAll();
                                                            },
                                                          },
                                                          close: {
                                                            text: 'Pagar pregunta',
                                                            btnClass: 'lgapp-secondary',
                                                            action: function(){
                                                              
                                                                window.location.replace(scope_url + "/cuenta");	
                                                              
                                                            }
                                                          }
                                                      }
                                                    });

                                                  }else{
                                                    var errormsj= errorMessage(data);
                                                    // var errormsj= data;
                                                    $.toast({
                                                      text: errormsj,
                                                      position: 'bottom-center',
                                                      stack: false
                                                    });	
                                                  }
                                                }
                                              });	
                                            
                                          }
                                        }
                                    }
                                  });
                                  return false;
                                  
                                  // closeAll();
                                  // window.location.replace(scope_url + "/home");
                                }else{
                                  var errormsj= errorMessage(resp);
                                  // var errormsj= data;
                                  $.toast({
                                    text: errormsj,
                                    position: 'bottom-center',
                                    stack: false
                                  });	
                                }
                              }
                            });	
                          
                        }
                      }
                  }
                });
                return false;

              }		
            }
          }
            
        },
        onContentReady: function () {
          // bind to events
          console.log('load')
          var jc = this;
          this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it

            console.log('ejecuta')
          });
        }
      });
    }
    $(function () {

                  //Datemask dd/mm/yyyy
           $("#dateend").inputmask("yyyy/mm/dd", {"placeholder": "yyyy/mm/dd"});

            //Datemask dd/mm/yyyy
           $("#datestart").inputmask("yyyy/mm/dd", {"placeholder": "yyyy/mm/dd"});

            // $.datepicker.setDefaults($.datepicker.regional["es"]);
            // $("#datestart").datepicker({
            //     monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
            //         'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            //     dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            //     dateFormat: 'yy/mm/dd',
            //     firstDay: 1,
            //     //altFormat: "dd/mm/yy"
            //     //altField: "#actualDate"
            // });
            // $("#dateend").datepicker({
            //     monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
            //         'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            //     dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            //     //altField: "#actualDate",
            //     dateFormat: 'yy/mm/dd',
            //     //altFormat: "dd/mm/yy",
            //     firstDay: 1,
            //     //defaultDate: "2018/11/02"
            // });
        });
  </script>

  </body>

</html>
