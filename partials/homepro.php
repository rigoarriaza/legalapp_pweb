<?php include_once("head.php"); ?>

<?php include_once("navbar.php"); ?>


<?php
    if ( ! isset($elementPATH)) {
        $elementPATH = realpath(__DIR__ . '/..');
        define("elementPATH", $elementPATH);
    }
    
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . '/../../actions');
		define("actionPATH", $actionPATH);
	}

	if(!isset($_SESSION)){
		session_start();
	}
	
	include_once(actionPATH . DIRECTORY_SEPARATOR . "a_getdata.php");

  $infoPro = getServiceData('getInformacionProfesionalApp', $userData->token_session, $userData->idprofesional);

  $userData 	 = isset($_SESSION['lBo']['u_Data']) ? $_SESSION['lBo']['u_Data'] : null;
	$userData->u_idplan = $infoPro->user_plan->id_plan;
  $userData->u_ganancia = $infoPro->u_ganancia;
  $_SESSION['lBo']['u_Data'] 		= $userData;

  $pendientes = getServiceData('getDetallePendientes', $userData->token_session, $userData->idprofesional);
  
    $h_members_list_a = getServiceData('getPreguntasExpressPendientes', $userData->token_session, $userData->idprofesional, (array)[0,15]);
    //get Preguntas express
    
	if($h_members_list_a != false && $h_members_list_a != "no_data"  && count($h_members_list_a) > 0 ){
		foreach ($h_members_list_a as $key => $h_member_a) {
			$m_photo = (empty($h_member_a->u_foto) || ($h_member_a->p_privacidad == 1)) ? $page_url . '/assets/default_user.png' : $h_member_a->u_foto;
      $nombre = (($h_member_a->p_privacidad == 1)) ? 'Anónimo' : $h_member_a->u_nombre.' '.$h_member_a->u_apellido;
      $member_a = 	 array (
                  "idpreguntae"=> $h_member_a->idpreguntae,
									"nombre"=> $nombre,
									"estado"=> getEstado($h_member_a->p_estado),
									"estadoclase"=> getEstadoClase($h_member_a->p_estado),
									"titulo"=> $h_member_a->p_titulo,
									"contenido"=> $h_member_a->p_contenido,
									"caso_url"=> "$page_url/caso/" . $h_member_a->idpreguntae,
									"fecha"=> formatDate($h_member_a->p_fecha),
                  "foto"=> $m_photo
                                    );
			$members_a->lawyer[] = (object) $member_a;
		}
	}

  
  //get Preguntas directas
  $list_directas = getServiceData('getPreguntasDirectas', $userData->token_session, $userData->idprofesional, (array)[0,15]);
	
	if($list_directas != false && $list_directas != "no_data" && count($list_directas) > 0 ){
		foreach ($list_directas as $key => $h_member) {
			$m_photo = (empty($h_member->u_imagen) || ($h_member->p_privacidad == 1)) ? $page_url . '/assets/default_user.png' : $h_member->u_imagen;
      $nombre = (($h_member->p_privacidad == 1)) ? 'Anónimo' : $h_member->u_nombre.' '.$h_member->u_apellido;
      $pregunta = 	 array (
                  "estado"=> getEstado($h_member->p_estado),
									"nombre"=> $nombre,
								  "estadoclase"=> getEstadoClase($h_member->p_estado),
									"contenido"=> $h_member->p_contenido,
									"titulo"=> $h_member->p_titulo,
									"caso_url"=> "$page_url/caso/" . $h_member->idpreguntad,
									"fecha"=> formatDate($h_member->p_datetime),
                  "foto"=> $m_photo
                                    );
			$preguntas_dir->preg[] = (object) $pregunta;
		}
	}  

  //get Cotizaciones Pendientes
  $list_cotizaciones = getServiceData('getCotizacionesPendientes', $userData->token_session, $userData->idprofesional, (array)[0,15]);
	
	if($list_cotizaciones != false && $list_cotizaciones != 'no_data' && count($list_cotizaciones) > 0 ){
		foreach ($list_cotizaciones as $key => $h_member) {
			$m_photo = (empty($h_member->u_imagen) || ($h_member->privacidad == 1)) ? $page_url . '/assets/default_user.png' : $h_member->u_imagen;
      $nombre = (($h_member->privacidad == 1)) ? 'Anónimo' : $h_member->u_nombre.' '.$h_member->u_apellido;
      $pregunta = 	 array (
                  "estado"=> getEstadoCot($h_member->c_estado),
									"nombre"=> $nombre,
								  "estadoclase"=> getEstadoClaseCot($h_member->c_estado),
									"titulo"=> $h_member->c_titulo,
									"caso_url"=> "$page_url/caso/" . $h_member->idpreguntad,
									"fecha"=> formatDate($h_member->c_datetime),
                  "foto"=> $m_photo
                                    );
			$preguntas_cot->preg[] = (object) $pregunta;
		}
	}

  // var_dump($list_cotizaciones);

	  
?>


    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <!-- Sidebar Widgets Column -->
        <div class="col col-md-4">

 
          <?php include_once("sidemenu.php"); ?>



        </div>

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          
          <!--acceso a consulta express-->
          <!--<div class="list-group">
       
                      <div class="col-auto">
                          <img style="width: 80px !important; height: 80px !important;" src="<?=$imageUrl ?>/<?=$userData->u_foto; ?>"  class="img-fluid imglist"  alt="">
                      </div>
                      <div class="col" onClick="preguntaExpress()">
                          <div class="card-block px-2">

                              <h4 class="title-list"><?=$userData->u_nombre; ?><?=$userData->u_apellido; ?><label class="detail-list"> </label></h4>
                                <i class="fa fa-edit"></i> ¿Cuál es tu consulta?

        -->
          <!--notificacion-->
          <?php if($userData->u_estado != 2 ){ ?>
          <div class="alert alert-danger" role="alert">
            Tu cuenta está pendiente de validar por los administradores. Completa <a href="<?=$page_url?>/profesional" class="alert-link">tu perfil </a> y espera la confirmación de activación.
          </div>
          <? } ?>

          <div class="row">
            <div class="col-4" onClick="gotoExpress()" >
              <img src="<?=$page_url;?>/assets/mpro_comunidad.png" />
              <br>Preguntas express
              <span class="badge badge-secondary"><?=$pendientes->express ?></span>
            </div>
            <div class="col-4" onClick="gotoDirectas()" >
              <img src="<?=$page_url;?>/assets/mpro_privada.png" />
              <br>Preguntas Directas
              <span class="badge badge-secondary"><?=$pendientes->directas ?></span>
            </div>
            <div class="col-4" onClick="gotoCotizaciones()" >
              <img src="<?=$page_url;?>/assets/mpro_cotizacion.png" />
              <br> Cotizaciones 
              <span class="badge badge-secondary"><?=$pendientes->cotizaciones ?></span>
            </div>
            
          </div>

          <br>


          <!--profesionales destacados-->
          <h3 class="subtitulo">Preguntas Express pendientes  </h3>

           <div class="list-group" id="result_">

          <?php

              if(isset($h_members_list_a) && $h_members_list_a == 'no_data'){?>

                <div class="error" >
                  <label > <i class="fa fa-search"></i> No hay datos</label>
                </div>

              <?	
              }else if(isset($members_a->lawyer) && count($members_a->lawyer) > 0){
                foreach ($members_a->lawyer as $member_a) {      
                
                foreach ($member_a as $key => $value) {
                    // print "$key => $value\n";
                    
                    
                    if($key == "titulo"){
                        $titulo = $value;
                    }
                    if($key == "idpreguntae"){
                        $id = $value;
                    }
                    if($key == "foto"){
                        $foto = $value;
                    }if($key == "nombre"){
                        $nombre = $value;
                    }
                    if($key == "contenido"){
                      $contenido = $value;
                    }if($key == "estado"){
                      $estado = $value;
                    }if($key == "caso_url"){
                      $caso = $value;
                    }if($key == "fecha"){
                      $fecha = $value;
                    }if($key == "estadoclase"){
                      $estadoclase = $value;
                    }
                }
                ?>
                <a class="card" onClick="tomarPregunta(<?=$id?>)" >
                    <div class="row no-gutters">
                        <div class="col-auto">
                            <img src="<?=$foto ?>" style="width:75px !important; height:75px !important;" class="img-fluid imglist"  alt="">
                        </div>
                        <div class="col">
                            <div class="card-block px-2">
                                <div class="row">
                                    <div class="col-8">
                                        <h5 class="title-list"><?=$nombre; ?> </h5>
                                        <h5 class="title-list"><?=$titulo; ?> </h5>
                                        <p><?=$contenido; ?></p>
                                        <label class="detail-list"><?=$fecha; ?></label>
                                    </div>
                                    <div class="col-4" style="padding: 5px;">
                                        <button type="button" class="btn btn-sm <?=$estadoclase?>">
													<?=$estado; ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </a>


                
                <?
                }

                
              }

          ?>   
            
           
            
          </div>

          <?php if(count($members_a->lawyer)  > 9){ ?>
          <button  id="load_dir" class="btn btn-lgapp-primary"><i class="fa fa-plus"></i> Cargar más</button>
          <input type="hidden" id="result_no" value="10">
          <?php } ?>
           

          <!--preguntas directas-->
          <h3 class="subtitulo">Preguntas Directas pendientes  </h3>

           <div class="list-group" id="result_">

          <?php

              if(isset($list_directas) && $list_directas == 'no_data'){?>

                <div class="error" >
                  <label > <i class="fa fa-search"></i> No hay datos</label>
                </div>

              <?	
              }else if(isset($preguntas_dir->preg) && count($preguntas_dir->preg) > 0){
                foreach ($preguntas_dir->preg as $pregunta) {      
                
                foreach ($pregunta as $key => $value) {
                    // print "$key => $value\n";
                    
                    
                    if($key == "titulo"){
                        $titulo = $value;
                    }
                    if($key == "idpreguntae"){
                        $id = $value;
                    }
                    if($key == "foto"){
                        $foto = $value;
                    }if($key == "nombre"){
                        $nombre = $value;
                    }
                    if($key == "contenido"){
                      $contenido = $value;
                    }if($key == "estado"){
                      $estado = $value;
                    }if($key == "caso_url"){
                      $caso = $value;
                    }if($key == "fecha"){
                      $fecha = $value;
                    }if($key == "estadoclase"){
                      $estadoclase = $value;
                    }
                }
                ?>
                <a class="card" href="<?=$caso; ?>/2" >
                    <div class="row no-gutters">
                        <div class="col-auto">
                            <img src="<?=$foto ?>" style="width:75px !important; height:75px !important;" class="img-fluid imglist"  alt="">
                        </div>
                        <div class="col">
                            <div class="card-block px-2">
                                <div class="row">
                                    <div class="col-8">
                                        <h5 class="title-list"><?=$nombre; ?> </h5>
                                        <h5 class="title-list"><?=$titulo; ?> </h5>
                                        <p><?=$contenido; ?></p>
                                        <label class="detail-list"><?=$fecha; ?></label>
                                    </div>
                                    <div class="col-4" style="padding: 5px;">
                                        <button type="button" class="btn btn-sm <?=$estadoclase?>">
													<?=$estado; ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </a>


                
                <?
                }

                
              }

          ?>   
            
                      

          <!--Cotizaciones -->
          <h3 class="subtitulo">Cotizaciones pendientes  </h3>

           <div class="list-group" id="result_c">

          <?php

              if(isset($list_cotizaciones) && $list_cotizaciones == 'no_data'){?>

                <div class="error" >
                  <label > <i class="fa fa-search"></i> No hay datos</label>
                </div>

              <?	
              }else if(isset($preguntas_cot->preg) && count($preguntas_cot->preg) > 0){
                foreach ($preguntas_cot->preg as $pregunta) {      
                
                foreach ($pregunta as $key => $value) {
                    // print "$key => $value\n";
                    
                    
                    if($key == "titulo"){
                        $titulo = $value;
                    }
                    if($key == "idpreguntae"){
                        $id = $value;
                    }
                    if($key == "foto"){
                        $foto = $value;
                    }if($key == "nombre"){
                        $nombre = $value;
                    }
                    if($key == "estado"){
                      $estado = $value;
                    }if($key == "caso_url"){
                      $caso = $value;
                    }if($key == "fecha"){
                      $fecha = $value;
                    }if($key == "estadoclase"){
                      $estadoclase = $value;
                    }
                }
                ?>
                <a class="card" href="<?=$caso; ?>/3" >
                    <div class="row no-gutters">
                        <div class="col-auto">
                            <img src="<?=$foto ?>" style="width:75px !important; height:75px !important;" class="img-fluid imglist"  alt="">
                        </div>
                        <div class="col">
                            <div class="card-block px-2">
                                <div class="row">
                                    <div class="col-8">
                                        <h5 class="title-list"><?=$nombre; ?> </h5>
                                        <h5 class="title-list"><?=$titulo; ?> </h5>
                                        <label class="detail-list"><?=$fecha; ?></label>
                                    </div>
                                    <div class="col-4" style="padding: 5px;">
                                        <button type="button" class="btn btn-sm <?=$estadoclase?>">
													<?=$estado; ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </a>


                
                <?
                }

                
              }

          ?>   
            
           
            
          </div>

        </div></div>


      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include_once("foot.php"); ?>

    <!-- Bootstrap core JavaScript -->
    <!--<script src="vendor/jquery/jquery.min.js"></script>-->
    <script src="<?= $page_url ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script type="text/javascript">
    const scope_url = "<?php echo $page_url; ?>";

    $(document).ready(function(){
        $("#load").click(function(){
            loadmore();
        });
    });

    function loadmore(tipo)
    {
        var val = document.getElementById("result_no").value;
        
        console.log(val);
        $.ajax({
            type: 'POST',
            url: scope_url + "/actions/a_getmore.php",
            data: { valor:val, tipo: 'loadmore', consulta: 0 },
            success: function (response) {

                var content = document.getElementById("result_");
                document.getElementById("result_no").value = Number(val)+10;
                
        
                content.innerHTML = content.innerHTML+response;

                // We increase the value by 2 because we limit the results by 2
                
            }
        });
    }

    var refobj = [];
    function aliasConfirm(option){
         refobj.push($.confirm(option));
    }
    function closeAll(){
       $.each(refobj, function(i, a){ 
            a.close(); 
            // you can pop the closed ones from refobj array here.
       })
    }

    function tomarPregunta(id){

      var estado = <?=$userData->u_estado ?>;

      if(estado == 2){ 

                            var datos = {
                                id:id
                            };
                            // var form3 = $("#formExpress").serializeArray();
                            console.log(datos);
                            $.ajax({
                              url: scope_url + "/actions/a_takequestion.php",
                              type:'POST',
                              data: { data: JSON.stringify(datos) },
                              success: function(resp){
                                
                                //aqui va a devolver obj respuesta id
                                console.log(resp);
                                if (resp == "OK") {
                                  
                                  window.location.replace(scope_url + "/caso/" + id + "/1");
                                  //consultar nuevamente
                                  
                                }else if( resp == "error_already"){

                                
                                  aliasConfirm({
                                    title: '¡Pregunta tomada!',
                                    content: 'Lo sentimos, esta pregunta ya esta siendo contestada por otro colega.',
                                    buttons: {
                                        cancel: {
                                          text: 'Aceptar',
                                          action: function () {
                                             window.location.reload();
                                          },
                                        }
                                    }
                                  });
                                  return false;
                                  
                                  // closeAll();
                                  // window.location.replace(scope_url + "/home");
                                }else{
                                  var errormsj= errorMessage(resp);
                                  // var errormsj= data;
                                  $.toast({
                                    text: errormsj,
                                    position: 'top-center',
                                    stack: false
                                  });	
                                }
                              }
                            });	
      }else{
        $.confirm({
          title: 'Acceso restringido',
          content: 'Su cuenta esta pendiente de ser verficada por los administradores.',
          buttons: {
            cancel: {
              text: 'Aceptar',
              action: function () {
                  
              //close
            },
            }
              
          }
        });	
      }
    }


    function preguntaExpress(){

      aliasConfirm({
        title: 'Consulta Express',
        theme: 'material', // 'material', 
        columnClass: 'col-md-6 col-md-offset-6',
        content: '' +
          '<form id="formExpress"  novalidate="novalidate">' +
          '<div class="form-group">' +
          '<input maxlength="70" placeholder="Título de tu consulta" class="msj1 form-control" required /><br>' +
          '<textarea rows="3" maxlength="1000" placeholder="Ingresa tu consulta..." class="msj2 form-control" required /></textarea>' +
          '</div>' +
          '<div class="modal-msj">'+
          '<div class="info"><span > límite 1000 caracteres </span></div>'+
          '<p>Te sugerimos que al momento de elaborar el texto de tu pregunta, evites escribir nombres propios y/o empresas. '+
          '<br><span class="negrita">El precio de la consulta express es de Bs. 35</span></p>'+
          '</div>'+ 
          '<div class="form-group">'+
            '<div class="col-sm-offset-2 col-sm-10">'+
              '<div class="checkbox">'+
                '<label><input type="checkbox" id="msj3" > Consulta Anónima </label>'+
              '</div>'+
            '</div>'+
          '</div>'+
          '</form>',
        buttons: {
         cancel: {
            text: 'Cancelar',
              function () {
              //close
            },
          },
          formSubmit: {
            text: 'Enviar',
            btnClass: 'lgapp-secondary',
            action: function () {
              var mensaje = this.$content.find('.msj1').val();
              var mensaje2 = this.$content.find('.msj2').val();
              var elm = document.getElementById('msj3');
              // console.log(elm.checked);
              // var ckb = this.$content.find('.msj3').val();

              if(!mensaje || !mensaje2){
                $.alert('Ingrese el título y detalle de su consulta.');
                return false;
              }else{

                aliasConfirm({
                  title: '¿Haz finalizado su pregunta?',
                  content: 'Recuerde que no podrá realizar cambios una vez enviada su pregunta.<br> Asegúrese que contiene todo lo necesario.',
                  buttons: {
                      cancel: {
                        text: 'No, editar',
                        action: function () {
                            console.log('no,editar');
                          // return false;
                        },
                      },
                      close: {
                        text: 'Si, enviar',
                        btnClass: 'lgapp-secondary',
                        action: function(){
                          console.log('cerrando');
                          // return true;

                            var datos = {
                                title:mensaje,
                                detalle:mensaje2,
                                anonimo: elm.checked
                            };
                            // var form3 = $("#formExpress").serializeArray();
                            console.log(datos);
                            $.ajax({
                              url: scope_url + "/actions/a_ask.php",
                              type:'POST',
                              data: { data: JSON.stringify(datos), tipo: 'express' },
                              success: function(resp){
                                
                                //aqui va a devolver obj respuesta id
                                console.log(resp);
                                if ($.isNumeric(resp)) {
                                  
                                  //consultar nuevamente
                                  //Pagar Pregunta  //Ir a mis preguntas
                                  aliasConfirm({
                                    title: '¡Pregunta enviada!',
                                    content: 'Tu pregunta ha sido enviada a la comunidad de abogados. <br>Para que la pregunta sea visible, confirma tu saldo y en breve un abogado te responderá. Tienes saldo disponible, ¿quieres pagar tu pregunta ahora?',
                                    buttons: {
                                        cancel: {
                                          text: 'Ir a mis preguntas',
                                          action: function () {
                                             window.location.replace(scope_url + "/preguntas");
                                          },
                                        },
                                        close: {
                                          text: 'Pagar pregunta',
                                          btnClass: 'lgapp-secondary',
                                          action: function(){
                                            
                                              $.ajax({
                                                url: scope_url + "/actions/a_getmore.php",
                                                type:'POST',
                                                data: { id: resp, tipo: 'setPayExpress' },
                                                success: function(data){
                                                  
                                                  console.log(data);
                                                  if (data === "OK") {
                                                    $.toast({
                                                      text: "Tu pregunta ha sido enviada a la comunidad de abogados. <br><br>En breve un abogado te responderá. ",
                                                      heading: '¡Pregunta enviada!',
                                                      position: 'bottom-center',
                                                      loaderBg: '#C1262E',
                                                      bgColor: 'white',
                                                      textColor: 'grey',
                                                      stack: false,
                                                      hideAfter: 7000
                                                    });	                                                    
                                                    closeAll();
                                                  
                                                  }else if (data === "error_saldo") {

                                                    aliasConfirm({
                                                      title: '¡Saldo insuficiente!',
                                                      content: 'Tu saldo actual no es suficiente para pagar la pregunta.',
                                                      buttons: {
                                                          cancel: {
                                                            text: 'Aceptar',
                                                            action: function () {
                                                               closeAll();
                                                            },
                                                          },
                                                          close: {
                                                            text: 'Pagar pregunta',
                                                            btnClass: 'lgapp-secondary',
                                                            action: function(){
                                                              
                                                                window.location.replace(scope_url + "/cuenta");	
                                                              
                                                            }
                                                          }
                                                      }
                                                    });

                                                  }else{
                                                    var errormsj= errorMessage(data);
                                                    // var errormsj= data;
                                                    $.toast({
                                                      text: errormsj,
                                                      position: 'bottom-center',
                                                      stack: false
                                                    });	
                                                  }
                                                }
                                              });	
                                            
                                          }
                                        }
                                    }
                                  });
                                  return false;
                                  
                                  // closeAll();
                                  // window.location.replace(scope_url + "/home");
                                }else{
                                  var errormsj= errorMessage(resp);
                                  // var errormsj= data;
                                  $.toast({
                                    text: errormsj,
                                    position: 'bottom-center',
                                    stack: false
                                  });	
                                }
                              }
                            });	
                          
                        }
                      }
                  }
                });
                return false;

              }		
            }
          }
            
        },
        onContentReady: function () {
          // bind to events
          console.log('load')
          var jc = this;
          this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it

            console.log('ejecuta')
          });
        }
      });
      // aliasConfirm({
      //     title: 'Consulta Express',
      //     buttons: {
      //       enviar:{
      //         text: 'uno',
      //         action: function(){
      //             console.log('clse all');
      //             closeAll();
      //         }
      //       },
      //       cerar:{
      //         text: 'dos',
      //         action: function(){
      //             console.log('clse all');
      //             closeAll();
      //         }
      //       }  
      //     }
      // });
    }
    function preguntaExpress2(){
      $.confirm({
        title: 'Consulta Express',
        theme: 'material', // 'material', 
        columnClass: 'col-md-6 col-md-offset-6',
        content: '' +
          '<form id="formExpress"  novalidate="novalidate">' +
          '<div class="form-group">' +
          '<input maxlength="70" placeholder="Título de tu consulta" class="msj1 form-control" required /><br>' +
          '<textarea rows="3" maxlength="1000" placeholder="Ingresa tu consulta..." class="msj2 form-control" required /></textarea>' +
          '</div>' +
          '<div class="modal-msj">'+
          '<div class="info"><span > límite 1000 caracteres </span></div>'+
          '<p>Te sugerimos que al momento de elaborar el texto de tu pregunta, evites escribir nombres propios y/o empresas. '+
          '<br><span class="negrita">El precio de la consulta express es de Bs. 35</span></p>'+
          '</div>'+ 
          '<div class="form-group">'+
            '<div class="col-sm-offset-2 col-sm-10">'+
              '<div class="checkbox">'+
                '<label><input type="checkbox" id="msj3" > Consulta Anónima </label>'+
              '</div>'+
            '</div>'+
          '</div>'+
          '</form>',
        buttons: {
          formSubmit: {
            text: 'Enviar',
            btnClass: 'lgapp-secondary',
            action: function () {
              var mensaje = this.$content.find('.msj1').val();
              var mensaje2 = this.$content.find('.msj2').val();
              var elm = document.getElementById('msj3');
              // console.log(elm.checked);
              // var ckb = this.$content.find('.msj3').val();

              if(!mensaje || !mensaje2){
                $.alert('Ingrese el título y detalle de su consulta.');
                return false;
              }else{

                $.confirm({
                  title: '¿Haz finalizado su pregunta?',
                  content: 'Recuerde que no podrá realizar cambios una vez enviada su pregunta.<br> Asegúrese que contiene todo lo necesario.',
                  buttons: {
                      cancel: {
                        text: 'No, editar',
                        action: function () {
                            console.log('no,editar');
                          // return false;
                        },
                      },
                      close: {
                        text: 'Si, enviar',
                        action: function(){
                          console.log('cerrando');
                          // return true;
                          Close();
                        }
                      }
                  }
              });
              return false;
                var datos = {
                    title:mensaje,
                    detalle:mensaje2,
                    anonimo: elm.checked
                };
                // var form3 = $("#formExpress").serializeArray();
                console.log(datos);
		            $.ajax({
                  url: scope_url + "/actions/a_ask.php",
                  type:'POST',
                  data: { data: JSON.stringify(datos), tipo: 'express' },
                  success: function(data){
                    
                    console.log(data);
                    if (data === "OK") {
                      console.log(data);
                      $.toast({
                        text: "Hemos enviado a tu cuentas las instrucciones para recuperar tu contraseña.",
                        position: 'bottom-center',
                        stack: false
                      });	
                      // window.location.replace(scope_url + "/home");
                    }else{
                      var errormsj= errorMessage(data);
                      // var errormsj= data;
                      $.toast({
                        text: errormsj,
                        position: 'bottom-center',
                        stack: false
                      });	
                    }
                  }
                });	
              }		
            }
          },
          cancel: {
            text: 'Cancelar',
            function () {
            //close
          },
          }
            
        },
        onContentReady: function () {
          // bind to events
          console.log('load')
          var jc = this;
          this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it

            console.log('ejecuta')
          });
        }
      });
    }

  </script>


  </body>

</html>
