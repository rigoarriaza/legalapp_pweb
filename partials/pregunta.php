<?php include_once("head.php"); ?>

<?php include_once("navbar.php"); ?>


<?php
    if ( ! isset($elementPATH)) {
        $elementPATH = realpath(__DIR__ . '/..');
        define("elementPATH", $elementPATH);
    }
    
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . '/../../actions');
		define("actionPATH", $actionPATH);
	}

	if(!isset($_SESSION)){
		session_start();
	}
	
	include_once(actionPATH . DIRECTORY_SEPARATOR . "a_getdata.php");

    $showReplica = false;
    $formDisabled = true;

    if($slug_2 == 1){
        $pregunta = getServiceData('getDetalleExpressUsuarioApp', $userData->token_session, $userData->iduserapp, (array)[$slug]);
    
        $privacidad = $pregunta->p_privacidad;
        if($pregunta->p_estado == 5){
            $today	= date('Y-m-d H:i:s');
            $eDate 	= explode(" ", $pregunta->detalle_r[0]->datetime);
	        $date 	= date('Y-m-d',strtotime($eDate[0]));
	
            $t = new DateTime($today);
            $s = new DateTime( $pregunta->detalle_r[0]->datetime);
            $fecha = $s->diff($t);

            if($fecha->d >=1){
                $showReplica = false;
                $restante = 24 - $fecha->h;
                $tiempo = "Te quedan ". $restante ." horas para realizar una réplica.";
            }else{
                $showReplica = true;
                $restante = 24 - $fecha->h;
                $tiempo = "Te quedan ". $restante ." horas para realizar una réplica.";
            }
            
        }
    
        
    }else if($slug_2 == 2){
        $pregunta = getServiceData('getDetallePreguntaDirectaUApp', $userData->token_session, $userData->iduserapp, (array)[$slug]);
    
        $privacidad = $pregunta->privacidad;
        if($pregunta->estado == 5){
            $today	= date('Y-m-d H:i:s');
            $eDate 	= explode(" ", $pregunta->detalle_r[0]->datetime);
	        $date 	= date('Y-m-d',strtotime($eDate[0]));
	
            $t = new DateTime($today);
            $s = new DateTime( $pregunta->detalle_r[count($pregunta->detalle_r)-1]->datetime);
            $fecha = $s->diff($t);

            if($fecha->d == 0 ){
                // $showReplica = false;
                $formDisabled = false;
                $restante = 24 - $fecha->h;
                $tiempo = "Te quedan ". $restante ." horas para realizar una réplica.";
            }
            
        }  
    }else if($slug_2 == 3){
        $pregunta = getServiceData('getDetalleCotizacionUsuarioApp', $userData->token_session, $userData->iduserapp, (array)[$slug]);
        $precio= '0';
        $contenido='';
        if(isset($pregunta[0]->detalle_respuesta->precio)){
            $precio = $pregunta[0]->detalle_respuesta->precio;
            $contenido = $pregunta[0]->detalle_respuesta->contenido;
        }
    }

    // var_dump($pregunta);

?>

<script>
$(document).ready(function() {
    
    // var estado = ;

    // if(estado == 5){
    //         var a = moment();
    //         var b = moment();
    //         this.tiempoRestante  = b.diff(a,  'hours');

    //         if(this.tiempoRestante > 24){
            
    //         }
            
    // }

 }); 
</script>


    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <!-- Sidebar Widgets Column -->
        <div class="col col-md-4">

          <?php include_once("sidemenu.php"); ?>

        </div>

        <!-- Blog Entries Column -->
        <div class="col-md-8">

        <!--PREGUNTAS EXPRESS-->
        <?php if($slug_2 == 1){ ?>
          <h3 class="subtitulo"><?=$pregunta->p_titulo ?></h3>
                <div class="panel-body-chat">
                    <ul class="chat">

                    <? foreach ($pregunta as $key => $value) {  
                    
                        if($key == "u_imagen"){
                            $imagen = $value;
                        }else if($key == "p_fecha"){
                            $fecha = $value;
                        }else if($key == "p_estado"){
                            $estado = $value;
                        }else if($key == "p_titulo"){
                            $titulo = $value;
                        }else if($key == "p_contenido"){
                            $contenido = $value;
                        }
                       }
                    ?>


                    <label class="lblestado <?=getEstadoClase($estado);?>"><?=getEstado($estado);?></label>
                        
                        <li class="left clearfix">
                            <span class="chat-img pull-left">
                                <img src="<?=$imagen ?>" alt="" class="img-circle" />
                            </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font"><?=$userData->u_nombre; ?> <?=$userData->u_apellido; ?></strong> <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span><?=formatDate($fecha)?></small>
                                </div>
                                <p>
                                   <?=$contenido?>

                                   <input type="hidden" value="<?=$contenido?>" id="pdetalle" />
                                </p>
                            </div>
                        </li>
                    
                     <? $i=0;
                        foreach ($pregunta->detalle_r as $key => $datos) {  
                            // var_dump($datos);
                            if($i % 2){
                                
                            ?>
                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="<?=$imagen ?>" alt="User Avatar" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                        <div class="header">
                                            <strong class="primary-font"><?=$userData->u_nombre; ?> <?=$userData->u_apellido; ?></strong> <small class="pull-right text-muted">
                                                <span class="glyphicon glyphicon-time"></span><?=formatDate($datos->datetime)?></small>
                                        </div>
                                            <?=$datos->contenido?>
                                        </p>
                                    </div>
                                </li>
                            <?
                            }else{
                                // var_dump($datos);
                                foreach($datos->profesional_info as $key => $valor){

                                    if($key == "p_nombre"){
                                        $pro_nombre = $valor;
                                    }else if($key == "p_apellido"){
                                        $pro_apellido = $valor;
                                    }else if($key == "p_foto"){
                                        $pro_foto = $valor;
                                    }
                                }
                                ?>

                                    <li class="right clearfix">
                                        <span class="chat-img pull-right">
                                            <img src="<?=$pro_foto?>" alt="User Avatar" class="img-circle" />
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <small class=" text-muted"><span class="glyphicon glyphicon-time"></span><?=formatDate($datos->datetime)?></small>
                                                <strong class="pull-right primary-font"><?=$pro_nombre;?> <?=$pro_apellido;?></strong>
                                            </div>
                                            <p>
                                                <?=$datos->contenido?>
                                                </p>
                                        </div>
                                    </li>
                                <?
                            }
                       $i++;
                       }
                    ?>

                    <!--BOTONES DE ACCIONES DISPONIBLES-->
                        <div style="width:90%; margin:15px;text-align:center;">
                        <?php if($pregunta->p_estado == 1){ ?>
                            <button onClick="cancelar('express')" class="btn lgapp-secondary btnlg-m btn-sm"> Cancelar </button>
                            <button onClick="modificar('expressm')"  class="btn btn-lgapp-primary btnlg-m btn-sm"> Modificar </button>
                            <button onClick="pagar('express')" class="btn contestada btnlg-m btn-sm" > Pagar </button>
                        <?php }
                            if($showReplica){ ?>
                            <button onClick="complementar()"  class="btn btn-lgapp-primary  btn-sm"> ¿Quieres complementar tu pregunta? </button>
                            <br><label style="color:red; font-style: oblique; font-weight: 300;"><?=$tiempo ?></label>
                        <?php } 
                            if($pregunta->p_estado == 7 && $pregunta->calificacion == 0 ){ ?>
                            
                            <div class="row">
                                <div class="col-lg-12" style="text-align:center">
                                <div class="star-rating">
                                    <span class="fa fa-star-o" data-rating="1"></span>
                                    <span class="fa fa-star-o" data-rating="2"></span>
                                    <span class="fa fa-star-o" data-rating="3"></span>
                                    <span class="fa fa-star-o" data-rating="4"></span>
                                    <span class="fa fa-star-o" data-rating="5"></span>
                                    <input type="hidden" name="whatever1" class="rating-value" value="1">
                                </div>
                                </div>
                            </div>                           
                            <br>
                            <textarea id="txtcomentario"  maxlength="500"  row="3" class="form-control input-sm" placeholder="Ingresa tu comentario, valora la atención del profesional..." ></textarea>
                            <br>
                            <button onClick="calificar()"  class="btn btn-lgapp-primary  btn-sm"> Calificar Pregunta</button>
                           
                        <?php } ?>
                        </div>     
                        
  
                    </ul>
                </div>
                <div class="panel-footer">
                    <div class="input-group">
                        <textarea id="btn-input"  maxlength="1500" <?php echo ($formDisabled)? ' disabled ': ' ' ?> row="3" class="form-control input-sm" placeholder="Enviar pregunta..." ></textarea>
                        <span class="input-group-btn">
                            <button id="btn-send" onClick="responder('express')" <?php echo ($formDisabled)? ' disabled ': ' ' ?> class="btn btn-lgapp-secondary btn-lg" >
                                <i class="fa fa-send "></i> Enviar</button>
                                
                        </span>
                    </div>
                </div>
            

        <?php }else if($slug_2 == 2){ ?>
        <!--PREGUNTAS DIRECTAS-->
          <h3 class="subtitulo">Pregunta directa a: <?=$pregunta->p_nombre?> <?=$pregunta->p_apellido?>
                                <br>
                                <?=$pregunta->pd_titulo?> </h3>
                <div class="panel-body">
                    <ul class="chat">

                    <? foreach ($pregunta as $key => $value) {  
                    
                        if($key == "p_foto"){
                            $imagen = $value;
                        }else if($key == "estado"){
                            $estado = $value;
                        }else if($key == "datetime"){
                            $fecha = $value;
                        }else if($key == "pd_titulo"){
                            $titulo = $value;
                        }else if($key == "pd_contenido"){
                            $contenido = $value;
                        }
                       }
                    ?>
                        <label class="lblestado <?=getEstadoClase($estado);?>"><?=getEstado($estado);?></label>
                        <li class="left clearfix">
                            <span class="chat-img pull-left">
                                <img src="<?=$imageUrl ?>/<?=$userData->u_foto; ?>" alt="" class="img-circle" />
                            </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font"><?=$userData->u_nombre; ?> <?=$userData->u_apellido; ?></strong> <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span><?=formatDate($fecha)?></small>
                                </div>
                                <p>
                                   <?=$contenido?>
                                   <input type="hidden" value="<?=$contenido?>" id="pdetalledirecta" />
                                            
                                </p>
                            </div>
                        </li>
                    
                     <? $i=0;
                        foreach ($pregunta->detalle_r as $key => $datos) {  
                            // var_dump($datos);
                            if($i % 2){
                                
                            ?>
                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="<?=$imageUrl ?>/<?=$userData->u_foto; ?>" alt="User Avatar" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                        <div class="header">
                                            <strong class="primary-font"><?=$userData->u_nombre; ?> <?=$userData->u_apellido; ?></strong> <small class="pull-right text-muted">
                                                <span class="glyphicon glyphicon-time"></span><?=formatDate($datos->datetime)?></small>
                                        </div>
                                        <p>
                                            <?=$datos->contenido?>
                                        </p>
                                    </div>
                                </li>
                            <?
                            }else{
                                // var_dump($datos);
                                foreach($datos->profesional_info as $key => $valor){

                                    if($key == "p_nombre"){
                                        $pro_nombre = $valor;
                                    }else if($key == "p_apellido"){
                                        $pro_apellido = $valor;
                                    }
                                }
                                ?>
                                    <li class="right clearfix">
                                        <span class="chat-img pull-right">
                                            <img src="<?=$pregunta->p_foto?>" alt="User Avatar" class="img-circle" />
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <small class=" text-muted"><span class="glyphicon glyphicon-time"></span><?=formatDate($datos->datetime)?></small>
                                                <strong class="pull-right primary-font"><?=$pro_nombre;?> <?=$pro_apellido;?></strong>
                                            </div>
                                            <p>
                                                <?=$datos->contenido?>
                                            </p>
                                        </div>
                                    </li>
                                <?
                            }
                       $i++;
                       }
                    ?>

                        <div style="width:90%; margin:15px;text-align:center;">
                           <?php if($pregunta->estado == 1){ ?>
                            <button onClick="cancelar('directa')" class="btn lgapp-secondary btnlg-m btn-sm"> Cancelar </button>
                            <button onClick="modificar('directam')"  class="btn btn-lgapp-primary btnlg-m btn-sm"> Modificar </button>
                            <button onClick="pagar('directa')" class="btn contestada btnlg-m btn-sm" > Pagar </button>
                        <?php } ?>

                        </div>
                        
                    </ul>
                </div>
                <div class="panel-footer">
                    <div class="input-group">
                        <textarea id="btn-input"  maxlength="1500" <?php echo ($formDisabled)? ' disabled ': ' ' ?> row="5" class="form-control input-sm" placeholder="Enviar pregunta..." ></textarea>
                        <span class="input-group-btn">
                            <button id="btn-send" onClick="responder('directa')" <?php echo ($formDisabled)? ' disabled ': ' ' ?> class="btn btn-lgapp-secondary btn-lg" >
                                <i class="fa fa-send "></i> Enviar</button>
                                
                        </span>
                    </div>
                </div>
            
        <?php }else if($slug_2 == 3){  ?>

        <div class="list-group">
            <a class="card" >
                <!--<div class="row">-->
                    <div class="row no-gutters">
                        <div class="col-auto">
                            <img src="<?=$pregunta[0]->pro_imagen ?>" style="width:75px !important; height:75px !important;" class="img-fluid imglist"  alt="">
                        </div>
                        <div class="col">
                            <div class="card-block px-2">
                                <div class="row">
                                    <div class="col-8">
                                        <h5 class="title-list">Abodago: <?=$pregunta[0]->pro_nombre; ?> <?=$pregunta[0]->pro_apellido; ?> </h5>
                                        
                                    </div>
                                    <div class="col-4">
                                        <label style="margin:2px;" class="lblestado <?=getEstadoClaseCot($pregunta[0]->c_estado);?>"><?=getEstadoCot($pregunta[0]->c_estado);?></label>
                        
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <h5 class="title-list">Consulta: <?=$pregunta[0]->c_titulo; ?> </h5>
                                        <p><?=$pregunta[0]->c_descripcion; ?> </p>
                                        <label class="detail-list"></label>
                                    </div>
                                    
                                    
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <h5 class="title-list">Fecha de Inicio: <?=formatDate($pregunta[0]->c_fechainicio); ?></h5>
                                    </div>
                                    <div class="col-6">
                                        <h5 class="title-list">Fecha de Fin: <?=formatDate($pregunta[0]->c_fechafin); ?></h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <h5 class="title-list">Prioridad: <?=$pregunta[0]->c_nivel; ?></h5>
                                    </div>
                                    <div class="col-6">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <h5 class="title-list">Respuesta: </h5>
                                        <p><?=$contenido ?></p>
                                        <h5 class="title-list">Precio: <?=$precio?> Bs.</h5>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!--</div>-->

                
            </a>
        </div>
        <?php }  ?>
        

        
      </div></div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include_once("foot.php"); ?>

    <!-- Bootstrap core JavaScript -->
    <!--<script src="vendor/jquery/jquery.min.js"></script>-->
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script type="text/javascript">
    const scope_url = "<?php echo $page_url; ?>";
    
    var $star_rating = $('.star-rating .fa');

    var SetRatingStar = function() {
    return $star_rating.each(function() {
        if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
        return $(this).removeClass('fa-star-o').addClass('fa-star');
        } else {
        return $(this).removeClass('fa-star').addClass('fa-star-o');
        }
    });
    };

    $star_rating.on('click', function() {
        $star_rating.siblings('input.rating-value').val($(this).data('rating'));
        
        return SetRatingStar();
    });

    SetRatingStar();
 


    var refobj = [];
    function aliasConfirm(option){
         refobj.push($.confirm(option));
    }
    function closeAll(){
       $.each(refobj, function(i, a){ 
            a.close(); 
            // you can pop the closed ones from refobj array here.
       })
    }

    function cancelar(tipo){

      aliasConfirm({
        title: '¿Estas seguro de Cancelar su pregunta?',
        content: 'Esta pregunta ya no estará disponible para los abogados y tu dinero será reintegrado a tu cuenta.',
        theme: 'material', // 'material', 
        columnClass: 'col-md-6 col-md-offset-6',
        buttons: {
         cancel: {
            text: 'No, regresa',
              function () {
              //close
            },
          },
          formSubmit: {
            text: 'Si, cancelar',
            btnClass: 'lgapp-secondary',
            action: function () {
              
                var idpregunta = <?=$slug; ?>

                          var datos = {
                                idpregunta:idpregunta
                            };
                            // var form3 = $("#formExpress").serializeArray();
                            console.log(datos);
                            $.ajax({
                              url: scope_url + "/actions/a_pregunta.php",
                              type:'POST',
                              data: { data: JSON.stringify(datos), tipo: tipo, accion: 'cancelar' },
                              success: function(resp){
                                
                                //aqui va a devolver obj respuesta id
                                console.log(resp);
                                if (resp=='OK') {
                                  
                                  //consultar nuevamente
                                  //Pagar Pregunta  //Ir a mis preguntas
                                  aliasConfirm({
                                    title: '¡Pregunta cancelada!',
                                    content: 'Tu pregunta express ha sido cancelada',
                                    buttons: {
                                        cancel: {
                                          text: 'Aceptar',
                                          action: function () {
                                             window.location.replace(scope_url + "/preguntas");
                                          }
                                        }
                                    }
                                  });
                                  return false;
                                  
                                  // closeAll();
                                  // window.location.replace(scope_url + "/home");
                                }else{
                                  var errormsj= errorMessage(resp);
                                  // var errormsj= data;
                                  $.toast({
                                    text: errormsj,
                                    position: 'top-center',
                                    stack: false
                                  });	
                                }
                              }
                            });	
                          
                        

              		
            }
          }
            
        },
        onContentReady: function () {
          // bind to events
          console.log('load')
          var jc = this;
          this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it

            console.log('ejecuta')
          });
        }
      });

    }

    function pagar(tipo){


      aliasConfirm({
        title: '¿Quieres realizar el pago de tu pregunta?',
        content: 'Vamos a verificar tu saldo para poder realizar el pago.',
        theme: 'material', // 'material', 
        columnClass: 'col-md-6 col-md-offset-6',
        buttons: {
         cancel: {
            text: 'No, regresa',
              function () {
              //close
            },
          },
          formSubmit: {
            text: 'Sí, pagar',
            btnClass: 'lgapp-secondary',
            action: function () {
                $('body').loading({message: 'Cargando...'});
                var idpregunta = <?=$slug; ?>;
                var idprofesional = <?=(isset($pregunta->idprofesional))? $pregunta->idprofesional : 0 ; ?>;

                          var datos = {
                                idpregunta:idpregunta,
                                idprofesional:idprofesional
                            };
                            // var form3 = $("#formExpress").serializeArray();
                            console.log(datos);
                            $.ajax({
                              url: scope_url + "/actions/a_pregunta.php",
                              type:'POST',
                              data: { data: JSON.stringify(datos), tipo: tipo, accion: 'pagar' },
                              success: function(resp){
                                $('body').loading('stop');
                                //aqui va a devolver obj respuesta id
                                console.log(resp);
                                if (resp=='OK') {
                                  
                                  //consultar nuevamente
                                  //Pagar Pregunta  //Ir a mis preguntas
                                  aliasConfirm({
                                    title: '¡Pregunta Pagada!',
                                    content: 'Tu pregunta ha sido pagada satisfactoriamente.',
                                    buttons: {
                                        cancel: {
                                          text: 'Aceptar',
                                          action: function () {
                                             window.location.replace(scope_url + "/preguntas");
                                          }
                                        }
                                    }
                                  });
                                  return false;
                                  
                                  // closeAll();
                                  // window.location.replace(scope_url + "/home");
                                }else if(resp == "error_saldo"){ 
                                  aliasConfirm({
                                    title: '¡Saldo insuficiente!',
                                    content: 'Para que la pregunta sea visible, confirma tu saldo y en breve un abogado te responderá.',
                                    buttons: {
                                        cancel: {
                                          text: 'Pagar pregunta',
                                          action: function () {
                                             window.location.replace(scope_url + "/user");
                                          }
                                        }
                                    }
                                  });
                                  return false;                  
                                }else{
                                  var errormsj= errorMessage(resp);
                                  // var errormsj= data;
                                  $.toast({
                                    text: errormsj,
                                    position: 'top-center',
                                    stack: false
                                  });	
                                }
                              }
                            });	
                          
                        

              		
            }
          }
            
        },
        onContentReady: function () {
          // bind to events
          console.log('load')
          var jc = this;
          this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it

            console.log('ejecuta')
          });
        }
      });

    }
    
   
    function modificar(tipo){
    
        if(tipo == 'expressm'){
            var titulomodal = 'Modificar Consulta Express';
            var titulo = "<?=$titulo; ?>";
            var detall = $('#pdetalle').val();
            var etiqueta = 'El precio de la consulta express es de Bs. 35';
        }else{
            var titulomodal = 'Modificar Consulta Directa';
            var titulo = "<?=$titulo; ?>";
            var detall = $('#pdetalledirecta').val();
            var idprofesional = "<?=(isset($pregunta->idprofesional))? $pregunta->idprofesional : 0 ; ?>";
            var etiqueta = '';
        }
        var privacidad = <?=$privacidad ?>;
        var anonimo = '';
        if(privacidad == 1){
            anonimo = 'checked';
        }
        
      aliasConfirm({
        title: titulomodal,
        theme: 'material', // 'material', 
        columnClass: 'col-md-6 col-md-offset-6',
        content: '' +
          '<form id="formExpress"  novalidate="novalidate">' +
          '<div class="form-group">' +
          '<input maxlength="70" placeholder="Título de tu consulta" class="msj1 form-control" required value="'+titulo+'" /><br>' +
          '<textarea  rows="8" maxlength="1000" placeholder="Ingresa tu consulta..." class="msj2 form-control" required  >'+detall+'</textarea>' +
          '</div>' +
          '<div class="modal-msj">'+
          '<div class="info"><span > límite 1000 caracteres </span></div>'+
          '<p>Te sugerimos que al momento de elaborar el texto de tu pregunta, evites escribir nombres propios y/o empresas. '+
          '<br><span class="negrita">'+etiqueta+'</span></p>'+
          '</div>'+ 
          '<div class="form-group">'+
            '<div class="col-sm-offset-2 col-sm-10">'+
              '<div class="checkbox">'+
                '<label><input type="checkbox" id="msj3" '+anonimo+' > Consulta Anónima </label>'+
              '</div>'+
            '</div>'+
          '</div>'+
          '</form>',
        buttons: {
         cancel: {
            text: 'Cancelar',
              function () {
              //close
            },
          },
          formSubmit: {
            text: 'Enviar',
            btnClass: 'lgapp-secondary',
            action: function () {
              var mensaje = this.$content.find('.msj1').val();
              var mensaje2 = this.$content.find('.msj2').val();
              var elm = document.getElementById('msj3');
              // console.log(elm.checked);
              // var ckb = this.$content.find('.msj3').val();

              if(!mensaje || !mensaje2){
                $.alert('Ingrese el título y detalle de su consulta.');
                return false;
              }else{

                aliasConfirm({
                  title: '¿Haz finalizado su pregunta?',
                  content: 'Recuerde que no podrá realizar cambios una vez enviada su pregunta.<br> Asegúrese que contiene todo lo necesario.',
                  buttons: {
                      cancel: {
                        text: 'No, editar',
                        action: function () {
                            console.log('no,editar');
                          // return false;
                        },
                      },
                      close: {
                        text: 'Si, enviar',
                        btnClass: 'lgapp-secondary',
                        action: function(){
                          console.log('cerrando');
                          // return true;
                          
                            var datos = {
                                id: <?=$slug; ?>,
                                title:mensaje,
                                detalle:mensaje2,
                                anonimo: elm.checked,
                                idprofesional: idprofesional
                            };
                            // var form3 = $("#formExpress").serializeArray();
                            console.log(datos);
                            $.ajax({
                              url: scope_url + "/actions/a_ask.php",
                              type:'POST',
                              data: { data: JSON.stringify(datos), tipo: tipo },
                              success: function(resp){
                                
                                //aqui va a devolver obj respuesta id
                                console.log(resp);
                                if (resp=="OK") {
                                  
                                  //consultar nuevamente
                                  //Pagar Pregunta  //Ir a mis preguntas
                                  aliasConfirm({
                                    title: '¡Pregunta modificada!',
                                    content: 'Tu pregunta ha sido modificada. <br>Recuerda que para que la pregunta sea visible, confirma tu saldo y en breve un abogado te responderá.',
                                    buttons: {
                                        cancel: {
                                          text: 'Aceptar',
                                          action: function () {
                                             closeAll();
                                             window.location.reload();
                                          },
                                        },
                                        close: {
                                          text: 'Pagar pregunta',
                                          btnClass: 'lgapp-secondary',
                                          action: function(){
                                            
                                            if(tipo == 'expressm'){
                                                tipopago = 'setPayExpress';
                                            }else{
                                                tipopago = 'setPayDirecta';
                                            }

                                            
                                              $.ajax({
                                                url: scope_url + "/actions/a_getmore.php",
                                                type:'POST',
                                                data: { id: <?=$slug; ?>, tipo: tipopago },
                                                success: function(data){
                                                  
                                                  console.log(data);
                                                  if (data === "OK") {
                                                    if(tipo == 'expressm'){
                                                        msjrespuesta = "Tu pregunta ha sido enviada a la comunidad de abogados. <br><br>En breve un abogado te responderá. ";
                                                    }else{
                                                        msjrespuesta = 'Tu pregunta directa ha sido pagada satisfactoriamente.';
                                                    }                                      
                                                    $.toast({
                                                      text: msjrespuesta,
                                                      heading: '¡Pregunta enviada!',
                                                      position: 'top-center',
                                                      loaderBg: '#C1262E',
                                                      bgColor: 'white',
                                                      textColor: 'grey',
                                                      stack: false,
                                                      hideAfter: 10000
                                                    });	                                                    
                                                    closeAll();
                                                    window.location.reload();;
                                                  
                                                  }else if (data === "error_saldo") {

                                                    aliasConfirm({
                                                      title: '¡Saldo insuficiente!',
                                                      content: 'Tu saldo actual no es suficiente para pagar la pregunta.',
                                                      buttons: {
                                                          cancel: {
                                                            text: 'Aceptar',
                                                            action: function () {
                                                               closeAll();
                                                            },
                                                          },
                                                          close: {
                                                            text: 'Pagar pregunta',
                                                            btnClass: 'lgapp-secondary',
                                                            action: function(){
                                                              
                                                                window.location.replace(scope_url + "/cuenta");	
                                                              
                                                            }
                                                          }
                                                      }
                                                    });

                                                  }else{
                                                    var errormsj= errorMessage(data);
                                                    // var errormsj= data;
                                                    $.toast({
                                                      text: errormsj,
                                                      position: 'top-center',
                                                      stack: false
                                                    });	
                                                  }
                                                }
                                              });	
                                            
                                          }
                                        }
                                    }
                                  });
                                  return false;
                                  
                                  // closeAll();
                                  // window.location.replace(scope_url + "/home");
                                }else{
                                  var errormsj= errorMessage(resp);
                                  // var errormsj= data;
                                  $.toast({
                                    text: errormsj,
                                    position: 'bottom-center',
                                    stack: false
                                  });	
                                }
                              }
                            });	
                          
                        }
                      }
                  }
                });
                return false;

              }		
            }
          }
            
        },
        onContentReady: function () {
          // bind to events
          console.log('load')
          var jc = this;
          this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it

            console.log('ejecuta')
          });
        }
      });

    }

    function complementar(){
      aliasConfirm({
        title: '¿Quieres complementar tu pregunta?',
        content: 'Recuerda que tienes derecho a complementar una vez tu pregunta. El complemento a una pregunta debe ser derivada de la pregunta original.',
        theme: 'material', // 'material', 
        columnClass: 'col-md-6 col-md-offset-6',
        buttons: {
         cancel: {
            text: 'No hace falta, gracias',
              function () {
              //close
            },
          },
          formSubmit: {
            text: 'Sí, me encantaría',
            btnClass: 'lgapp-secondary',
            action: function () {
                $("#btn-send").removeAttr("disabled");
                $("#btn-input").removeAttr("disabled");
              		
            }
          }
            
        }
      });
    }

    function responder(tipo){      

      aliasConfirm({
        title: '¿Has finalizado tu respuesta?',
        content: 'Recuerda que no podrás realizar cambios una vez enviada tu pregunta. Asegúrate que contiene todo lo necesario.',
        theme: 'material', // 'material', 
        columnClass: 'col-md-6 col-md-offset-6',
        buttons: {
         cancel: {
            text: 'No, editar',
              function () {
              //close
            },
          },
          formSubmit: {
            text: 'Si, enviar',
            btnClass: 'lgapp-secondary',
            action: function () {
              
                var idpregunta = <?=$slug; ?>;
                if(tipo == 'express'){
                     var idprofesional = <?=(isset($pregunta->detalle_r[0]->profesional_info->idprofesional))? $pregunta->detalle_r[0]->profesional_info->idprofesional : 0 ; ?>;
                }else{
                    var idprofesional = <?=(isset($pregunta->idprofesional))? $pregunta->idprofesional : 0 ; ?>;
                }
                var descripcion = $("#btn-input").val();
                var datos = {
                    idpregunta:idpregunta,
                    idprofesional:idprofesional,
                    detalle: descripcion
                };
                            // var form3 = $("#formExpress").serializeArray();
                            console.log(datos);
                            $('body').loading({message: 'Cargando...'});
                            $.ajax({
                              url: scope_url + "/actions/a_pregunta.php",
                              type:'POST',
                              data: { data: JSON.stringify(datos), tipo: tipo, accion: 'responder' },
                              success: function(resp){
                                $('body').loading('stop');
                                //aqui va a devolver obj respuesta id
                                console.log(resp);
                                if (resp=='OK') {
                                  
                                  //consultar nuevamente
                                  //Pagar Pregunta  //Ir a mis preguntas
                                  aliasConfirm({
                                    title: 'Pregunta enviada!',
                                    content: 'Tu respuesta ha sido enviada',
                                    buttons: {
                                        cancel: {
                                          text: 'Aceptar',
                                          action: function () {
                                            closeAll();
                                             window.location.reload();
                                          }
                                        }
                                    }
                                  });
                                  return false;
                                  
                                }else{
                                  var errormsj= errorMessage(resp);
                                  // var errormsj= data;
                                  $.toast({
                                    text: errormsj,
                                    position: 'top-center',
                                    stack: false
                                  });	
                                }
                              }
                            });	
              		
            }
          }
            
        }
      });

    }

    function calificar(){
        var tipo = 'express';
        
  
              
                var idpregunta = <?=$slug; ?>;
                var calificacion = $(".rating-value").val();
                var comentario = $("#txtcomentario").val();
                var datos = {
                        idpregunta:idpregunta,
                        calificacion:calificacion,
                        comentario:comentario

                 };
                            // var form3 = $("#formExpress").serializeArray();
                            console.log(datos);
                            $.ajax({
                              url: scope_url + "/actions/a_pregunta.php",
                              type:'POST',
                              data: { data: JSON.stringify(datos), tipo: tipo, accion: 'calificar' },
                              success: function(resp){
                                
                                //aqui va a devolver obj respuesta id
                                console.log(resp);
                                if (resp=='OK') {
                                  
                                  //consultar nuevamente
                                  //Pagar Pregunta  //Ir a mis preguntas
                                  aliasConfirm({
                                    title: '¡Calificación realizada!',
                                    content: '¡Gracias por puntuar el servicio, ayudará a nuestra mejora constante!',
                                    buttons: {
                                        cancel: {
                                          text: 'Aceptar',
                                          action: function () {
                                             window.location.reload();
                                          }
                                        }
                                    }
                                  });
                                  return false;
                                  
                                  // closeAll();
                                  // window.location.replace(scope_url + "/home");
                                }else if(resp == "error_saldo"){ 
                                  aliasConfirm({
                                    title: '¡Saldo insuficiente!',
                                    content: 'Para que la pregunta sea visible, confirma tu saldo y en breve un abogado te responderá.',
                                    buttons: {
                                        cancel: {
                                          text: 'Pagar pregunta',
                                          action: function () {
                                             window.location.replace(scope_url + "/user");
                                          }
                                        }
                                    }
                                  });
                                  return false;                  
                                }else{
                                  var errormsj= errorMessage(resp);
                                  // var errormsj= data;
                                  $.toast({
                                    text: errormsj,
                                    position: 'top-center',
                                    stack: false
                                  });	
                                }
                              }
                            });	
                          
                    

    }
    

  </script>


  </body>

</html>
