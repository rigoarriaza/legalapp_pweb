<?php include_once("head.php"); ?>

<?php include_once("navbar.php"); ?>

<?php
    if ( ! isset($elementPATH)) {
        $elementPATH = realpath(__DIR__ . '/..');
        define("elementPATH", $elementPATH);
    }
    
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . '/../../actions');
		define("actionPATH", $actionPATH);
	}

	if(!isset($_SESSION)){
		session_start();
	}
	
	include_once(actionPATH . DIRECTORY_SEPARATOR . "a_getdata.php");


    $ingresos = getServiceDataCommun('getHistorialGananciasProfesionalApp', (array)[$userData->idprofesional, $userData->token_session]);
	
	$suscripciones = getServiceDataCommun('getHistorialRecargasProfesionalApp', (array)[$userData->idprofesional, $userData->token_session]);
	
	$cobros = getServiceDataCommun('getHistorialCobrosProfesionalApp', (array)[$userData->idprofesional, $userData->token_session]);
	
    // var_dump($cobros);

?>



    <!-- Page Content -->
    <div class="container">


      <div class="row">
        <!-- Sidebar Widgets Column -->
        <div class="col col-md-4">


          <?php include_once("sidemenu.php"); ?>


          <br>
          <div class="list-group menu-side">
            <h5 class="card-header">¿Quieres saber cómo te pagamos?  </h5>
              <a class="card" href="#">
                  <div class="row no-gutters">
                      <div class="col" >
                          <div class="card-block px-2">

                            <a href="#" target="_blank" class="list-group-item ">
                                Transacción Fee: 6,0%
                            </a> 
                            <a href="#" target="_blank" class="list-group-item ">
                                 Impuestos: 16,5%
                            </a> 
                            <a href="#" target="_blank" class="list-group-item ">
                                 Comisión LegalApp: 7,5%
                            </a> 
                            <a href="#" target="_blank" class="list-group-item ">
                                 Ganancia: 70,0%
                            </a>                          

                              


                          </div>
                      </div>
                  </div>
                  
              </a>
          </div>


        </div>

        <!-- Blog Entries Column -->
        <div class="col-md-6">

            <h3 class="subtitulo">Ingresos por servicios</h3>

 
           <div class="list-group" id="result_express">

            <?php

            if(isset($ingresos) && $ingresos== 'no_data'){?>

				<div class="error" >
					<label > <i class="fa fa-search"></i> No hay datos</label>
				</div>

			<?	
			}else if(isset($ingresos) && count($ingresos) > 0){
                foreach ($ingresos as $ingreso) {      
                
                foreach ($ingreso as $key => $value) {
                    // print "$key => $value\n";
                    
                    
                    if($key == "r_tipo"){
                        $r_tipo = $value;
                    }else if($key == "r_datetime"){
                      $fecha_pregunta = $value;
                    }else if($key == "r_valor"){
                      $deb_cantidad = $value;
                    }else if($key == "deb_fecha"){
                      $deb_fecha = $value;
                    }else if($key == "id_deb"){
                      $id_deb = $value;
                    }
					
                }
                ?>
                <div class="card" >
                    <div class="row no-gutters">
                        <!--<div class="col-auto">
                            <img src="" style="width:75px !important; height:75px !important;" class="img-fluid imglist"  alt="">
                        </div>-->
                        <div class="col">
                            <div class="card-block px-2">
								
                                <a class="row" data-toggle="collapse" data-target="#collapse<?=$id_deb; ?>" aria-expanded="true" aria-controls="collapse<?=$id_deb; ?>">
                                    <div class="col-8">
                                        <h5 class="title-list">Servicio: <?=$r_tipo; ?> </h5>
                                        <label class="detail-list">Fecha de pregunta: <?=formatDate($fecha_pregunta); ?> </label>
                                    </div>
                                    <div class="col-4" style="padding: 5px;">
										<label class="precio"> <?=$deb_cantidad; ?> Bs </label>
										
                                    </div>
                                </a>
								<div class="row" id="collapse<?=$id_deb; ?>" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="col-8">
                                    </div>
                                    <div class="col-4" style="padding: 5px;">
										
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>


                
                <?
                }

                
            }

            ?>      

          </div>    


<!--suscripciones-->
            <h3 class="subtitulo">Suscripciones</h3>
 
           <div class="list-group" id="result_express">

            <?php

            if(isset($suscripciones) && $suscripciones== 'no_data'){?>

				<div class="error" >
					<label > <i class="fa fa-search"></i> No hay datos</label>
				</div>

			<?	
			}else if(isset($suscripciones) && count($suscripciones) > 0){
                foreach ($suscripciones as $suscripcion) {      
                
                foreach ($suscripcion as $key => $value) {
                    // print "$key => $value\n";
                    
                    
                    if($key == "r_tipo"){
                        $r_tipo = $value;
                    }else if($key == "r_datetime"){
                      $fecha_pregunta = $value;
                    }else if($key == "r_valor"){
                      $deb_cantidad = $value;
                    }else if($key == "deb_fecha"){
                      $deb_fecha = $value;
                    }else if($key == "r_plan"){
                      $r_plan = $value;
                    }else if($key == "r_referencia"){
                      $r_referencia = $value;
                    }
					
                }
                ?>
                <div class="card" >
                    <div class="row no-gutters">
                        <!--<div class="col-auto">
                            <img src="" style="width:75px !important; height:75px !important;" class="img-fluid imglist"  alt="">
                        </div>-->
                        <div class="col">
                            <div class="card-block px-2">
								
                                <a class="row" >
                                    <div class="col-8">
                                        <h5 class="title-list">Plan: <?=$r_plan; ?> </h5>
                                        <label class="detail-list">Fecha de pregunta: <?=formatDate($fecha_pregunta); ?> </label>
                                    </div>
                                    <div class="col-4" style="padding: 5px;">
										<label class="precio"> <?=$deb_cantidad; ?> Bs </label>
										
                                    </div>
                                </a>
								<div class="row" id="collapse<?=$id_deb; ?>" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="col-8">
										<label class="detail-list">No. Referencia: <?=$r_referencia; ?> </label>
                                    	<label class="detail-list">Tipo de transacción: <?=$r_tipo; ?> </label>
                                    </div>
                                    <div class="col-4" style="padding: 5px;">
										
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>


                
                <?
                }

                
            }

            ?>      

          </div>    
 
<!--Monetizaciones /cobros-->
            <h3 class="subtitulo">Monetizaciones</h3>
 
           <div class="list-group" id="result_express">

            <?php

            if(isset($cobros) && $cobros== 'no_data'){?>

				<div class="error" >
					<label > <i class="fa fa-search"></i> No hay datos</label>
				</div>

			<?	
			}else if(isset($cobros) && count($cobros) > 0){
                foreach ($cobros as $cobro) {      
                
                foreach ($cobro as $key => $value) {
                    // print "$key => $value\n";
                    
                    
                    if($key == "r_datetime"){
                      $fecha = $value;
                    }else if($key == "r_valor"){
                      $deb_cantidad = $value;
                    }else if($key == "r_tipo"){
                      $tipo = $value;
                    }else if($key == "id_deb"){
                      $id_deb = $value;
                    }
					
                }
                ?>
                <div class="card" >
                    <div class="row no-gutters">
                        <!--<div class="col-auto">
                            <img src="" style="width:75px !important; height:75px !important;" class="img-fluid imglist"  alt="">
                        </div>-->
                        <div class="col">
                            <div class="card-block px-2">
								
                                <a class="row">
                                    <div class="col-8">
                                        <h5 class="title-list"> <?=$tipo; ?> </h5>
                                        <label class="detail-list">Fecha de cobro: <?=formatDate($fecha); ?> </label>
                                    </div>
                                    <div class="col-4" style="padding: 5px;">
										<label class="precio"> <?=$deb_cantidad; ?> Bs </label>
										
                                    </div>
                                </a>
								
                            </div>
                        </div>
                    </div>
                    
                </div>


                
                <?
                }

                
            }

            ?>      

          </div>  
 
        </div>

        <div class="col-md-2">
          <div class="list-group menu-side">
            <h5 class="card-header">Mi Saldo</h5>
              <a class="card2" href="#">
                  <div class="row no-gutters">
                      <div class="col" >
                          <div class="card-block px-2">

                              <h4 class="title-list" style="font-size:2em"><?=$userData->u_ganancia; ?> Bs<label class="detail-list"> </label></h4>
                              <p>Total por servicios</p>  
                          </div>
                      </div>
                  </div>
                  
              </a>
          </div>


        </div>


      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include_once("foot.php"); ?>

  <script type="text/javascript">
    const scope_url = "<?php echo $page_url; ?>";
 $(document).ready(function() {
    $("#pago").hide();


 });
    function pagoshow(){
        $("#pago").show();
        $("#btnpago").hide();
        
    }

	function update(){

        var form = $("#update-form").serializeArray();
		
    	var datos = objectifyForm(form)
		$.ajax({
			url: scope_url + "/actions/a_update.php",
			type:'POST',
            data: { data: JSON.stringify(datos), tipo: 1 },
            success: function(data){
				console.log(data);
				if (data === "OK") {
					window.location.replace(scope_url + "/user");
				}else{
					var errormsj= errorMessage(data);
					$.toast({
						heading: 'Error',
						text: '<span style="font-size:18px;"></span> '+errormsj,
						position: 'bottom-center',
						loaderBg: '#C1262E',
						bgColor: 'white',
    					textColor: 'grey',
						stack: false,
						hideAfter: 6000
					});	
				}
            }

		});
    }     
	    
	function updateFoto(){

        // var form = $("#foto-form").serializeArray();
		// var base64 = getBase64Image(document.getElementById("imageid"));
		// var base64 = getBase64Image($("#files").val());
        // var ruta = $("#files").val();
    	
        // console.log(base64);
        var preview = document.getElementById("fperfil");
        var file    = document.querySelector('input[type=file]').files[0];
        var reader  = new FileReader();

        reader.addEventListener("load", function () {
            preview.src = reader.result;
            setFoto(reader.result);
        }, false);

        if (file) {
            reader.readAsDataURL(file);

            // console.log(datos);
        }


    }

    function setFoto(img){
        $.ajax({
			url: scope_url + "/actions/a_foto.php",
			type:'POST',
            data: { foto: img , tipo: 1 },
            success: function(data){
				console.log(data);
				if (data === "OK") {
					window.location.replace(scope_url + "/user");
				}else{
					var errormsj= errorMessage(data);
					$.toast({
						heading: 'Error',
						text: '<span style="font-size:18px;"></span> '+errormsj,
						position: 'bottom-center',
						loaderBg: '#C1262E',
						bgColor: 'white',
    					textColor: 'grey',
						stack: false,
						hideAfter: 6000
					});	
				}
            }

		});
    }

    function setClave(){

		$.confirm({
			title: 'Cambiar contraseña',
			content: '' +
			'<form action="" class="formClave">' +
			'<div class="form-group">' +
			'<input type="password" placeholder="Nueva Contraseña" class="pass form-control" required /><br>' +
			'<input type="password" placeholder="Confirmar Contraseña" class="pass2 form-control" required />' +
			'</div>' +
			'</form>',
			buttons: {
				formSubmit: {
					text: 'Enviar',
					btnClass: 'lgapp-secondary',
					action: function () {
						var pass = this.$content.find('.pass').val();
						var pass2 = this.$content.find('.pass2').val();
						if(!pass || !pass2){
							$.alert('Ingresa ambos campos.');
							return false;
						}else if(pass != pass2){
							$.alert('Las contraseñas no coinciden');
							return false;
						}else{
							$.ajax({
								url: scope_url + "/actions/a_passchange.php",
								type:'POST',
								data: { password: pass, tipo:1 },
								success: function(data){
									console.log(data);
									if (data === "OK") {
										$.toast({
											heading: 'Contraseña actualizada',
											text: "La contraseña se cambio correctamente.",
											position: 'bottom-center',
											loaderBg: '#C1262E',
											bgColor: 'white',
											textColor: 'grey',
											stack: false,
											hideAfter: 6000
										});	
									}else{
										var errormsj= errorMessage(data);
										$.toast({
											heading: 'Error',
											text: '<span style="font-size:18px;"></span> '+errormsj,
											position: 'bottom-center',
											loaderBg: '#C1262E',
											bgColor: 'white',
											textColor: 'grey',
											stack: false,
											hideAfter: 6000
										});	
									}
								}
							});	
						}		
					}
				},
				cancel: {
					text: 'Cancelar',
					function () {
					//close
				},
				}
			},
			onContentReady: function () {
				// bind to events
				var jc = this;
				this.$content.find('form').on('submit', function (e) {
					// if the user submits the form by pressing enter in the field.
					e.preventDefault();
					jc.$$formSubmit.trigger('click'); // reference the button and click it
				});
			}
		});
    } 

    function pagarTarjeta(){

        $.alert('Redireccionando a plataforma de pago');
        
        var carnet = <?=$userData->u_carnet; ?>;

        window.location.replace("http://multipago.bo/service/legal_app/external/start/"+carnet+"/1");
        
        
    } 

    function pagarBanco(){

		$.confirm({
			title: 'Pago en Banco',
			content: 'Estimado usuario, puedes pagar en cualquiera de las sucursales del Banco Fassil. Solicita al cajero pagar el servicio de Legal App para preguntas Express o preguntas Directas. Tienes que presentar tu cédula de identidad.',
			buttons: {
				formSubmit: {
					text: 'Aceptar',
					btnClass: 'lgapp-primary',
					action: function () {

						
						//close	
					}
				},
				cancel: {
					text: 'No, regresar',
					function () {
					//close
				},
				}
					
			}
		});

    }

  </script>

  </body>

</html>
