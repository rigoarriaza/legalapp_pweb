<?php include_once("head.php"); ?>

<?php include_once("navbar.php"); ?>
<?php include_once("validar.php"); ?>
<?php
    if ( ! isset($elementPATH)) {
        $elementPATH = realpath(__DIR__ . '/..');
        define("elementPATH", $elementPATH);
    }
    
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . '/../../actions');
		define("actionPATH", $actionPATH);
	}

	if(!isset($_SESSION)){
		session_start();
	}
    if($mensaje == 0){
      $examenes = getServiceData('getExamenesDisponiblesProfesional',$userData->token_session,$userData->idprofesional, (array)[0,10] );
    }else{
      $examenes = 'no_data';
    }

?>    

    
    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <!-- Sidebar Widgets Column -->
        <div class="col col-md-3">

          <?php include_once("sidemenu.php"); ?>

        </div>

        <!-- Blog Entries Column -->
        <div class="col-md-6">

          
              <h3 class="subtitulo">Exámenes disponibles</h3>


 
           <div class="list-group" id="result_express">

            <?php

            if(isset($examenes) && $examenes== 'no_data'){?>

				<div class="error" >
					<label > <i class="fa fa-search"></i> No hay datos</label>
				</div>

			<?	
			}else if(isset($examenes) && count($examenes) > 0){
                foreach ($examenes as $examen) {      
                
                foreach ($examen as $key => $value) {
                    // print "$key => $value\n";
                    
                    
                    if($key == "r_tipo"){
                        $r_tipo = $value;
                    }else if($key == "r_datetime"){
                      $fecha_pregunta = $value;
                    }else if($key == "r_valor"){
                      $deb_cantidad = $value;
                    }else if($key == "deb_fecha"){
                      $deb_fecha = $value;
                    }else if($key == "id_deb"){
                      $id_deb = $value;
                    }
					
                }
                ?>
                <div class="card" >
                    <div class="row no-gutters">
                        <!--<div class="col-auto">
                            <img src="" style="width:75px !important; height:75px !important;" class="img-fluid imglist"  alt="">
                        </div>-->
                        <div class="col">
                            <div class="card-block px-2">
								
                                <a class="row" data-toggle="collapse" data-target="#collapse<?=$id_deb; ?>" aria-expanded="true" aria-controls="collapse<?=$id_deb; ?>">
                                    <div class="col-8">
                                        <h5 class="title-list">Servicio: <?=$r_tipo; ?> </h5>
                                        <label class="detail-list">Fecha de pregunta: <?=formatDate($fecha_pregunta); ?> </label>
                                    </div>
                                    <div class="col-4" style="padding: 5px;">
										<label class="precio"> <?=$deb_cantidad; ?> Bs </label>
										
                                    </div>
                                </a>
								<div class="row" id="collapse<?=$id_deb; ?>" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="col-8">
                                    </div>
                                    <div class="col-4" style="padding: 5px;">
										
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>


                
                <?
                }

                
            }

            ?>      

          </div> 

          

        </div>
                <!-- Sidebar Widgets Column -->
        <div class="col col-md-3">

          <h3 class="subtitulo">Exámenes logrados</h3>
          <?  if(isset($examenes) && $examenes== 'no_data'){?>

            <div class="error" >
              <label > <i class="fa fa-search"></i> No hay datos</label>
            </div>

          <?			}?>
        </div>

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include_once("foot.php"); ?>

    <!-- Bootstrap core JavaScript -->
    <!--<script src="vendor/jquery/jquery.min.js"></script>-->
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script type="text/javascript">


  </script>
   

  </body>

</html>