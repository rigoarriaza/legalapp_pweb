<!doctype html>
<html lang="en">
    <head>
        
        <?php include_once("metatags.php"); ?>
        
        
        <!-- Bootstrap core CSS -->
        <link href="<?= $page_url ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <script src="<?= $page_url ?>/js/jquery.min.js"></script>
        
        <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
        <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
        
        <!--jquery confirm message-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

        <!--toast message-->
        <link rel="stylesheet" href="<?= $page_url ?>/css/jquery.toast.css">
        <script type="text/javascript" src="<?= $page_url ?>/js/jquery.toast.js"></script>
  
        <!--local functions-->
        <script src="<?= $page_url ?>/js/jform.js"></script>
        <script src="<?= $page_url ?>/js/moments.min.js"></script>
        <script src="<?= $page_url ?>/js/loading.js"></script>
        <script src="<?= $page_url ?>/css/loading.css"></script>

        <!-- Custom styles for this template -->
        <link href="<?= $page_url ?>/css/estilo.css" rel="stylesheet">   

        <meta http-equiv="content-type" content="text/html; charset=utf-8"></meta>
        <link rel="icon" type="image/png" href="<?= $page_url ?>/assets/favicon.ico">
        
    </head>
    <body class="bgGrey <?= $page_name ?>">

