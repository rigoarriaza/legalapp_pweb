<?php include_once("head.php"); ?>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-legal fixed-top">
      <div class="container">
        <a class="navbar-brand"  href="#">
          <img class="logo" src="assets/logo-s.png" >
        </a>
        <h3>Legal App</h3>
        
      </div>
    </nav>
    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            </div>
        <div class="col-md-8 noselect">
            <br><br>
            <h2>  Términos y Condiciones LEGAL APP </h2>


            <h3>Relación contractual</h3>
            <p>Las presentes condiciones de uso (“Condiciones”) regulan el acceso o uso que usted haga, como persona, desde cualquier país del mundo, de aplicaciones, páginas web, contenido, productos y servicios (los “Servicios”) puestos a disposición por por Legal App, empresa que funciona conforme a las leyes bolivianas (“Legal App”).
            </p>

            <h3> Terminología </h3>
            <p>
            Usuarios: Es el conjunto de personas, sea e-Lawyer o Legal Seeker, que utiliza la plataforma de Legal App para prestar o requerir servicios. 
            </p>
            <p> e-Lawyer: Abogado profesional suscrito a la plataforma de Legal App para prestar sus servicios de asesoría legal por cuenta propia y a requerimiento del Legal Seeker. 
            </p>
            <p>A efectos de los presentes términos y condiciones, también podrá denominarse “e-Lawyer” a los gestores de trámites legales, sean profesionales o no, suscritos a la plataforma de Legal App para brindar sus servicios por cuenta propia según requerimiento del Legal Seeker. 
            </p>
            <p>Legal Seeker: Persona natural que utiliza la plataforma de Legal App para realizar consultas de índole legal y/o requiere asesoramiento y servicios legales de cualquier e-Lawyer suscrito a la presente plataforma. 
            </p>
            <p>Servicios: Es el conjunto de prestaciones que ofrece la plataforma de Legal App al cual pueden acceder tanto el e-Lawyer como el Legal Seeker. 
            </p>
            <p>Plataforma: Establecimiento virtual, sea aplicación o página web, mediante el cual se brindan los servicios de Legal App. LEA DETENIDAMENTE  LAS SIGUIENTE CONDICIONES PREVIAMENTE AL ACCESO DE LOS SERVICIOS.
            </p>
            <p>Mediante su acceso y uso de los Servicios usted acuerda vincularse jurídicamente por estas Condiciones, que establecen una relación contractual entre usted y Legal App. Si usted no acepta estas Condiciones, no podrá acceder o usar los Servicios. Estas Condiciones sustituyen expresamente los acuerdos o compromisos previos con usted. Legal App podrá poner fin de inmediato a estas Condiciones o cualquiera de los Servicios respecto de usted o, en general, dejar de ofrecer o denegar el acceso a los Servicios o cualquier parte de ellos, en cualquier momento y por cualquier motivo.
            </p>
            <p>Se podrán aplicar condiciones suplementarias a determinados Servicios, como políticas para un evento, una actividad o una promoción particular, y dichas condiciones suplementarias se le comunicarán en relación con los Servicios aplicables. Las condiciones suplementarias se establecen además de las Condiciones, y se considerarán una parte de estas, para los fines de los Servicios aplicables. Las condiciones suplementarias prevalecerán sobre las Condiciones en el caso de conflicto con respecto a los Servicios aplicables.
            </p>
            <p>Legal App podrá modificar las Condiciones relativas a los Servicios cuando lo considere oportuno. Las modificaciones serán efectivas después de la publicación por parte de Legal App de dichas Condiciones actualizadas en esta ubicación o las políticas modificadas o condiciones suplementarias sobre el Servicio aplicable. Su acceso o uso continuado de los Servicios después de dicha publicación constituye su consentimiento a vincularse por las Condiciones y sus modificaciones.
            </p>
            <p>La recopilación y el uso que hacemos de la información personal en relación con los Servicios son en conformidad con la Política de Privacidad de Legal App.
            </p>
            <p>Legal App podrá facilitar a un procesador de reclamaciones, a una aseguradora y/o a alguna autoridad o Tribunal, cualquier información necesaria (incluida su información de contacto) si hubiera quejas, disputas, conflictos y/o procesos legales, que pudieran derivar del uso de la presente plataforma, implicándole a usted y/o a un tercero que la mencionada información o los datos en cuestión fueren necesarios para resolver la queja, disputa, conflicto y/o proceso legal.
            </p>

            Los Servicios
            <p>Los Servicios constituyen otorgar una plataforma de tecnología que permite a los usuarios de aplicaciones móviles de Legal App o páginas web proporcionadas como parte de los Servicios, en los cuales puedan demandar y ofertar servicios legales, principalmente, la elaboración  de consultas de índole legal o solicitud de cotizaciones por servicios legales, entre otros servicios relativos. 
            </p>
            <p>Solo en caso que  Legal App  acuerde y acepte mediante la suscripción de un contrato separado y por escrito con usted, un tenor distinto con lo estipulado en las presentes condiciones, se entiende que los Servicios se encuentran  a disposición sólo para su uso personal y no así comercial. USTED RECONOCE QUE LEGAL APP NO PRESTA SERVICIOS LEGALES O FUNCIONA COMO UNA EMPRESA DE SERVICIOS LEGALES Y QUE LOS MENCIONADOS SERVICIOS LEGALES SE PRESTAN POR CUENTA DE TERCEROS CONTRATISTAS INDEPENDIENTES, QUE NO ESTÁN EMPLEADOS NI SON DEPENDIENTES LABORALES DE LEGAL APP NI POR NINGUNA DE SUS AFILIADAS.
            </p>
            <p>En ese contexto, se establece que: Legal App no se responsabiliza por la calidad y el resultado final de los trabajos ofertados por el e-Lawyer; Legal App no se responsabiliza por la transgresión al Secreto Profesional ocasionado por los profesionales; Legal App no se responsabiliza de las infracciones cometidas por el profesional, con relación a la oferta de sus servicios de forma engañosa o referencias anticipadas sobre tiempo o resultado.
            </p>

            Restricciones.

            <p>Los usuarios no podrán realizar las siguientes acciones dentro de la plataforma de Legal App ni en el marco del uso de la misma: 
            </p>
            <p>a)	Proporcionar información, por ningún medio, como ser número telefónico o cualquier información referencial que permita establecer contacto directo entre el e-Lawyer y el Legal Seeker fuera de la plataforma, en el marco de los servicios ofrecidos por Legal App, salvo estos datos sean proporcionados directamente por Legal App al usuario, en los casos que corresponda.  
            b)	Hacer Preguntas o brindar respuestas que tiendan a evadir las leyes vigentes del país donde reside.
            c)	Transferir saldo de su cuenta a otro Legal Seeker.
            d)	Responder preguntas por fuera de la plataforma, a consultas realizadas, dentro de plataforma..
            e)	Retirar cualquier nota de derechos de autor, marca registrada u otra nota de propiedad de cualquier parte de los Servicios.
            f)	Reproducir, modificar, preparar obras derivadas sobre los Servicios, distribuir, licenciar, arrendar, revender, transferir, exhibir públicamente, presentar públicamente, transmitir, retransmitir o explotar de otra forma los Servicios, excepto como se permita expresamente por Legal App.
            g)	Descompilar, realizar ingeniería inversa o desmontar los Servicios, excepto como se permita por la ley aplicable.
            h)	 Enlazar, reflejar o enmarcar cualquier parte de los Servicios
            i)	Causar o lanzar cualquier programa o script con el objeto de extraer, indexar, analizar o de otro modo realizar prospección de datos de cualquier parte de los Servicios o sobrecargar o bloquear indebidamente la operación y/o funcionalidad de cualquier aspecto de los Servicios.
            j)	Intentar obtener un acceso no autorizado o dañar cualquier aspecto de los Servicios o sus sistemas o redes relacionados.
            k)	 Los e-Lawayer -en el ejercicio de sus ofertas de servicios - no podrán menoscabar la integridad y el prestigio de los demás e-Lawayer o abogados en general.
            l)	La información al público destinada a hacer conocer la cualidad profesional del e-Lawyer se limitará a señalar los servicios ofrecidos y la especialidad.
            </p>
            <p>El Servicio no está disponible para el uso de personas menores de 16 años. No podrá ceder o transferir de otro modo su Cuenta a cualquier otra persona o entidad. Usted acuerda cumplir con todas las leyes aplicables al utilizar los Servicios y solo podrá utilizar los Servicios con fines legítimos. 
            </p>

            Titularidad.

            <p>Los Servicios y todos los derechos relativos a estos son y permanecerán de  propiedad de Legal App o de sus licenciantes. Ninguna de estas Condiciones ni el uso de los Servicios le transfiere u otorgan ningún derecho: (i) sobre o con relación a los Servicios, excepto las respuestas brindadas o recibidas, según corresponda; o bien (ii) a utilizar o mencionar en cualquier modo a los nombres de empresa, logotipos, nombres de producto y servicio, marcas comerciales o marcas de servicio de Legal App o de sus licenciantes.
            </p>
            Uso de los Servicios
            Cuentas de usuario


            <p>Con el fin de usar los Servicios, usted debe registrarse y mantener activa una cuenta personal de usuario de los Servicios (“Cuenta”). Para obtener una Cuenta, usted debe tener capacidad legal. Si desea registrarse como Legal Seeker, debe tener como mínimo 16 años de edad, o tener la mayoría de edad legal en su jurisdicción. Para obtener una cuenta como e-Lawyer, debe ser titulado, como mínimo, con una licenciatura en Derecho y estar inscrito en el Registro Público de Abogados del Ministerio de Justicia, excepto para los casos de registro de gestores, cuando corresponda. El registro de la cuenta le requiere que comunique a Legal App determinada información personal, como su nombre, dirección, número de teléfono móvil, así como por lo menos un método de pago válido (bien sea una tarjeta de crédito, débito, prepago, cuenta con un procesador de pagos efectivo en las ciudades en que este método esté disponible). Asimismo, deberá enviar a Legal App los requisitos de suscripción a la Plataforma. Usted se compromete a mantener la información en su Cuenta de forma exacta, completa y actualizada. Si no mantiene la información de Cuenta de forma exacta, completa y actualizada, incluso el tener un método de pago inválido o que haya vencido, podrá resultar en su imposibilidad para acceder y utilizar los Servicios o en la resolución por parte de Legal App de este Acuerdo celebrado con usted. 
            </p>
            <p>Legal App se reserva el derecho de usar cualquier acción legal posible para identificar a los usuarios, así como requerir, en cualquier momento, documentación extra que se considere apropiado para verificar la información personal del usuario.
            </p>
            <p>Usted es responsable de toda la actividad que ocurre en su Cuenta y declara que la información brindada en la creación o actualización de la misma es veraz y actual. 
            </p>
            <p>En todo caso usted será el único responsable de las manifestaciones falsas o inexactas que realice y de los perjuicios que cause a la plataforma o a terceros por la información que facilite.
            </p>
            <p>Usted se compromete a actuar en forma responsable en la plataforma y a tratar a otros usuarios con respeto.
            </p>
            <p>Asimismo, usted se compromete a mantener en todo momento de forma segura y secreta el nombre de usuario y la contraseña de su Cuenta. 
            </p>
            <p>Legal App no se responsabiliza por cualquier daño como resultado de la pérdida o mal uso de la clave por parte de terceros. El usuario es el único responsable por ello.
            </p>
            <p>A menos que Legal App permita otra cosa por escrito, usted solo puede poseer una Cuenta como Legal Seeker y/o una cuenta como e-Lawyer, cuando corresponda.
            </p>
            <p>Usted acepta que se le podrá denegar el acceso o uso de los Servicios si se niega a facilitar alguno de los datos o documentos requeridos para la verificación o creación de su cuenta personal.
            </p>
            <p>Asimismo, usted acepta que Legal App podrá suspender su cuenta o suscripción por las siguientes causales:
            </p>
            <p>•	Cuando el e-Lawyer seleccionó más de 10 preguntas y las mismas no fueron contestadas dentro del plazo. 
            •	Cuando el e-Lawyer reciba 3 llamadas de atención por proporcionar sus datos personales para ser contactado directamente por el Legal Seeker, dentro de una consulta. 
            •	Por falta de pago de la suscripción por parte del e-Lawyer, cuando corresponda. 
            •	Cuando el usuario proceda a realizar actos que incurran en falta de respeto o amenazas de cualquier tipo que Legal App considere como tal. 
            </p>
            <p>Al suspender una cuenta Legal App le quitará el acceso definitivo a toda la información generada por su persona. 
            </p>
            <p>Legal App podrá también suspender temporalmente algunos accesos dentro de la plataforma cuando el e-Lawyer no haya finalizado el proceso de respuesta o ante situaciones análogas. 
            </p>
            <p>De igual manera, el usuario podrá cancelar su cuenta de manera voluntaria con las herramientas brindadas dentro de la plataforma. 

            Mensajes de texto.

            <p>Al crear una Cuenta, usted acepta que los Servicios le puedan enviar mensajes de texto informativos (SMS), notificaciones mediante la aplicación móvil y/o correos electrónicos, como parte de la actividad comercial normal de su uso de los Servicios. 

            Códigos promocionales.

            <p>Legal App podrá, a su sola discreción, crear códigos promocionales que podrán ser canjeados por crédito de Cuenta u otros elementos o beneficios relacionados con los Servicios y/o los servicios de un Tercero proveedor, con sujeción a cualquier condición adicional que Legal App establezca sobre la base de cada código promocional (“Códigos promocionales”). Usted acuerda que los Códigos promocionales: (i) deben usarse para la audiencia y el propósito deseado, y de manera lícita; (ii) no podrán duplicarse, venderse o transferirse de ninguna manera, o ponerse a disposición del público general (tanto si se publican en un foro público como de otra forma), a menos que sea con el permiso de Legal App; (iii) podrán ser invalidados por Legal App en cualquier momento por cualquier motivo sin responsabilidad para Legal App; (iv) podrán usarse solo conforme a las condiciones específicas que Legal App establezca para dicho Código promocional; (v) no son válidos como efectivo; y (vi) podrán caducar antes de que usted los utilice. Legal App se reserva el derecho de retener o deducir el crédito u otros elementos o beneficios obtenidos a través de la utilización de los Códigos promocionales por usted o cualquier otro usuario en el caso de que Legal App determine o crea que el uso o el canje de los Códigos promocionales fue de modo erróneo, fraudulento, ilegal o infringiendo las condiciones del Código promocional o las presentes Condiciones.
            </p>

            Contenido proporcionado por el Usuario.

            <p>Legal App podrá, a su sola discreción, permitirle cuando considere oportuno, que envíe, cargue, publique o de otro modo ponga a disposición de Legal App a través de los Servicios, contenido e información de texto, audio y/o visual, incluidos comentarios y opiniones relativos a los Servicios. Todo Contenido de usuario facilitado por usted, incluyendo consultas, respuestas, asesoramiento, y/o informes, seguirá siendo de su propiedad. No obstante, al proporcionar Contenido de usuario a Legal App, usted otorga una licencia mundial, perpetua, irrevocable, transferible, libre de regalías, con derecho a sublicenciar, usar, copiar, modificar, crear obras derivadas, distribuir, exhibir públicamente, presentar públicamente o de otro modo explotar de cualquier manera dicho Contenido de usuario en todos los formatos y canales de distribución, conocidos ahora o ideados en un futuro (incluidos en relación con los Servicios y el negocio de Legal App y en sitios y servicios de terceros), sin más aviso o consentimiento de usted y sin requerirse el pago a usted o a cualquier otra persona o entidad.
            </p>
            <p>Usted declara y garantiza que: (i) es el único y exclusivo propietario de todo el Contenido de usuario o que tiene todos los derechos, licencias, consentimientos y permisos necesarios para otorgar a Legal App la licencia al Contenido de usuario como está establecido líneas arriba; y (ii) ni el Contenido de usuario ni su presentación, carga, publicación o puesta a disposición de otro modo de dicho Contenido de usuario, ni el uso por parte de Legal App del Contenido de usuario como está aquí permitido, infringirán, malversarán o violarán la propiedad intelectual o los derechos de propiedad de un tercero o los derechos de publicidad o privacidad o resultarán en la violación de cualquier ley o reglamento aplicable.
            </p>
            <p>Usted acuerda no proporcionar Contenido de usuario que sea difamatorio, calumnioso, odioso, violento, obsceno, pornográfico, ilícito o de otro modo ofensivo, como determine Legal App, a su sola discreción, tanto si dicho material pueda estar protegido o no por la ley. Legal App podrá, a su sola discreción y en cualquier momento y por cualquier motivo, sin avisarle previamente, revisar, controlar o eliminar Contenido de usuario, pero sin estar obligada a ello.Acceso a la red y dispositivos.
            </p>
            <p>Usted es responsable de obtener el acceso a la red de datos necesario para utilizar los Servicios. Podrán aplicarse las tarifas y tasas de datos y mensajes de su red móvil si usted accede o utiliza los Servicios desde un dispositivo inalámbrico y usted será responsable de dichas tarifas y tasas. Usted es responsable de adquirir y actualizar el hardware compatible o los dispositivos necesarios para acceder y utilizar los Servicios y Aplicaciones y cualquier actualización de estos. Legal App no garantiza que los Servicios, o cualquier parte de estos, funcionen en cualquier hardware o dispositivo particular. Además, los Servicios podrán ser objeto de disfunciones o retrasos inherentes al uso de Internet y de las comunicaciones electrónicas.
            </p>

            Formas de Pago.

            <p>Usted entiende que el uso de los Servicios deriva en cargos por los servicios que reciba (“Cargos”) incluyendo cargos por suscripción, por consulta, por cotizaciones o por otros ítems creados por Legal App a través del tiempo y debidamente comunicados al usuario.  
            </p>
            <p>Estos cargos, pueden ser modificados en cualquier momento por Legal App. Toda modificación será comunicada a los Usuarios para su conocimiento. 
            </p>
            <p>Cuando se realicen cotizaciones y se cobre el fee correspondiente, el e-Lawyer declara que ese será su costo final. Por su parte, el Legal Seeker declara haber dejado en claro el alcance del servicio por el cual pide cotización. En caso de haber cobros fuera de los establecidos en la cotización por modificación del alcance por parte del Legal Seeker, el pago de los mismos, se definirá fuera de la Plataforma y Legal App no se hace responsable por el fee pagado.
            </p>
            <p>Para que usted acceda a los servicios, los mismos deberán ser cancelados de manera previa a su uso, para lo cual, Legal App facilitará diferentes medios de pago. Legal App está establecido en Bolivia, por lo que es posible que los bancos del país donde usted utiliza el servicio puedan aplicar tarifas de conversión de monedas y de uso internacional que no se reflejarán en la plataforma o el correo electrónico enviado por Legal App. Los Cargos incluirán los impuestos aplicables cuando se requiera por ley. Los Cargos pagados por usted son definitivos y no reembolsables, a menos que Legal App determine lo contrario, para lo cual, previamente usted deberá llenar el Formulario de Reclamos. Los usuarios son los únicos responsables de pagar sus deudas, ya sea a través de sí mismos o de terceros. Los pagos deberán hacerse a las cuentas que Legal App provea por sus medios oficiales, por lo cual, Legal App no será responsable en caso de error en los depósitos realizados por los usuarios. Todos los Cargos son pagaderos inmediatamente y el pago se facilitará por Legal App utilizando el método de pago preferido indicado en su Cuenta, y después de ello Legal App le enviará un recibo por correo electrónico. Si se determina que el método de pago, establecido como principal en su Cuenta ha caducado, es inválido o de otro modo no sirve para cobrarle, usted acepta que Legal App, como agente de cobro, utilice un método de pago secundario en su Cuenta, si estuviera disponible.
            </p>
            <p>Legal App, en cualquier momento y a su sola discreción, se reserva el derecho de establecer, eliminar y/o revisar los Cargos para alguno o todos los servicios ofertados en la Plataforma. 
            </p>
            <p>Legal App podrá, cuando lo considere oportuno, proporcionar a determinados usuarios ofertas promocionales y descuentos que pueden resultar en el cobro de diferentes importes por estos o similares servicios o bienes obtenidos a través del uso de los Servicios.
            </p>
            <p>Esta estructura de pago está destinada para compensar plenamente al e-Lawyer por los servicios proporcionados. 
            </p>

            Evaluación del e-Lawyer 


            <p>Es la elección del Legal Seeker de evaluar al e-Lawyer que le brindó el servicio, asignando puntos y comentando sobre el servicio brindado por el e-Lawyer. Dicha evaluación será de uso exclusivo de Legal App con la finalidad de brindar un mejor servicio al Legal Seeker, y pueden también bloquear al e-Lawyer en caso de que éste tenga muchas quejas o una puntuación menor a 2 estrellas. Legal App se reserva el derecho de usar las evaluaciones para mejorar el servicio, sea para uso interno o externo.
            Renuncias; Limitación de responsabilidad; Indemnidad.
            </p>

            Renuncia.-

            <p>Los servicios se proporcionan “tal cual” y “según estén disponibles”. Legal App renuncia a toda declaración y garantía, expresa, implícita o estatutaria, no expresamente establecida en estas condiciones, incluidas las garantías implícitas de comerciabilidad, idoneidad para un fin particular y no violación. Además, Legal App no hace declaración ni presta garantía alguna relativa a la fiabilidad, puntualidad, calidad, idoneidad o disponibilidad de los servicios o cualquiera de los servicios o bienes solicitados a través del uso de los servicios, o que los servicios no serán interrumpidos o estarán libres de errores. Legal App no garantiza la calidad, idoneidad, seguridad o habilidad del e-Lawyer. Usted acuerda que todo riesgo derivado de su uso de los servicios y cualquier servicio o bien solicitado en relación con aquellos será únicamente suyo, en la máxima medida permitida por la ley aplicable.
            </p>

            Limitación de responsabilidad.

            <p>Legal App no garantiza que el sistema esté disponible sin interrupciones y que siempre esté libre de errores y por tanto, no se responsabiliza por el daño causado a los usuarios.
            </p>
            <p>Usted acepta que Legal App tampoco se responsabiliza por cualquier daño o pérdida causada a su computadora o dispositivo móvil como resultado de usar la aplicación o página web.
            </p>
            <p>Legal App no se responsabiliza por cualquier error y/u inconsistencia de información con otros sistemas independientes.
            </p>
            <p>Asimismo, Legal App no será responsable de daños indirectos, incidentales, especiales, ejemplares, punitivos o emergentes, incluidos el lucro cesante, la pérdida de datos, ni de perjuicios relativos o de otro modo derivados de cualquier uso de los servicios, incluso aunque Legal App haya sido advertido de la posibilidad de dichos daños. Legal App no será responsable de cualquier daño, responsabilidad o pérdida que deriven de: (i) su uso o dependencia de los servicios o su incapacidad para acceder o utilizar los servicios; o (ii) cualquier transacción o relación entre usted y cualquier tercero proveedor, aunque Legal App hubiera sido advertido de la posibilidad de dichos daños. Legal App no será responsable del retraso o de la falta de ejecución resultante de causas que vayan más allá del control razonable de Legal App. En ningún caso la responsabilidad total de Legal App hacia usted en relación con los servicios por todos los daños, las pérdidas y los juicios podrá exceder de quinientos dólares estadounidenses ($500).
            </p>
            <p>Las limitaciones y la renuncia en este apartado no pretenden limitar la responsabilidad o alterar sus derechos como consumidor que no puedan excluirse según la ley aplicable.
            </p>

            Indemnidad.

            <p>Usted acuerda mantener indemnes y responder frente a Legal App, socios, representantes legales y sus consejeros, directores, empleados y agentes por cualquier reclamación, demanda, pérdida, responsabilidad y gasto (incluidos los honorarios de abogados) que deriven de: (i) su uso de los Servicios o servicios o bienes obtenidos a través de su uso de los Servicios; (ii) su incumplimiento o violación de cualquiera de estas Condiciones o la legislación vigente; (iii) el uso por parte de Legal App de su Contenido de usuario; o (iv) su infracción de los derechos de cualquier tercero, incluidos Terceros proveedores.
            </p>

            Legislación aplicable; Arbitraje.

            <p>Salvo que aquí se especifique lo contrario, las presentes Condiciones se regirán e interpretarán exclusivamente en virtud de la legislación boliviana.
            </p>
            <p>Cualquier disputa, conflicto, reclamación o controversia, del tipo que sea, que resulte de las presentes Condiciones o que se relacione en gran parte con ellas, incluyendo las relativas a su validez, interpretación y exigibilidad (cualquier “Disputa”), deberán someterse forzosamente a procedimientos de mediación directa entre las partes.
            </p>
            <p>Si dicha disputa no fuese solucionada en un plazo de treinta (30) días desde la fecha en la que se formalice la solicitud de mediación directa mediante carta, se hará referencia a dicha disputa y se solucionará exclusiva y definitivamente mediante arbitraje institucional a efectuarse en el Centro de Conciliación y Arbitraje Comercial de CAINCO de la ciudad de Santa Cruz de la Sierra, al que se encomienda la administración del arbitraje conforme al Reglamento vigente a la fecha en que se presente la solicitud de arbitraje, lo que implica la renuncia expresa a iniciar cualquier proceso judicial. Las partes acuerdan que el arbitraje estará a cargo de un árbitro único. 

            Otras disposiciones
            Notificaciones.


            <p>Legal App podrá notificar por medio de una notificación general en los Servicios, mediante un correo electrónico enviado a su dirección electrónica en su Cuenta o por comunicación escrita enviada a su dirección o a su teléfono móvil, según lo dispuesto en su Cuenta. Usted podrá notificar a Legal App por comunicación escrita a la dirección de correo electrónico de  Legal App: info@legalbo.com. 
            </p>

            Disposiciones generales.

            <p>Usted no podrá ceder ni transferir estas Condiciones, en todo o en parte, sin el consentimiento previo por escrito de Legal App. Usted da su aprobación a Legal App para ceder o transferir estas Condiciones, en todo o en parte, incluido a: (i) una subsidiaria o un afiliado; (ii) un adquirente del capital, del negocio o de los activos de Legal App; o (iii) un sucesor por fusión. No existe entre usted, Legal App o cualquier Tercer proveedor una empresa conjunta o relación de socios, relación laboral, empleo o agencia como resultado del contrato entre usted y Legal App o del uso de los Servicios.
            </p>
            <p>Si cualquier disposición de estas Condiciones se considerara ilegal, nula o inexigible, ya sea en su totalidad o en parte, de conformidad con cualquier legislación, dicha disposición o parte de esta se considerará que no forma parte de estas Condiciones, aunque la legalidad, validez y exigibilidad del resto de las disposiciones de estas Condiciones no se verá afectada. En ese caso, las partes deberán reemplazar dicha disposición ilegal, nula o inexigible, en todo o en parte por una disposición legal, válida y exigible que tenga, en la medida de lo posible, un efecto similar al que tenía la disposición ilegal, nula o inexigible, dados los contenidos y el propósito de estas Condiciones. Estas Condiciones constituyen el contrato íntegro y el entendimiento entre las partes en relación con el objeto y sustituye y reemplaza a todos los contratos o acuerdos anteriores o contemporáneos en relación con dicho objeto. En estas Condiciones, las palabras “incluido/a/os/as” e “incluye/n” significan “incluido, de forma meramente enunciativa”.
            </p>

            <!--politicas de privacidad-->

            <h1>  Política De Privacidad</h1>

            <h2>Política De Privacidad</h2>
            <p>
            Por medio de nuestra política de privacidad le ponemos al tanto de las debidas condiciones de uso en la aplicación de Legal App.
            </p>
            <p> La utilización de los servicios implica su aceptación plena y sin reservas a todas y cada una de las disposiciones incluidas en este Aviso Legal, por lo que, si usted no está de acuerdo con cualquiera de las condiciones aquí establecidas, no deberá usar u/o acceder a la aplicación y/o servicios.
            </p>
            <p>Reservamos el derecho a modificar esta Declaración de Privacidad en cualquier momento. Su uso continuo de la aplicación tras la notificación o anuncio de tales modificaciones constituirá su aceptación de tales cambios.
            </p>

            <h3>Política De Protección De Datos Personales</h3>
            <p>Para utilizar los servicios de Legal App, deberá proporcionar previamente ciertos datos de carácter personal, que solo serán utilizados para el propósito que fueron recopilados.
            </p>
            <p>El tipo de la posible información que se le sea solicitada incluye, de manera enunciativa más no limitativa, su nombre, foto de perfil, dirección de correo electrónico (e-mail), género, teléfono e información profesional. No toda la información solicitada al momento de registrarse es obligatoria de proporcionarse, salvo aquella que consideremos conveniente y que así se le haga saber.
            </p>
            <p>Como principio general, este sitio no comparte ni revela información obtenida, excepto cuando haya sido autorizada por usted, o en los siguientes casos:
            </p>
            <p>I) Cuando le sea requerido por una autoridad competente y previo el cumplimiento del trámite legal correspondiente y II) Cuando a juicio de este sitio sea necesario para hacer cumplir las condiciones de uso y demás términos de esta plataforma, o para salvaguardar la integridad de los demás usuarios o del sitio.
            </p>
            <p>Deberá estar consciente de que, si usted voluntariamente revela información personal en línea en un área pública, esa información puede ser recogida y usada por otros. Legal App no controla las acciones de los usuarios.
            </p>

        </div>
</body>

</html>
