<?php include_once("head.php"); ?>

<div id="fb-root"></div>
<script></script>

<script>
	$(document).ready(function(){
		window.fbAsyncInit = function() {
			FB.init({
			appId      : '347110125765907',
			cookie     : true,
			xfbml      : true,
			version    : 'v3.2'
			});
			
			FB.AppEvents.logPageView();   
			
			FB.getLoginStatus(function(response) {
				statusChangeCallback(response);
			});
		};

		(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v3.2&appId=347110125765907&autoLogAppEvents=1';
		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

		function checkLoginState() {
				FB.getLoginStatus(function(response) {
					statusChangeCallback(response);
				});
		}


	});
</script>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-legal fixed-top">
      <div class="container" style="text-align:left;">
        <a class="navbar-brand"  href="#">
          <img class="logo" src="assets/logo-s.png" >
        </a>
		<h1 class="titulo">Legal App</h1>
        <!--<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>-->
        <div class="collapse navbar-collapse" id="navbarResponsive">
        
        </div>
      </div>
    </nav>


<div class="" >
    <!--<div class="row">-->
		<!--<div>
		</div>-->
	    <div class="divi">
            <article id="log-user">
                <img src="assets/normal_user-s.png" >
                <h6>Iniciar como</h6>
                <h3>Persona Natural</h3>
                
				<div>

                <div class="row">
					<div class="col-1 col-md-4">
					</div>
					<div class="col-10 col-md-4 panel-login">
						<div class="row">
							<div id="tab1" class="col-6 col-lg-6 pestana activa">
								<a href="#" class="active" id="login-form-link">Iniciar sesión</a>
							</div>
							<div id="tab2" class="col-6 col-lg-6  pestana">
								<a href="#" id="register-form-link">Registrarse</a>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<form id="login-form"  novalidate="novalidate">
									<br>
									<div class="form-group">
										
										<!--<label for="exampleInputEmail1">
											Correo electrónico
										</label>-->
										<input type="email" class="form-control" id="email" name="email"  placeholder="Correo electrónico"/>
										<input type="hidden" id="tipo" name="tipo" value="1" />
									</div>
									<div class="form-group">
										
										<!--<label for="exampleInputPassword1">
											Contraseña
										</label>-->
										<input type="password" class="form-control" id="password" name="password" placeholder="Contraseña" />
									</div>
									
									<div class="row">
										
										<div class="col-md-6">
											<div class="checkbox">
												<label  class="lap-lbl12">
													<!--<input type="checkbox" /> Recordar datos de sesión-->
												</label>
											</div> 
										</div>
										<div class="col-md-6"  style="text-align:right;">
												<label   class="lap-lbl12 lbl-clave" onClick="recuperarClave(1);">Olvidé mi clave</label>
										</div>

									</div>
								
									<div class="row">
										
										<div class="col-md-4">
											<button type="button" onClick="login(1);"   class="btn lgapp-primary">
												Ingresar
											</button>
										</div>
										<div class="col-md-8"  style="text-align:right;">
											<div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false">Iniciar con Facebook</div>
										</div>

									</div>
								

									<!--<fb:login-button 
									scope="public_profile,email"
									onlogin="checkLoginState();">
									</fb:login-button>
									<button type="button" onClick="checkLoginState();"   class="btn lgapp-fb">
										Inicia con Facebook
									</button>-->
								</form>
								<form id="register-form"  style="display: none;">
									<br>
									<input type="hidden" id="tipoingreso" name="tipo" value="1" />
									<div class="form-group">
										<input type="text" name="nombre" id="nombre" tabindex="1" class="form-control" placeholder="Nombres" value="">
									</div>
									<div class="form-group">
										<input type="text" name="apellido" id="apellido" tabindex="2" class="form-control" placeholder="Apellidos" value="">
									</div>
									<div class="form-group">
										<input type="email" name="correo3" id="correo3" tabindex="3" class="form-control" placeholder="Correo">
									</div>
									<div class="form-group">
										<input type="text" name="celular" id="celular" tabindex="4" class="form-control" placeholder="Teléfono">
									</div>
									<div class="form-group">
										<input type="password" name="password3" id="password3" tabindex="5" class="form-control" placeholder="Contraseña">
									</div>

									<div class="checkbox">
										<label  class="lap-lbl12">
											<input type="checkbox"  tabindex="6" id="terminosuser" name="terminosuser" onClick="terminos();" /> Acepto los <span class="lbl-link" onClick="terminos_link();">  Términos y Condiciones de Uso y Políticas de Privacidad </span>
										</label>
									</div> 
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
											
												<button type="button" onClick="registrarse(1);"   class="btn lgapp-primary">
													Registrarse
												</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-1 col-md-4">
					</div>
				</div>              

                </div>
            </article>
            
            <article  id="log-pro">
                <img src="assets/lega_user-s.png" >
                <h6>Iniciar como</h6>
                <h3>Profesional</h3>

                <div>
                <div class="row">
					<div class="col-1 col-md-4">
					</div>
					<div class="col-10 col-md-4 panel-login">
						<div class="row">
							<div id="tab3" class="col-6 col-lg-6 pestana activa">
								<a href="#" class="active" id="login-form-link2">Iniciar sesión</a>
							</div>
							<div id="tab4" class="col-6 col-lg-6  pestana">
								<a href="#" id="register-form-link2">Registrarse</a>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<form id="login-form2"  novalidate="novalidate">
									<br>
									<div class="form-group">
										
										<!--<label for="exampleInputEmail1">
											
										</label>-->
										<input type="email" placeholder="Correo electrónico" class="form-control" id="email2" name="email2" />
										
									</div>
									<div class="form-group">
										
										<!--<label for="exampleInputPassword1">
											
										</label>-->
										<input type="password" placeholder="Contraseña"   class="form-control" id="password2" name="password2" />
									</div>
									
									
									<div class="row">
										
										<div class="col-md-6">
											<div class="checkbox">
												<label  class="lap-lbl12">
													<!--<input type="checkbox" /> Recordar datos de sesión-->
												</label>
											</div> 
										</div>
										<div class="col-md-6"  style="text-align:right;">
												<label   class="lap-lbl12 lbl-clave" onClick="recuperarClavePro(2);">Olvidé mi clave</label>
										</div>

									</div>

									<button type="button" onClick="login(2);"   class="btn lgapp-primary">
										Ingresar
									</button>
								</form>
								<form id="register-form2"  style="display: none;">
									<br>
									<div class="form-group">
										<input type="text" name="nombre2" id="nombre2" tabindex="1" class="form-control" placeholder="Nombres" value="">
									</div>
									<div class="form-group">
										<input type="email" name="apellido2" id="apellido2" tabindex="2" class="form-control" placeholder="Apellidos" value="">
									</div>
									<div class="form-group">
										<input type="text" name="correo4" id="correo4" tabindex="3" class="form-control" placeholder="Correo">
									</div>
									<div class="form-group">
										<input type="password" name="password4" id="password4" tabindex="5" class="form-control" placeholder="Contraseña">
									</div>

									<div class="checkbox">
										<label  class="lap-lbl12">
											<input type="checkbox"  tabindex="6" id="terminospro" name="terminospro" onClick="terminos();" /> Acepto los <span class="lbl-link" onClick="terminos_link();">  Términos y Condiciones de Uso y Políticas de Privacidad </span>
										</label>
									</div> 
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
											
												<button type="button" onClick="registrarse(2);"   class="btn lgapp-primary">
													Registrarse
												</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-1 col-md-4">
					</div>
				</div>              
            
				</div>
            </article>
        
	    </div>
    <!--</div>-->
</div>


<script type="text/javascript">	
	const scope_url = "<?php echo $page_url; ?>";
    
	function login( tipo ){

		if(tipo == 1){
			var form = $("#login-form").serializeArray();
		}else{
			console.log(tipo);
			var form = $("#login-form2").serializeArray();
		}
    	var datos = objectifyForm(form)
		$.ajax({
			url: scope_url + "/actions/a_login.php",
			type:'POST',
            data: { data: JSON.stringify(datos), tipo: tipo },
            success: function(data){
				console.log(data);
				if (data === "OK") {
					console.log(data);
						if(tipo == 1){
							window.location.replace(scope_url + "/home");
						}else{
							window.location.replace(scope_url + "/homepro");
						}
				}else{
					var errormsj= errorMessage(data);
					// var errormsj= data;
					$.toast({
						heading: 'Error',
						text: '<span style="font-size:18px;"></span> '+errormsj,
						position: 'top-center',
						loaderBg: '#C1262E',
						bgColor: 'white',
    					textColor: 'grey',
						stack: false,
						hideAfter: 5000
					});	
				}
            }

		});
    } 
	
	function registrarse( tipo ){
		
		if(tipo == 1){
			var form = $("#register-form").serializeArray();
		}else{
			var form = $("#register-form2").serializeArray();
		}
    	var datos = objectifyForm(form);
		console.log(datos);
		$.ajax({
			url: scope_url + "/actions/a_register.php",
			type:'POST',
            data: { data: JSON.stringify(datos), tipo:tipo },
                success: function(data){
						console.log(data);
					if (data === "OK") {
						console.log(data);
						loginRegistro(tipo);
						
					}else{
						var errormsj= errorMessage(data);
						$.toast({
							
							heading: 'Error',
							text: '<span style="font-size:18px;"></span> '+errormsj,
							position: 'bottom-center',
							loaderBg: '#C1262E',
							bgColor: 'white',
							textColor: 'grey',
							stack: false,
							hideAfter: 6000
						});	
					}
                }
            
		});
    }

	function loginRegistro( tipo ){
		if(tipo == 1){
			email = $('#correo3').val();
			pass = $('#password3').val();
			var datos = {
				email: email,
				password: pass
			}
		}else{
			email = $('#correo4').val();
			pass = $('#password4').val();
			var datos = {
				email2: email,
				password2: pass
			}			
		}

		$.ajax({
			url: scope_url + "/actions/a_login.php",
			type:'POST',
            data: { data: JSON.stringify(datos), tipo: tipo },
            success: function(data){
				console.log(data);
				if (data === "OK") {
					console.log(data);
						if(tipo == 1){
							window.location.replace(scope_url + "/home");
						}else{
							window.location.replace(scope_url + "/homepro");
						}
				}else{
					var errormsj= errorMessage(data);
					// var errormsj= data;
					$.toast({
						heading: 'Error',
						text: '<span style="font-size:18px;"></span> '+errormsj,
						position: 'top-center',
						loaderBg: '#C1262E',
						bgColor: 'white',
    					textColor: 'grey',
						stack: false,
						hideAfter: 5000
					});	
				}
            }

		});
    } 

	function recuperarClave(tipo){
		$.confirm({
			title: '¿Olvidaste tu contraseña?',
			content: '' +
			'<form action="" class="formRecuperar">' +
			'<div class="form-group">' +
			'<label>Se enviarán las indicaciones al correo registrado</label>' +
			'<input type="text" placeholder="Correo electrónico" class="emailrecovery form-control" required />' +
			'</div>' +
			'</form>',
			buttons: {
				formSubmit: {
					text: 'Enviar',
					btnClass: 'lgapp-primary',
					action: function () {
						var emailrecovery = this.$content.find('.emailrecovery').val();
						if(!emailrecovery){
							$.alert('Ingresa una cuenta de correo.');
							return false;
						}else{
							$.ajax({
								url: scope_url + "/actions/a_passrecovery.php",
								type:'POST',
								data: { email: emailrecovery, tipo:tipo },
								success: function(data){
									console.log(data);
									if (data === "OK") {
										console.log(data);
										$.toast({
											heading: 'Correo enviado',
											text: "Hemos enviado a tu cuentas las instrucciones para recuperar tu contraseña.",
											position: 'bottom-center',
											loaderBg: '#C1262E',
											bgColor: 'white',
											textColor: 'grey',
											stack: false,
											hideAfter: 6000
										});	
									}else{
										var errormsj= errorMessage(data);
										$.toast({
											heading: 'Error',
											text: '<span style="font-size:18px;"></span> '+errormsj,
											position: 'bottom-center',
											loaderBg: '#C1262E',
											bgColor: 'white',
											textColor: 'grey',
											stack: false,
											hideAfter: 6000
										});	
									}
								}
							});	
						}		
					}
				},
				cancel: {
					text: 'Cancelar',
					function () {
					//close
				},
				}
			},
			onContentReady: function () {
				// bind to events
				var jc = this;
				this.$content.find('form').on('submit', function (e) {
					// if the user submits the form by pressing enter in the field.
					e.preventDefault();
					jc.$$formSubmit.trigger('click'); // reference the button and click it
				});
			}
		});
	}

	function recuperarClavePro(tipo){
		$.confirm({
			title: '¿Olvidaste tu contraseña?',
			content: '' +
			'<form action="" class="formRecuperarPro">' +
			'<div class="form-group">' +
			'<label>Se enviarán las indicaciones al correo registrado</label>' +
			'<input type="text" placeholder="Correo electrónico" class="emailrecovery2 form-control" required />' +
			'</div>' +
			'</form>',
			buttons: {
				formSubmit: {
					text: 'Enviar',
					btnClass: 'lgapp-primary',
					action: function () {
						var emailrecovery2 = this.$content.find('.emailrecovery2').val();
						if(!emailrecovery2){
							$.alert('Ingresa una cuenta de correo.');
							return false;
						}else{
							$.ajax({
								url: scope_url + "/actions/a_passrecovery.php",
								type:'POST',
								data: { email: emailrecovery2, tipo:tipo },
								success: function(data){
									console.log(data);
									if (data === "OK") {
										console.log(data);
										$.toast({
											heading: 'Correo enviado',
											text: "Hemos enviado a tu cuentas las instrucciones para recuperar tu contraseña.",
											position: 'top-center',
											loaderBg: '#C1262E',
											bgColor: 'white',
											textColor: 'grey',
											stack: false,
											hideAfter: 6000
										});	
										// window.location.replace(scope_url + "/home");
									}else{
										var errormsj= errorMessage(data);
										// var errormsj= data;
										$.toast({
											heading: 'Error',
											text: '<span style="font-size:18px;"></span> '+errormsj,
											position: 'bottom-center',
											loaderBg: '#C1262E',
											bgColor: 'white',
											textColor: 'grey',
											stack: false
										});	
									}
								}
							});	
						}		
					}
				},
				cancel: {
					text: 'Cancelar',
					function () {
					//close
				},
				}
					
			},
			onContentReady: function () {
				// bind to events
				var jc = this;
				this.$content.find('form').on('submit', function (e) {
					// if the user submits the form by pressing enter in the field.
					e.preventDefault();
					jc.$$formSubmit.trigger('click'); // reference the button and click it
				});
			}
		});
	}

	function terminos(){
		// console.log('terminos');
		// // document.getElementById("terminosuser").checked = true;

		// $("#terminosuser").attr("checked", true);
		
		// var elm = document.getElementById('terminosuser');
		// if (checked != elm.checked) {
		// 	elm.click();
		// }
		// console.log(elm);
	}
	function terminos_link(){
		
		window.open(scope_url + "/terminos",'_blank');

	}
	
 $(document).ready(function() {


	$('#terminosuser').on('click',function(event){
        event.stopPropagation(); 
    });

	$('#terminospro').on('click',function(event){
        event.stopPropagation(); 
    });

	$('#login-form-link').click(function(e) {

        console.log('ok');
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$('#tab2').removeClass('activa');
		$('#tab1').addClass('activa');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#login-form-link2').click(function(e) {

        console.log('ok');
		$("#login-form2").delay(100).fadeIn(100);
 		$("#register-form2").fadeOut(100);
		$('#register-form-link2').removeClass('active');
		$('#tab4').removeClass('activa');
		$('#tab3').addClass('activa');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {

        console.log('ok');
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$('#tab1').removeClass('activa');
		$('#tab2').addClass('activa');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link2').click(function(e) {

        console.log('ok');
		$("#register-form2").delay(100).fadeIn(100);
 		$("#login-form2").fadeOut(100);
		$('#login-form-link2').removeClass('active');
		$('#tab3').removeClass('activa');
		$('#tab4').addClass('activa');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#log-user').click(function(e) {
		
		$('#log-pro').removeClass('activa');
		$('#log-pro').addClass('inactiva');
		$('#log-user').removeClass('inactiva');
		$('#log-user').addClass('activa');
		e.preventDefault();
	});
	$('#log-pro').click(function(e) {
        
		$('#log-pro').removeClass('inactiva');
		$('#log-pro').addClass('activa');
		$('#log-user').removeClass('activa');
		$('#log-user').addClass('inactiva');
		// $(this).addClass('active');
		e.preventDefault();
	});

});
</script>
</body>


</html>