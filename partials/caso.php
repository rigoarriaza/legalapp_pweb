<?php include_once("head.php"); ?>

<?php include_once("navbar.php"); ?>


<?php
    if ( ! isset($elementPATH)) {
        $elementPATH = realpath(__DIR__ . '/..');
        define("elementPATH", $elementPATH);
    }
    
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . '/../../actions');
		define("actionPATH", $actionPATH);
	}

	if(!isset($_SESSION)){
		session_start();
	}
	
	include_once(actionPATH . DIRECTORY_SEPARATOR . "a_getdata.php");

    $showReplica = false;
    $formDisabled = true;
    $tiempo = '' ;
    $precio = 0 ;

    if($slug_2 == 1){ //PREGUNTAS EXPRESS
        $pregunta = getServiceData('getDetallePreguntaContestada', $userData->token_session, $userData->idprofesional, (array)[$slug]);
    
        if($pregunta->p_estado == 2 || $pregunta->p_estado == 4 || $pregunta->p_estado == 6){
            $formDisabled = false;

            if( $pregunta->p_estado != 6) {
                $fecha_comp = $pregunta->p_fechaTomada;
            }else{
                $fecha_comp = $pregunta->p_respuesta[1]->datetime;
            }
            $today	= date('Y-m-d H:i:s');
            $eDate 	= explode(" ", $fecha_comp);
	        $date 	= date('Y-m-d',strtotime($eDate[0]));
	
            $t = new DateTime($today);
            $s = new DateTime( $fecha_comp);
            $fecha = $s->diff($t);

            if($fecha->h <=15 && $fecha->d == 0){
                $restante = 15 - $fecha->h;
                $tiempo = "Te quedan ". $restante ." horas para contestar.";
            }else{
                // $formDisabled = true;
                $tiempo = "Tu usuario estará bloqueado hasta que respondas todas las réplicas pendientes";
            }
        }else{
            $formDisabled = true;
        }
    
        
    }else if($slug_2 == 2){ //PREGUNTAS DIRECTAS
        $pregunta = getServiceData('getDetallePreguntaDirectaProfesional', $userData->token_session, $userData->idprofesional, (array)[$slug]);
    
        if( $pregunta->p_estado == 5) {
            $formDisabled = true;
        }else{
            var_dump(count($pregunta->detalle_r));
            if( count($pregunta->detalle_r) > 0) {
                $fecha_comp = $pregunta->detalle_r[count($pregunta->detalle_r)-1]->datetime;
            }else{
                $fecha_comp = $pregunta->p_datetimepago;
                // $fecha_comp = $pregunta->p_respuesta[1]->datetime;
            }
            $today	= date('Y-m-d H:i:s');
            $eDate 	= explode(" ", $fecha_comp);
	        $date 	= date('Y-m-d',strtotime($eDate[0]));
	
            $t = new DateTime($today);
            $s = new DateTime( $fecha_comp);
            $fecha = $s->diff($t);

            if($fecha->h <=15 && $fecha->d == 0){
                $formDisabled = false;
                $restante = 15 - $fecha->h;
                $tiempo = "Te quedan ". $restante ." horas para contestar.";
            }else{
                $formDisabled = false;
                $tiempo = "Tu usuario estará bloqueado hasta que respondas todas las réplicas pendientes";
            }
        }
    
    }else if($slug_2 == 3){  //COTIZACIONES
        $pregunta = getServiceData('getDetalleCotizacion', $userData->token_session, $userData->idprofesional, (array)[$slug]);
        
        $m_photo = (empty($pregunta->u_imagen) || ($pregunta->privacidad == 1)) ? $page_url . '/assets/default_user.png' : $pregunta->u_imagen;
        $nombre = (($pregunta->privacidad == 1)) ? 'Anónimo' : $pregunta->u_nombre.' '.$pregunta->u_apellido;

        $contenido = '';
        if($pregunta->c_estado==0){
            $formDisabled = false;
        }
        if(isset($pregunta->detalle_respuesta->precio)){
            $precio = $pregunta->detalle_respuesta->precio;
            $contenido = $pregunta->detalle_respuesta->contenido;
        }

    }

    // var_dump($pregunta);

?>

<script>
$(document).ready(function() {
    

 }); 
</script>


    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <!-- Sidebar Widgets Column -->
        <div class="col col-md-4">

          <?php include_once("sidemenu.php"); ?>

        </div>

        <!-- Blog Entries Column -->
        <div class="col-md-8">

        <!--PREGUNTAS EXPRESS-->
        <?php if($slug_2 == 1){ ?>
          <h3 class="subtitulo"><?=$pregunta->p_titulo ?></h3>
                <div class="panel-body-chat">
                    <ul class="chat">

                    <? foreach ($pregunta as $key => $value) {  
                    
                        if($key == "u_foto"){
                            $imagen = $value;
                        }else if($key == "u_nombre"){
                            $nombre = $value;
                        }else if($key == "u_apellido"){
                            $apellido = $value;
                        }else if($key == "p_fecha"){
                            $fecha = $value;
                        }else if($key == "p_estado"){
                            $estado = $value;
                        }else if($key == "p_titulo"){
                            $titulo = $value;
                        }else if($key == "p_contenido"){
                            $contenido = $value;
                        }else if($key == "p_privacidad"){
                            $privacidad = $value;
                        }
                       }
                       if($privacidad == 1){
                           $nombre = "Anónimo";
                           $apellido = "";
                           $imagen = $page_url . '/assets/default_user.png';
                       }
                    ?>


                    <label class="lblestado <?=getEstadoClase($estado);?>"><?=getEstado($estado);?></label>
                        
                        <li class="left clearfix">
                            <span class="chat-img pull-left">
                                <img src="<?=$imagen ?>" alt="" class="img-circle" />
                            </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font"><?=$nombre; ?> <?=$apellido; ?></strong> <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span><?=formatDate($fecha)?></small>
                                </div>
                                <p>
                                   <?=$contenido?>

                                   <input type="hidden" value="<?=$contenido?>" id="pdetalle" />
                                </p>
                            </div>
                        </li>
                    
                     <? $i=0;
                        foreach ($pregunta->p_respuesta as $key => $datos) {  
                            // var_dump($datos);
                            if($i % 2){
                                
                            ?>
                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="<?=$imagen ?>" alt="User Avatar" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                        <div class="header">
                                            <strong class="primary-font"><?=$nombre; ?> <?=$apellido; ?></strong> <small class="pull-right text-muted">
                                                <span class="glyphicon glyphicon-time"></span><?=formatDate($datos->datetime)?></small>
                                        </div>
                                            <?=$datos->contenido?>
                                        </p>
                                    </div>
                                </li>
                            <?
                            }else{
                                // var_dump($datos);
                                foreach($datos->profesional_info as $key => $valor){

                                    if($key == "p_nombre"){
                                        $pro_nombre = $valor;
                                    }else if($key == "p_apellido"){
                                        $pro_apellido = $valor;
                                    }else if($key == "p_foto"){
                                        $pro_foto = $valor;
                                    }
                                }
                                ?>

                                    <li class="right clearfix">
                                        <span class="chat-img pull-right">
                                            <img src="<?=$pro_foto?>" alt="User Avatar" class="img-circle" />
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <small class=" text-muted"><span class="glyphicon glyphicon-time"></span><?=formatDate($datos->datetime)?></small>
                                                <strong class="pull-right primary-font"><?=$pro_nombre;?> <?=$pro_apellido;?></strong>
                                            </div>
                                            <p>
                                                <?=$datos->contenido?>
                                            </p>
                                        </div>
                                    </li>
                                <?
                            }
                       $i++;
                       }
                    ?>

                    <!--BOTONES DE ACCIONES DISPONIBLES-->
                       <div style="width:90%; margin:15px;text-align:center;">
                           <br><label style="color:red; font-style: oblique; font-weight: 300;"><?=$tiempo ?></label>
                        
                        </div>
                    </ul>
                </div>
                <div class="panel-footer">
                    <div class="input-group">
                        <textarea id="btn-input"  maxlength="1500" <?php echo ($formDisabled)? ' disabled ': ' ' ?> row="3" class="form-control input-sm" placeholder="Enviar pregunta..." ></textarea>
                        <span class="input-group-btn">
                            <button onClick="responder('express')" <?php echo ($formDisabled)? ' disabled ': ' ' ?> class="btn btn-lgapp-secondary btn-lg" id="btn-chat">
                                <i class="fa fa-send "></i> Enviar</button>
                                
                        </span>
                    </div>
                </div>
            

        <?php }else if($slug_2 == 2){ ?>
        <!--PREGUNTAS DIRECTAS-->
          <h3 class="subtitulo"><?=$pregunta->p_titulo?> </h3>
                <div class="panel-body">
                    <ul class="chat">

                    <? foreach ($pregunta as $key => $value) {  
                    
                        if($key == "u_imagen"){
                            $imagen = $value;
                        }else if($key == "p_privacidad"){
                            $privacidad = $value;
                        }else if($key == "u_nombre"){
                            $nombre = $value;
                        }else if($key == "u_apellido"){
                            $apellido = $value;
                        }else if($key == "p_estado"){
                            $estado = $value;
                        }else if($key == "p_datetime"){
                            $fecha = $value;
                        }else if($key == "p_titulo"){
                            $titulo = $value;
                        }else if($key == "p_contenido"){
                            $contenido = $value;
                        }
                       }
                       if($privacidad == 1){
                           $nombre = "Anónimo";
                           $apellido = "";
                           $imagen = $page_url . '/assets/default_user.png';
                       }
                    ?>
                        <label class="lblestado <?=getEstadoClase($estado);?>"><?=getEstado($estado);?></label>
                        <li class="left clearfix">
                            <span class="chat-img pull-left">
                                <img src="<?=$imagen?>" alt="" class="img-circle" />
                            </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font"></strong> <?=$nombre?> <?=$apellido?><small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span><?=formatDate($fecha)?></small>
                                </div>
                                <p>
                                    <?=$contenido?>
                                </p>
                            </div>
                        </li>
                    
                     <? $i=0;
                        foreach ($pregunta->detalle_r as $key => $datos) {  
                            // var_dump($datos);
                            if($i % 2){
                                
                            ?>
                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="<?=$imagen?>" alt="User Avatar" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                        <div class="header">
                                            <strong class="primary-font"><?=$userData->u_nombre; ?> <?=$userData->u_apellido; ?></strong> <small class="pull-right text-muted">
                                                <span class="glyphicon glyphicon-time"></span><?=formatDate($datos->datetime)?></small>
                                        </div>
                                        <p>
                                            <?=$datos->contenido?>
                                        </p>
                                    </div>
                                </li>
                            <?
                            }else{
                                // var_dump($datos);
                                foreach($datos->profesional_info as $key => $valor){

                                    if($key == "p_nombre"){
                                        $pro_nombre = $valor;
                                    }else if($key == "p_apellido"){
                                        $pro_apellido = $valor;
                                    }else if($key == "p_foto"){
                                        $pro_foto = $valor;
                                    }
                                }
                                ?>
                                    <li class="right clearfix">
                                        <span class="chat-img pull-right">
                                            <img src="<?=$imageUrl ?>/<?=$userData->u_foto; ?>" alt="User Avatar" class="img-circle" />
                                        </span>
                                        <div class="chat-body clearfix">
                                            <div class="header">
                                                <small class=" text-muted"><span class="glyphicon glyphicon-time"></span><?=formatDate($datos->datetime)?></small>
                                                <strong class="pull-right primary-font"><?=$pro_nombre;?> <?=$pro_apellido;?></strong>
                                            </div>
                                            <p>
                                                <?=$datos->contenido?>
                                            </p>
                                        </div>
                                    </li>
                                <?
                            }
                       $i++;
                       }
                    ?>

                        <div style="width:90%; margin:15px;text-align:center;">
                            <br><label style="color:red; font-style: oblique; font-weight: 300;"><?=$tiempo ?></label>
                        
                        </div>
                        
                    </ul>
                </div>
                <div class="panel-footer">
                    <div class="input-group">
                        <textarea id="btn-input"  maxlength="1500" <?php echo ($formDisabled)? ' disabled ': ' ' ?> row="3" class="form-control input-sm" placeholder="Enviar respuesta..." ></textarea>
                        <span class="input-group-btn">
                            <button onClick="responder('directa')" <?php echo ($formDisabled)? ' disabled ': ' ' ?> class="btn btn-lgapp-secondary btn-lg" id="btn-chat">
                                <i class="fa fa-send "></i> Enviar</button>
                                
                        </span>
                    </div>
                </div>
            
        <?php }else if($slug_2 == 3){  ?>

        <div class="list-group">
            <a class="card" >
                <!--<div class="row">-->
                    <div class="row no-gutters">
                        <div class="col-auto">
                            <img src="<?=$m_photo?>" style="width:75px !important; height:75px !important;" class="img-fluid imglist"  alt="">
                        </div>
                        <div class="col">
                            <div class="card-block px-2">
                                <div class="row">
                                    <div class="col-8">
                                        <h5 class="title-list"> <?=$nombre; ?> </h5>
                                        
                                    </div>
                                    <div class="col-4">
                                        <label style="margin:2px;" class="lblestado <?=getEstadoClaseCot($pregunta->c_estado);?>"><?=getEstadoCot($pregunta->c_estado);?></label>
                        
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <h5 class="title-list">Consulta: <?=$pregunta->c_titulo; ?> </h5>
                                        <p><?=$pregunta->c_descripcion; ?> </p>
                                        <label class="detail-list"></label>
                                    </div>
                                    
                                    
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <h5 class="title-list">Fecha de Inicio: <?=formatDate($pregunta->c_fechainicio); ?></h5>
                                    </div>
                                    <div class="col-6">
                                        <h5 class="title-list">Fecha de Fin: <?=formatDate($pregunta->c_fechafin); ?></h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <h5 class="title-list">Precio (Bs): </h5>
                                        <input type="text" id="precio" value="<?=$precio;?>" <?php echo ($formDisabled)? ' disabled ': ' ' ?>/> 
                                    </div>
                                    <div class="col-6">
                                        <h5 class="title-list">Prioridad: <?=$pregunta->c_nivel; ?></h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <h5 class="title-list">Respuesta: </h5>
                                        <textarea id="btn-input"  maxlength="1500" <?php echo ($formDisabled)? ' disabled ': ' ' ?> row="5" class="form-control input-sm" placeholder="Enviar respuesta..." ><?=$contenido?></textarea>
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                    </div>
                                    <div class="col-6" style="text-align:right; padding:5px;">
                                        <button onClick="rechazar('cotizacion')" <?php echo ($formDisabled)? ' disabled ': ' ' ?> class="btn btn-lgapp-primary btn-lg" id="btn-chat">
                                        <i class="fa fa-close "></i> Rechazar</button>
                                        <button onClick="responder('cotizacion')" <?php echo ($formDisabled)? ' disabled ': ' ' ?> class="btn btn-lgapp-secondary" id="btn-chat">
                                        <i class="fa fa-send "></i> Enviar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!--</div>-->

                
            </a>
        </div>
        <?php }  ?>
         

        </div>


      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include_once("foot.php"); ?>

    <!-- Bootstrap core JavaScript -->
    <!--<script src="vendor/jquery/jquery.min.js"></script>-->
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script type="text/javascript">
    const scope_url = "<?php echo $page_url; ?>";

    $(document).ready(function() {
        $("#pago").hide();

        var textarea = document.querySelector('textarea');

        textarea.addEventListener('keydown', autosize);
                    
        function autosize(){
        var el = this;
        setTimeout(function(){
            el.style.cssText = 'height:auto; padding:0';
            // for box-sizing other than "content-box" use:
            // el.style.cssText = '-moz-box-sizing:content-box';
            el.style.cssText = 'height:' + el.scrollHeight + 'px';
        },0);
        }

    });
    
    var refobj = [];
    function aliasConfirm(option){
         refobj.push($.confirm(option));
    }
    function closeAll(){
       $.each(refobj, function(i, a){ 
            a.close(); 
            // you can pop the closed ones from refobj array here.
       })
    }

    function rechazar(tipo){

    }

    function responder(tipo){      

      aliasConfirm({
        title: '¿Has finalizado tu respuesta?',
        content: 'Recuerda que no podrás realizar cambios una vez enviada tu respuesta. Asegúrate que contiene todo lo necesario.',
        theme: 'material', // 'material', 
        columnClass: 'col-md-6 col-md-offset-6',
        buttons: {
         cancel: {
            text: 'No, editar',
              function () {
              //close
            },
          },
          formSubmit: {
            text: 'Si, enviar',
            btnClass: 'lgapp-secondary',
            action: function () {
              
                var idpregunta = <?=$slug; ?>;
                var idusuario = <?=$pregunta->iduserapp; ?>;
                var descripcion = $("#btn-input").val();
                var precio = <?=$precio ?>;
                var datos = {
                    id:idpregunta,
                    description: descripcion,
                    iduser: idusuario,
                    precio: precio
                };
                $('body').loading({message: 'Cargando...'});
                            // var form3 = $("#formExpress").serializeArray();
                            console.log(datos);
                            $.ajax({
                              url: scope_url + "/actions/a_reply.php",
                              type:'POST',
                              data: { data: JSON.stringify(datos), tipo: tipo, accion: 'responder' },
                              success: function(resp){
                                $('body').loading('stop');
                                //aqui va a devolver obj respuesta id
                                console.log(resp);
                                if (resp=='OK') {
                                  
                                  //consultar nuevamente
                                  //Pagar Pregunta  //Ir a mis preguntas
                                  aliasConfirm({
                                    title: '¡Respuesta enviada!',
                                    content: 'Tu respuesta ha sido enviada',
                                    buttons: {
                                        cancel: {
                                          text: 'Aceptar',
                                          action: function () {
                                            closeAll();
                                             window.location.reload();
                                          }
                                        }
                                    }
                                  });
                                  return false;
                                  
                                  // closeAll();
                                  // window.location.replace(scope_url + "/home");
                                }else{
                                  var errormsj= errorMessage(resp);
                                  // var errormsj= data;
                                  $.toast({
                                    text: errormsj,
                                    position: 'top-center',
                                    stack: false
                                  });	
                                }
                              }
                            });	
                          
                        

              		
            }
          }
            
        }
      });

    }

    function pagar(tipo){


      aliasConfirm({
        title: '¿Quieres realizar el pago de tu pregunta?',
        content: 'Vamos a verificar tu saldo para poder realizar el pago.',
        theme: 'material', // 'material', 
        columnClass: 'col-md-6 col-md-offset-6',
        buttons: {
         cancel: {
            text: 'No, regresa',
              function () {
              //close
            },
          },
          formSubmit: {
            text: 'Sí, pagar',
            btnClass: 'lgapp-secondary',
            action: function () {
              
                var idpregunta = <?=$slug; ?>;
                var idprofesional = <?=(isset($pregunta->idprofesional))? $pregunta->idprofesional : 0 ; ?>;

                          var datos = {
                                idpregunta:idpregunta,
                                idprofesional:idprofesional
                            };
                            // var form3 = $("#formExpress").serializeArray();
                            console.log(datos);
                            $.ajax({
                              url: scope_url + "/actions/a_pregunta.php",
                              type:'POST',
                              data: { data: JSON.stringify(datos), tipo: tipo, accion: 'pagar' },
                              success: function(resp){
                                
                                //aqui va a devolver obj respuesta id
                                console.log(resp);
                                if (resp=='OK') {
                                  
                                  //consultar nuevamente
                                  //Pagar Pregunta  //Ir a mis preguntas
                                  aliasConfirm({
                                    title: '¡Pregunta Pagada!',
                                    content: 'Tu pregunta directa ha sido pagada satisfactoriamente.',
                                    buttons: {
                                        cancel: {
                                          text: 'Aceptar',
                                          action: function () {
                                             window.location.replace(scope_url + "/preguntas");
                                          }
                                        }
                                    }
                                  });
                                  return false;
                                  
                                  // closeAll();
                                  // window.location.replace(scope_url + "/home");
                                }else if(resp == "error_saldo"){ 
                                  aliasConfirm({
                                    title: '¡Saldo insuficiente!',
                                    content: 'Para que la pregunta sea visible, confirma tu saldo y en breve un abogado te responderá.',
                                    buttons: {
                                        cancel: {
                                          text: 'Pagar pregunta',
                                          action: function () {
                                             window.location.replace(scope_url + "/user");
                                          }
                                        }
                                    }
                                  });
                                  return false;                  
                                }else{
                                  var errormsj= errorMessage(resp);
                                  // var errormsj= data;
                                  $.toast({
                                    text: errormsj,
                                    position: 'bottom-center',
                                    stack: false
                                  });	
                                }
                              }
                            });	
                          
                        

              		
            }
          }
            
        },
        onContentReady: function () {
          // bind to events
          console.log('load')
          var jc = this;
          this.$content.find('form').on('submit', function (e) {
            // if the user submits the form by pressing enter in the field.
            e.preventDefault();
            jc.$$formSubmit.trigger('click'); // reference the button and click it

            console.log('ejecuta')
          });
        }
      });

    }
    

  </script>


  </body>

</html>
