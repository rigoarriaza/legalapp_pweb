<?php include_once("head.php"); ?>

<?php include_once("navbar.php"); ?>
    <style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 500px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>
<?php
    if ( ! isset($elementPATH)) {
        $elementPATH = realpath(__DIR__ . '/..');
        define("elementPATH", $elementPATH);
    }
    
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . '/../../actions');
		define("actionPATH", $actionPATH);
	}

	if(!isset($_SESSION)){
		session_start();
	}

?>    


    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <!-- Sidebar Widgets Column -->
        <div class="col col-md-4">

          <?php include_once("sidemenu.php"); ?>

        </div>

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          
              <h3 class="subtitulo">Notarías</h3>
            <!--The div element for the map -->
            <div id="map"></div>


          
           <div class="list-group"></div>

          

        </div>


      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include_once("foot.php"); ?>

    <!-- Bootstrap core JavaScript -->
    <!--<script src="vendor/jquery/jquery.min.js"></script>-->
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script type="text/javascript">


  </script>
    <script>
// Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {lat: -17.783314, lng: -63.182134};
  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 12, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
  var marker2 = new google.maps.Marker(
      {
            position:  {lat: -17.983414, lng: -63.182134}, 
            animation: google.maps.Animation.DROP,
            map: map});
    var marker3 = new google.maps.Marker(
      {
            position:  {lat: -17.983414, lng: -63.182134}, 
            animation: google.maps.Animation.DROP,
            map: map});
  var marker4 = new google.maps.Marker(
      {
            position:  {lat: -17.983004, lng: -64.182134}, 
            animation: google.maps.Animation.DROP,
            map: map});
}
    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCBcnynHTpMoYk3TXwT_NzI16SERjjpqoc&callback=initMap">
    </script>

  </body>

</html>