<?php include_once("head.php"); ?>

<?php include_once("navbar.php"); ?>
    <style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 500px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>
<?php
    if ( ! isset($elementPATH)) {
        $elementPATH = realpath(__DIR__ . '/..');
        define("elementPATH", $elementPATH);
    }
    
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . '/../../actions');
		define("actionPATH", $actionPATH);
	}

	if(!isset($_SESSION)){
		session_start();
	}

    $archivos = getServiceData('getArchivosDisponiblesProfesional',$userData->token_session,$userData->idprofesional, (array)[0,20] );
	
?>    

    <?php include_once("validar.php"); ?>
    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <!-- Sidebar Widgets Column -->
        <div class="col col-md-4">

          <?php include_once("sidemenu.php"); ?>

        </div>

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          
              <h3 class="subtitulo">Archivos</h3>

          <div class="container">
            <div class="flex-row row">
              <?php

              if(isset($archivos) && $archivos== 'no_data'){?>

                <div class="error" >
                  <label > <i class="fa fa-search"></i> No hay datos</label>
                </div>

              <?	
              }else if(isset($archivos) && count($archivos) > 0){
                foreach ($archivos as $archivo) {      
                
                foreach ($archivo as $key => $value) {
                    // print "$key => $value\n";
                    
                    
                    if($key == "a_nombre"){
                        $a_nombre = $value;
                    }else if($key == "a_url"){
                      $a_url = $value;
                    }else if($key == "especialidad"){
                      $especialidad = $value;
                    }else if($key == "deb_fecha"){
                      $deb_fecha = $value;
                    }else if($key == "id_deb"){
                      $id_deb = $value;
                    }
					
                }
                ?>
              <div class="col-xs-6 col-sm-4 col-lg-3">
                <div class="thumbnail file">
                  <div class="caption">
                    <h4><?=$a_nombre ?></h4>
                    <p class="flex-text text-muted"><?=$especialidad?>
                    </p>
                    <p><a class="btn btn-secondary" href="<?=$a_url ?>" target="_blank">Ver</a></p>
                  </div>
                  <!-- /.caption -->
                </div>
                <!-- /.thumbnail -->
              </div>

              <?
                }

                
            }

            ?>  
             

            </div>
            <!-- /.flex-row  -->
          </div>

          

        </div>


      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include_once("foot.php"); ?>

    <!-- Bootstrap core JavaScript -->
    <!--<script src="vendor/jquery/jquery.min.js"></script>-->
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script type="text/javascript">


  </script>
   

  </body>

</html>