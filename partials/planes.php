<?php include_once("head.php"); ?>

<?php include_once("navbar.php"); ?>
    <style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 500px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>
<?php
    if ( ! isset($elementPATH)) {
        $elementPATH = realpath(__DIR__ . '/..');
        define("elementPATH", $elementPATH);
    }
    
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . '/../../actions');
		define("actionPATH", $actionPATH);
	}

	if(!isset($_SESSION)){
		session_start();
	}

  
  $consulta = getServiceDataCommun('getPlanesProfesional');

?>    


    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <!-- Sidebar Widgets Column -->
        <div class="col col-md-4">

          <?php include_once("sidemenu.php"); ?>

        </div>

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          
              <h3 class="subtitulo">Planes</h3>

              <div class="container">
  <div class="row flex-items-xs-middle flex-items-xs-center">

    <?php if(isset($consulta)){ 
    
            foreach($consulta as $key => $plan){
    ?>

                <!-- Table #1  -->
                <div class="col-xs-12 col-lg-4">
                <div class="card2 text-xs-center">
                    <div class="card-header">
                    <h3 class="display-2"><span class="currency">Bs</span><?=$plan->costo_plan ?><span class="period">/<?=($plan->duracion<12)? 'mes' : 'año' ; ?></span></h3>
                    </div>
                    <div class="card-block">
                    <h4 class="card-title"> 
                        <?=$plan->nombre_plan ?>
                    </h4>
                    <ul class="list-group">
                        <li class="list-group">Selecciona hasta <?=$plan->c_especialidades ?> especialidades</li>
                        <li class="list-group">Acceso ilimitado a la biblioteca</li>
                        <li class="list-group">Soporte 24/7</li>
                    </ul>
                    <button data-toggle="modal" data-idplan="<?=$plan->id_plan ?>" data-plan="<?=$plan->nombre_plan ?>"  data-precio="<?=$plan->costo_plan ?>" data-target="#modalpay" class="btn btn-gradient mt-2" >Seleccionar</button>
                    </div>
                </div>
                </div>

    <?
            }
    }
       ?>


  </div>
</div>

        </div>


      </div>
      <!-- /.row -->

<input id="idplan" type="hidden" />
<input id="costo" type="hidden" />

<!-- Modal -->
<div class="modal fade" id="modalpay" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Opciones de Pago</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
              <a class="card" href="#">
                  <div class="row no-gutters">
                      <div class="col" >
                          <div class="card-block px-2" style="text-align:center;">

                              
                            <div id="pago" class="row">
                                <div class="col-4" onClick="pagarTarjeta()">
                                    <img src="<?=$page_url;?>/assets/credit-card.png" style="width:100%;" />
                                    <label> Pago con Tarjeta </label>
                                </div>
                                <div class="col-4" onClick="pagarBanco()">
                                    <img src="<?=$page_url;?>/assets/payment.png" style="width:100%;"  />
                                    <label> Pago en Banco </label>
                                </div>
                                <div class="col-4" onClick="pagarTarjeta()">
                                    <img src="<?=$page_url;?>/assets/paytigo.png" style="width:100%;"  />
                                    <label> Tigo Money </label>
                                </div>
                                
                            </div>

                              


                          </div>
                      </div>
                  </div>
                  
              </a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
      </div>
    </div>
  </div>
</div>
    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include_once("foot.php"); ?>

    <!-- Bootstrap core JavaScript -->
    <!--<script src="vendor/jquery/jquery.min.js"></script>-->
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script type="text/javascript">
    const scope_url = "<?php echo $page_url; ?>";

    $('#modalpay').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var plan = button.data('plan') // Extract info from data-* attributes
        var costo = button.data('precio');
         var idplan = button.data('idplan') ;
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Suscripción: ' + plan + '/Bs' + costo);
        // modal.find('.modal-body input').val(recipient)
        $('#idplan').val(idplan);
        $('#costo').val(costo);
    })

    function pagarTarjeta(){

        $.alert('Redireccionando a plataforma de pago');

        var carnet = <?=$userData->u_carnet; ?>;
        var idplan = $('#idplan').val();
                        var costo = $('#costo').val();
                        
                        var datos = {
                            costo: costo,
                            idplan: idplan
                        }

                        console.log(datos);

                        $.ajax({
                            url: scope_url + "/actions/a_plan.php",
                            type:'POST',
                            data: { data: JSON.stringify(datos) },
                            success: function(data){
                                console.log(data);
                                if (data === "OK") {
                                    window.location.replace("http://multipago.bo/service/legal_app/external/start/"+carnet+"/2");
                                }else{
                                    var errormsj= errorMessage(data);
                                    $.toast({
                                        heading: 'Error',
                                        text: '<span style="font-size:18px;"></span> '+errormsj,
                                        position: 'top-center',
                                        loaderBg: '#C1262E',
                                        bgColor: 'white',
                                        textColor: 'grey',
                                        stack: false,
                                        hideAfter: 6000
                                    });	
                                }
                            }

                        });	 

        
        
        
    } 

    function pagarBanco(){

		$.confirm({
			title: 'Pago en Banco',
			content: 'Al elegir pago en banco nuestro sistema genera un pago pendiente, el cual con la sola presentación de tu cédula de identidad puedes dirigirte a cualquiera de los puntos de pago',
			buttons: {
				formSubmit: {
					text: 'Aceptar',
					btnClass: 'lgapp-secondary',
					action: function () {

                        var idplan = $('#idplan').val();
                        var costo = $('#costo').val();
                        
                        var datos = {
                            costo: costo,
                            idplan: idplan
                        }

                        console.log(datos);

                        $.ajax({
                            url: scope_url + "/actions/a_plan.php",
                            type:'POST',
                            data: { data: JSON.stringify(datos) },
                            success: function(data){
                                console.log(data);
                                if (data === "OK") {
                                    window.location.replace(scope_url + "/wallet");
                                }else{
                                    var errormsj= errorMessage(data);
                                    $.toast({
                                        heading: 'Error',
                                        text: '<span style="font-size:18px;"></span> '+errormsj,
                                        position: 'top-center',
                                        loaderBg: '#C1262E',
                                        bgColor: 'white',
                                        textColor: 'grey',
                                        stack: false,
                                        hideAfter: 6000
                                    });	
                                }
                            }

                        });			
						//close	
					}
				},
				cancel: {
					text: 'No, regresar',
					function () {
					//close
				},
				}
					
			}
		});

    }

  </script>
   

  </body>

</html>