 <?
 
    $mensaje = 0;

    if($userData->u_estado == 2 ){
      if($userData->u_idplan == null){
        $mensaje = 1;
      }
    }else{
        $mensaje = 2;
    }
?>    

<script >
 $(document).ready(function() {

    const scope_url = "<?php echo $page_url; ?>";
    var mensaje = <?=$mensaje ?>;

    if(mensaje == 1){
      $.confirm({
        title: 'Adquiere tu plan Premium',
        content: 'Obtén un plan premium para poder acceder a los archivos que Legal App tiene para la comunidad de abogados.',
        buttons: {
          formSubmit: {
            text: 'Ver Planes',
            btnClass: 'lgapp-secondary',
            action: function () {
                window.location.replace(scope_url + "/planes");
              
              //close	
            }
          },
          cancel: {
            text: 'No, regresar',
            action: function () {
                window.location.replace(scope_url + "/homepro");
            //close
          },
          }
            
        }
      });	
    }else if(mensaje == 2){
      $.confirm({
        title: 'Acceso restringido',
        content: 'Su cuenta esta pendiente de ser verficada por los administradores.',
        buttons: {
          cancel: {
            text: 'Aceptar',
            action: function () {
                window.location.replace(scope_url + "/homepro");
            //close
          },
          }
            
        }
      });	
    }


 });
 </script>