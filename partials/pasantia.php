<?php include_once("head.php"); ?>

<?php include_once("navbar.php"); ?>

<?php
    if ( ! isset($elementPATH)) {
        $elementPATH = realpath(__DIR__ . '/..');
        define("elementPATH", $elementPATH);
    }
    
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . '/../../actions');
		define("actionPATH", $actionPATH);
	}

	if(!isset($_SESSION)){
		session_start();
	}

	
	include_once(actionPATH . DIRECTORY_SEPARATOR . "a_getdata.php");

  $pasantias = getServiceDataCommun('getPasantias', (array)[0,20]);
	
	// var_dump($pasantias);

?>    


    <!-- Page Content -->
    <div class="container">

      <div class="row">
        <!-- Sidebar Widgets Column -->
        <div class="col col-md-4">

          <?php include_once("sidemenu.php"); ?>

        </div>

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          
              <h3 class="subtitulo">Pasantías</h3>

              <div class="list-group">

            <?php

            if(isset($pasantias) && $pasantias== 'no_data'){?>

              <div class="error" >
                <label > <i class="fa fa-search"></i> No hay datos</label>
              </div>

            <?	
            }else if(isset($pasantias) && count($pasantias) > 0){
                foreach ($pasantias as $pasantia) {      
                
                foreach ($pasantia as $key => $value) {
                    // print "$key => $value\n";
                    
                    
                    if($key == "titulo_pasantia"){
                        $titulo = $value;
                    }else if($key == "detale_pasantia"){
                      $detale_pasantia = $value;
                    }
					
                }
                ?>
                <div class="card" >
                    <div class="row no-gutters">
                        <!--<div class="col-auto">
                            <img src="" style="width:75px !important; height:75px !important;" class="img-fluid imglist"  alt="">
                        </div>-->
                        <div class="col">
                            <div class="card-block px-2">
								
                                <a class="row" >
                                    <div class="col-12">
                                        <h5 class="title-list"> <?=$titulo; ?> </h5>
                                        <p> <?=$detale_pasantia; ?> </p>
                                    </div>
                                    
                                </a>
								              
                            </div>
                        </div>
                    </div>
                    
                </div>


                
                <?
                }

                
            }

            ?>      




              </div>   

        </div>


      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include_once("foot.php"); ?>

    <!-- Bootstrap core JavaScript -->
    <!--<script src="vendor/jquery/jquery.min.js"></script>-->
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script type="text/javascript">


  </script>
   

  </body>

</html>