<?php include_once("head.php"); ?>

<?php include_once("navbar.php"); ?>

<?php
    if ( ! isset($elementPATH)) {
        $elementPATH = realpath(__DIR__ . '/..');
        define("elementPATH", $elementPATH);
    }
    
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . '/../../actions');
		define("actionPATH", $actionPATH);
	}

	if(!isset($_SESSION)){
		session_start();
	}
	
	include_once(actionPATH . DIRECTORY_SEPARATOR . "a_getdata.php");


    // $carnet = getServiceDataCommun('getCarnetUsuario', (array)[$userData->iduserapp, $userData->token_session]);
	
	// var_dump($carnet);

//   $user = (object) ["id"=>$lawyer[0]->idprofesional,
//                      "photo"=> getUserPhoto($lawyer[0]->p_foto),
//                      "name"=> $lawyer[0]->p_nombre . ' ' . $lawyer[0]->p_apellido,
//                      "description"=> $lawyer[0]->p_descripcionpro ,
//                      "title"=> '',
//                      "rate"=> $lawyer[0]->calificacion,
//                      "comentarios"=> $lawyer[0]->comentarios,
//                      "especialidad"=> $lawyer[0]->especialidad,
//                      "answers" => $lawyer[0]->n_contestadas,
//                      "plan" => $lawyer[0]->idplan,
//                      "favorito" => $lawyer[0]->favorito,
//                      "ultimas" => $lawyer[0]->ultima_pregunta
//                      ];


//   var_dump($user);

	// if(count($user->ultimas) > 0 ){
	// 	foreach ($user->ultimas as $key => $preg) {
	// 		$preg_o = 	 array (
	// 								"titulo" =>$preg->titulo_p,
	// 								"detalle"=> $preg->contenido_p,
	// 								"valor"=> $preg->calificacion_p,
	// 								"fecha" =>formatDate($preg->fecha_p));
	// 		$preguntas->ultimas[] = (object) $preg_o;
	// 	}
	// }

  // var_dump($especialidades);

?>


      <div class="row divhead">
        <div class="col col-md-1"></div>
       
        <div class="col col-md-1"></div>
      </div>
    <!-- Page Content -->
    <div class="container">


      <div class="row">
        <div class="col col-md-4" style="text-align:center;">
            <div class="row">
                      <div class="col-12">
                        <form id="foto-form"  novalidate="novalidate">
                          <img id="fperfil" style="width: 150px !important; height: 150px !important;" src="<?=$imageUrl ?>/<?=$userData->u_foto; ?>" class="img-fluid imglist"  alt="">
                          <br>
                          <label for="fileToUpload" class="btn" style="color:grey;"><i class="fa fa-camera"></i> Actualizar foto</label>
                          <input onchange="updateFoto()" name="fileToUpload" id="fileToUpload" style="visibility:hidden;" type="file">
                        </form>      
                      </div>
            </div>
          <div class="row">
              <div class="col-12">
                <button id="btn-favorito" class="btn-lgapp-secondary  btn " type="button" onClick="setClave()">
                  <i class="fa fa-lock "></i> Cambia tu contraseña</button>
              </div>
                      
          </div>
          
        </div>

        <!-- Blog Entries Column -->
        <div class="col-md-8">

          <div class="list-group">
              <div class="" >
                  <div class="row no-gutters">

                      <div class="col-12 col-md-9" >
                          <div class="card-block px-2">

                              <h4 class="subtitulo"><?=$userData->u_nombre; ?> <?=$userData->u_apellido; ?><label class="detail-list"> </label>
                              
                              </h4>
                                <!--<i class="fa fa-edit"></i> ¿Cuál es tu consulta?-->
                              <p></p>
                              <div class="row">
                                <div class="col-12 col-md-8">
                                    <form id="update-form"  novalidate="novalidate">
                                        <input class="legal-input" id="u_nombre" name="u_nombre" placeholder="Nombre" value="<?=$userData->u_nombre; ?>"/>
                                        <br>
                                        <input class="legal-input"  id="u_apellido" name="u_apellido" placeholder="Apellido" value="<?=$userData->u_apellido; ?>"/>
                                        <br>
                                        <input  class="legal-input" id="u_celular" name="u_celular" placeholder="Celular"  value="<?=$userData->u_celular; ?>"/>
                                        <br>
                                        <input class="legal-input"  id="u_carnet" name="u_carnet" placeholder="carnet"  value="<?=$userData->u_carnet; ?>" />
                                        <br><br>
                                        <button onClick="update();" class="btn lgapp-secondary" type="button"><i class="fa fa-edit"></i> Actualizar </button> 
                                    <br>
                                    </form>  
                                </div>
                                <!--<div class="col-6"><button class="btn lgapp-secondary" type="button"></button> Respuestas</div>-->
                                
                              </div> 
                          </div>
                      </div>
                  </div>
                  <!--<div class="card-footer w-100 text-muted" onClick="preguntaExpress()">
                    <div class="row">
                      <div class="col-12"><button class="btn lgapp-secondary" type="button">Consultar</button></div>
                      
                    </div>
                  </div>-->
              </div>
            </div>        
       

        </div>

        <!-- Sidebar Widgets Column -->


      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <?php include_once("foot.php"); ?>

  <script type="text/javascript">
    const scope_url = "<?php echo $page_url; ?>";
 $(document).ready(function() {
    $("#pago").hide();


 });
    function pagoshow(){
        $("#pago").show();
        $("#btnpago").hide();
        
    }

	function update(){

        var form = $("#update-form").serializeArray();
		
    	var datos = objectifyForm(form)
		$.ajax({
			url: scope_url + "/actions/a_update.php",
			type:'POST',
            data: { data: JSON.stringify(datos), tipo: 1 },
            success: function(data){
				console.log(data);
				if (data === "OK") {
					window.location.replace(scope_url + "/user");
				}else{
					var errormsj= errorMessage(data);
					$.toast({
						heading: 'Error',
						text: '<span style="font-size:18px;"></span> '+errormsj,
						position: 'bottom-center',
						loaderBg: '#C1262E',
						bgColor: 'white',
    					textColor: 'grey',
						stack: false,
						hideAfter: 6000
					});	
				}
            }

		});
    }     
	    
	function updateFoto(){

        // var form = $("#foto-form").serializeArray();
		// var base64 = getBase64Image(document.getElementById("imageid"));
		// var base64 = getBase64Image($("#files").val());
        // var ruta = $("#files").val();
    	
        // console.log(base64);
        var preview = document.getElementById("fperfil");
        var file    = document.querySelector('input[type=file]').files[0];
        var reader  = new FileReader();

        reader.addEventListener("load", function () {
            preview.src = reader.result;
            setFoto(reader.result);
        }, false);

        if (file) {
            reader.readAsDataURL(file);

            // console.log(datos);
        }


    }

    function setFoto(img){
        $.ajax({
			url: scope_url + "/actions/a_foto.php",
			type:'POST',
            data: { foto: img , tipo: 1 },
            success: function(data){
				console.log(data);
				if (data === "OK") {
					window.location.replace(scope_url + "/user");
				}else{
					var errormsj= errorMessage(data);
					$.toast({
						heading: 'Error',
						text: '<span style="font-size:18px;"></span> '+errormsj,
						position: 'bottom-center',
						loaderBg: '#C1262E',
						bgColor: 'white',
    					textColor: 'grey',
						stack: false,
						hideAfter: 6000
					});	
				}
            }

		});
    }

    function setClave(){

		$.confirm({
			title: 'Cambiar contraseña',
			content: '' +
			'<form action="" class="formClave">' +
			'<div class="form-group">' +
			'<input type="password" placeholder="Nueva Contraseña" class="pass form-control" required /><br>' +
			'<input type="password" placeholder="Confirmar Contraseña" class="pass2 form-control" required />' +
			'</div>' +
			'</form>',
			buttons: {
				formSubmit: {
					text: 'Enviar',
					btnClass: 'lgapp-secondary',
					action: function () {
						var pass = this.$content.find('.pass').val();
						var pass2 = this.$content.find('.pass2').val();
						if(!pass || !pass2){
							$.alert('Ingresa ambos campos.');
							return false;
						}else if(pass != pass2){
							$.alert('Las contraseñas no coinciden');
							return false;
						}else{
							$.ajax({
								url: scope_url + "/actions/a_passchange.php",
								type:'POST',
								data: { password: pass, tipo:1 },
								success: function(data){
									console.log(data);
									if (data === "OK") {
										$.toast({
											heading: 'Contraseña actualizada',
											text: "La contraseña se cambio correctamente.",
											position: 'bottom-center',
											loaderBg: '#C1262E',
											bgColor: 'white',
											textColor: 'grey',
											stack: false,
											hideAfter: 6000
										});	
									}else{
										var errormsj= errorMessage(data);
										$.toast({
											heading: 'Error',
											text: '<span style="font-size:18px;"></span> '+errormsj,
											position: 'bottom-center',
											loaderBg: '#C1262E',
											bgColor: 'white',
											textColor: 'grey',
											stack: false,
											hideAfter: 6000
										});	
									}
								}
							});	
						}		
					}
				},
				cancel: {
					text: 'Cancelar',
					function () {
					//close
				},
				}
			},
			onContentReady: function () {
				// bind to events
				var jc = this;
				this.$content.find('form').on('submit', function (e) {
					// if the user submits the form by pressing enter in the field.
					e.preventDefault();
					jc.$$formSubmit.trigger('click'); // reference the button and click it
				});
			}
		});
    } 

    function pagarTarjeta(){

        $.alert('Redireccionando a plataforma de pago');
        
        var carnet = <?=$userData->u_carnet; ?>;

        window.location.replace("http://multipago.bo/service/legal_app/external/start/"+carnet+"/1");
        
        
    } 

    function pagarBanco(){

		$.confirm({
			title: 'Pago en Banco',
			content: 'Estimado usuario, puedes pagar en cualquiera de las sucursales del Banco Fassil. Solicita al cajero pagar el servicio de Legal App para preguntas Express o preguntas Directas. Tienes que presentar tu cédula de identidad.',
			buttons: {
				formSubmit: {
					text: 'Aceptar',
					btnClass: 'lgapp-primary',
					action: function () {

						
						//close	
					}
				},
				cancel: {
					text: 'No, regresar',
					function () {
					//close
				},
				}
					
			}
		});

    }

  </script>

  </body>

</html>
