
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-legal fixed-top">
      <div class="container">
        <a class="navbar-brand"  href="#">
          <img class="logo" src="<?= $page_url ?>/assets/logo-s.png" >
        </a>
        <h1 class="titulo">Legal App</h1>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            
            <?php if( $current_user->userType == 1){ ?>
              <li class="nav-item menu-top <?php echo $activemenu = ($page_name == 'home') ? 'active': ' '; ?>">
                <a class="nav-link" href="<?= $page_url ?>/home">
                  <i class="fa fa-home"></i>
                  Inicio
                  <!--<span class="sr-only">(current)</span>-->
                </a>
              </li>
              <li class="nav-item menu-top  <?php echo $activemenu = ($page_name == 'user') ? 'active': ' '; ?>">
                <a class="nav-link" href="<?= $page_url ?>/user">
                  <i class="fa fa-user"></i>
                  Mi Perfil
                  
                </a>
              </li>
            <?php } else if( $current_user->userType == 2){ ?>
              <li class="nav-item menu-top <?php echo $activemenu = ($page_name == 'homepro') ? 'active': ' '; ?>">
                <a class="nav-link" href="<?= $page_url ?>/homepro">
                  <i class="fa fa-home"></i>
                  Inicio
                  <!--<span class="sr-only">(current)</span>-->
                </a>
              </li>
              <li class="nav-item menu-top  <?php echo $activemenu = ($page_name == 'profesional') ? 'active': ' '; ?>">
                <a class="nav-link" href="<?= $page_url ?>/profesional">
                  <i class="fa fa-user"></i>
                  Mi Perfil
                  
                </a>
              </li>     
            <?php }  ?>
            <li class="nav-item menu-top">
              <a class="nav-link" href="<?= $page_url ?>/logout">
                <i class="fa fa-logout"></i>
                Salir
                
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

<script>
   $(document).ready(function() {

    $('li').click(function(){
      
      $('nav li').removeClass('active'); 
      $(this).addClass('active'); 
    });
   });

</script>