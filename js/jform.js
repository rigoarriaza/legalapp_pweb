function objectifyForm(formArray) {//serialize data function

  var returnArray = {};
  for (var i = 0; i < formArray.length; i++){
    returnArray[formArray[i]['name']] = formArray[i]['value'];
  }
  return returnArray;
}

function errorMessage(error) {//serialize data function

  var msj = "";
  if(error == "error_email"){
    msj = "Falta ingresar su cuenta de correo";
  }else if(error == "error_pass"){
    msj = "Falta ingresar su contraseña";
  }else if(error == "error_name"){
    msj = "Falta ingresar su nombre";
  }else if(error == "error_lastname"){
    msj = "Falta ingresar su apellido";
  }else if(error == "error_celular"){
    msj = "Falta ingresar su número de celular";
  }else if(error == "error_carnet"){
    msj = "Falta ingresar su número de carnet";
  }else if(error == "error_nodata"){
    msj = "No encontramos ningún registro";
  }else if(error == "error_already"){
    msj = "Correo electrónico ya registrado";
  }else if(error == "error_ws"){
    msj = "Error de servicio web. Consulte al administrador";
  }else if(error == "nodata"){
    msj = "No se están enviando ningún dato";
  }else if(error == "error_terms"){
    msj = "Debe aceptar los términos y condiciones";
  }else if(error == "error_already"){
    msj = "Correo ya esta registrado";
  }else if(error == "error_datos_recovery"){
    msj = "Hemos revisado en nuestra base y tu correo no esta registrado.";
  }else if(error == "error_description"){
    msj = "Tienes que ingresar una respuesta.";
  }else if(error == "error_valorpregunta"){
    msj = "Por favor ingrese el monto que cobrará por sus servicios de contestar cada pregunta directa. <br> El monto máximo por los servicios en nuestra plataforma es de 700 Bs y el mínimo de 35 Bs";
  }
  
  return msj;
}