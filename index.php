<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	$request = $_SERVER['REQUEST_URI'];
	$params     = explode("/", $request);
	$safe_pages = array("planes", "login", "home", "homepro", "logout", "terminos", "mapa", "favoritos", "user", "abogado","preguntas","pregunta", "caso","casos","history", "settings","cuenta","profesional","examenes", "archivos", "newpassword", "pasantia", "certificates", "quotations","quotation", "tests", "wallet", "files", "file");
	$site_name = 'Legal App';
	$page_name = '';
	
	if ( ! defined("actionPATH")) {
		$actionPATH = realpath(__DIR__ . "/actions");
		define("actionPATH", $actionPATH);
	}

  	include_once("global_variables.php");
	include_once(actionPATH . DIRECTORY_SEPARATOR . "a_getdata.php");

	$pi2 = $pi1 + 1;
	$pi3 = $pi1 + 2;
	$pi4 = $pi1 + 3;

  $page = evalPermalink($params[$pi1]);
	
  $header = true;
  $footer = true;
	
  // echo $page; 
	// echo $domainName;
	function evalPermalink($perm){
		$matched = $perm;
		$varData = explode("?",$perm); 
		$re = '/[(\?|\&)]([^=]+)\=([^&#]+)/';
		preg_match_all($re, $perm, $matches, PREG_SET_ORDER, 0);
		
		if(sizeof($matches) > 0){
            if(count($varData) > 0){
                $matched = $varData[0];
            }
		}
		
		return $matched;
	}

// if(in_array($page, $safe_pages) ) {
if(in_array($page, $safe_pages) || evalPermalink($params[$pi1]) == "") {
		//Configurar esta variable tambien en /resources/css/global.css
		$slug = NULL;
		$slug_2 = NULL;
		$slug_3 = NULL;
		
		if(isset($params[$pi2])){
			$slug = evalPermalink($params[$pi2]);
		}
		if(isset($params[$pi3])){
			$slug_2 = evalPermalink($params[$pi3]);
		}
		if(isset($params[$pi4])){
			$slug_3 = evalPermalink($params[$pi4]);
		}

///////////////////////LOGIN//////////////////////////
		if($currentUser == null || ($page == "terminos")){
			if($page == 'login'){
				$page_name = "login";
				$page_data = (object) ["title" => "Iniciar sesión | Legal App", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
				include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "login.php");
			}else if($page == 'terminos'){
				$page_name = "terminos";
				$page_data = (object) ["title" => "Legal App | Términos y condiciones", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
				include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "terminos.php");
			}
			else{
				$page_name = "login";
				$page_data = (object) ["title" => "Iniciar sesión | LegalBo", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
				include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "login.php");
			}
			
		}
		else{
			$verToken = "valid";
			// $verToken = getServiceData("getTokendeSesionEstado",$current_user->id,$current_user->db_id, [$u_type]);
			if($verToken == "valid"){
				if($page == 'home'){
					$page_name = "home";
					$page_data = (object) ["title" => "Bienvenido | Legal App", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "home.php");
				}
				else if($page == 'homepro'){
					$page_name = "homepro";
					$page_data = (object) ["title" => "Bienvenido | Legal App", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "homepro.php");
				}
				else if($page == 'user'){
					$page_name = "user";
					$page_data = (object) ["title" => "Legal App | Perfil de profesional", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "user.php");
				}
				else if($page == 'abogado'){
					$page_name = "abogado";
					$page_data = (object) ["title" => "Legal App | Perfil de profesional", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "abogado.php");
				}
				else if($page == 'profesional'){
					$page_name = "profesional";
					$page_data = (object) ["title" => "Legal App | Perfil de profesional", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "profesional.php");
				}
				else if($page == 'cuenta'){
					$page_name = "cuenta";
					$page_data = (object) ["title" => "Legal App | Perfil de profesional", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "cuenta.php");
				}
				else if($page == 'wallet'){
					$page_name = "wallet";
					$page_data = (object) ["title" => "Legal App | Perfil de profesional", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "wallet.php");
				}
				else if($page == 'preguntas'){
					$page_name = "preguntas";
					$page_data = (object) ["title" => "Legal App | Preguntas", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "preguntas.php");
				}
				else if($page == 'pregunta'){
					$page_name = "pregunta";
					$page_data = (object) ["title" => "Legal App | Preguntas", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "pregunta.php");
				}
				else if($page == 'casos'){
					$page_name = "casos";
					$page_data = (object) ["title" => "Legal App | Preguntas", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "casos.php");
				}
				else if($page == 'caso'){
					$page_name = "caso";
					$page_data = (object) ["title" => "Legal App | Preguntas", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "caso.php");
				}
				else if($page == 'pasantia'){
					$page_name = "pasantia";
					$page_data = (object) ["title" => "Legal App | Preguntas", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "pasantia.php");
				}
				else if($page == 'favoritos'){
					$page_name = "favoritos";
					$page_data = (object) ["title" => "Legal App | Abogados Favoritos", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "favoritos.php");
				}else if($page == 'mapa'){
					$page_name = "mapa";
					$page_data = (object) ["title" => "Legal App | Abogados Favoritos", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "mapa.php");
				}else if($page == 'examenes'){
					$page_name = "examenes";
					$page_data = (object) ["title" => "Legal App | Examenes", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "examenes.php");
				}else if($page == 'archivos'){
					$page_name = "archivos";
					$page_data = (object) ["title" => "Legal App | Archivos", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "archivos.php");
				}else if($page == 'planes'){
					$page_name = "planes";
					$page_data = (object) ["title" => "Legal App | Eligue tu plan", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "planes.php");
				}else{
					$page_name = "logout";
					$page_data = (object) ["title" => "Iniciar sesión | Legal App", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
					include_once(__DIR__ . DIRECTORY_SEPARATOR . "actions" . DIRECTORY_SEPARATOR . "a_logout.php");
					
				}
			}else{

				$page_name = "logout";
				$page_data = (object) ["title" => "Cerrar Sesión | LegalBo", "description" => "Tu app legal",	"url" => $page_url, "image" => "/resources/img/logo-l.jpg", "autor" => "LegalBo"];
				include_once(__DIR__ . DIRECTORY_SEPARATOR . "actions" . DIRECTORY_SEPARATOR . "a_logout.php");
			}
		}

}
?>