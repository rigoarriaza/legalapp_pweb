<?php
header('Access-Control-Allow-Origin: *');

if( ! isset($_SESSION)){
	session_start();
}

if ( ! defined("actionPATH")) {
	$actionPATH = realpath(__DIR__);
	define("actionPATH", $actionPATH);
}

include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "config.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "cConsumo.php");

if($_REQUEST){
	$data = isset($_REQUEST["password"]) ? $_REQUEST["password"] : false;
    if($data !== false){
        $oConsumo 	= new Consumo();
        
        $body = array(
            "token_session" => $_SESSION['lBo']['currentUserID'],
            "idusuarioapp" => $_SESSION['lBo']['u_Data']->iduserapp,
            "password" => $data
        );

        $tipo = isset($_REQUEST["tipo"]) ? $_REQUEST["tipo"] : false;
        if($tipo == 1){
            $url = PATH."setPasswordUsuario";
        }else{
            $url = PATH."setPasswordUsuario";
        }

        $body 	= json_encode($body);
        
        $result = $oConsumo->postConsumo($url,$body);
        $objt 	= json_decode($result); 
        
        if($objt->errorCode == 0){
            $data = $objt->msg;
            echo 'OK';
        }else if($objt->errorCode == 1){
            echo "error_already";
        }else if($objt->errorCode == 2){
            echo "error_datos_recovery";
        }else{
            echo "error_ws";	
        }
    }
}

?>