<?php
if( ! headers_sent()) {
	header('Access-Control-Allow-Origin: *');
}

if( ! isset($_SESSION)){
	session_start();
}

if ( ! defined("actionPATH")) {
	$actionPATH = realpath(__DIR__);
	define("actionPATH", $actionPATH);
}

if ( ! defined("rootPATH")) {
	$rootPATH = realpath(__DIR__ . '/..');
	define("rootPATH", $rootPATH);
}

include_once(rootPATH . DIRECTORY_SEPARATOR . "global_variables.php");

include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "config.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "cConsumo.php");

function formatDate($oDate){
	date_default_timezone_set('America/La_Paz'); // CDT
	$months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
	$today 	= date('Y-m-d');
	$fToday	= date('Y-m-d H:i:s');
	$year 	= date('Y');
	$eDate 	= explode(" ", $oDate);
	$date 	= date('Y-m-d',strtotime($eDate[0]));
	if($today > $date){
		$lDate = explode("-", $date);
		$oMonth = ((int) $lDate[1]) - 1;
		$fDate = $months[$oMonth] . " " . $lDate[2];	
		$fDate .= ", " . $lDate[0];
	}else{
		$t = new DateTime($fToday);
		$s = new DateTime($oDate);
		$fecha = $t->diff($s);
		$tiempo = "Hace ";
		if($fecha->h > 0){
			$tiempo .= $fecha->h;
			if($fecha->h == 1)
				$tiempo .= " hora, ";
			else
				$tiempo .= " horas, ";
		}
		if($fecha->i > 0){
			$tiempo .= $fecha->i;
			if($fecha->i == 1)
				$tiempo .= " minuto";
			else
				$tiempo .= " minutos";
		}
		else if($fecha->i == 0)
			$tiempo .= $fecha->s." segundos";
		
		$fDate = $tiempo;
	}

	return $fDate;
}

function transformDate($oDate){
	date_default_timezone_set('America/La_Paz'); // CDT
	$months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
	$eDate 	= explode(" ", $oDate);
	$date 	= date('Y-m-d',strtotime($eDate[0]));
	
	$lDate = explode("-", $date);
	$oMonth = ((int) $lDate[1]) - 1;
	$fDate = $months[$oMonth] . " " . $lDate[2];	
	$fDate .= ", " . $lDate[0];
	
	return $fDate;
}

function getUserPhoto($fileURL){
	$photo = $fileURL;
	if( ! isset($fileURL) || empty($fileURL) || ($fileURL=='http://legalbo.com/legalapp/IMGUSERS/') ){
		$photo = $GLOBALS["page_url"] . '/assets/default_user.png';
	}else{
		/*if(get_http_response_code($photo) != "200"){
			$photo = $GLOBALS["page_url"] . '/resources/img/default_user.png';
		}*/
	}
	
	return $photo;
}

function get_http_response_code($domain1) {
	$headers = get_headers($domain1);
	return substr($headers[0], 9, 3);
}

function strToHex($string){
    $hex = '';
    for ($i=0; $i<strlen($string); $i++){
        $ord = ord($string[$i]);
        $hexCode = dechex($ord);
        $hex .= substr('0'.$hexCode, -2);
    }
    return strToUpper($hex);
}
function hexToStr($hex){
    $string='';
    for ($i=0; $i < strlen($hex)-1; $i+=2){
        $string .= chr(hexdec($hex[$i].$hex[$i+1]));
    }
    return $string;
}

function getEstadoUsuarioPro(){
	return "1";
}

function getEstadoClase($estado){
	if($estado == 0){
		return 'cancelada';
	}else if($estado == 1){
		return 'porpagar';
	}else if($estado == 2){
		return 'porresponder';
	}else if($estado == 3){
		return 'SinRespuesta';
	}else if($estado == 4){
		return 'Recibiendo';
	}else if($estado == 5){
		return 'contestada';
	}else if($estado == 6){
		return 'porresponder';
	}else if($estado == 7){
		return 'contestada';
	}
}
function getEstado($estado){
	if($estado == 0){
		return 'Cancelada';
	}else if($estado == 1){
		return 'Por pagar';
	}else if($estado == 2){
		return 'Por responderse';
	}else if($estado == 3){
		return 'Sin Respuesta';
	}else if($estado == 4){
		return 'Recibiendo respuesta';
	}else if($estado == 5){
		return 'Contestada';
	}else if($estado == 6){
		return 'Por responderse';
	}else if($estado == 7){
		return 'Respondida';
	}
}
function getEstadoClaseCot($estado){
	if($estado == 0){
		return 'porresponder';
	}else if($estado == 1){
		return 'contestada';
	}else if($estado == 2){
		return 'cancelada';
	}
}
function getEstadoCot($estado){
	if($estado == 0){
		return 'Por responderse';
	}else if($estado == 1){
		return 'Contestada';
	}else if($estado == 2){
		return 'Rechazada';
	}
}

function getRandom(){
	if(!isset($_SESSION['lBo']['random']) || $_SESSION['lBo']['random'] == NULL){
		$_SESSION['lBo']['random'] 	= rand(1,100);
		return intval($_SESSION['lBo']['random']);
	}else{
		return intval($_SESSION['lBo']['random']);
	}
}

function getServiceData($service, $token = null, $uid = null, $params = null){
	$data = false;
	$url = PATH.$service;

	if( ! is_null($uid)){
		$url .= "/" . $uid;
	}
	if( ! is_null($token)){
		$url .= "/" . $token;
	}
	if( ! is_null($params)){
		$params = join("/",$params);
		$url = $url . "/" . $params;
	}

	// var_dump($url);

	$oConsumo 	= new Consumo();
	$result = $oConsumo->getConsumo($url);
	// var_dump($result);
	$obj 	= json_decode($result);

	
	if($obj->errorCode == 0){
		$data = $obj->msg;
	}else if($obj->errorCode == 3){
		$data = "no_data";
	}else{
		if($obj->errorCode == 12 && $service == "getTokendeSesionEstado"){
			$data = "valid";
		}else if($obj->errorCode == 13 && $service == "getTokendeSesionEstado"){
			$data = "invalid";
		}
	}
	return $data;
}

function getServiceDataCommun($service, $params = null){
	$data = false;
	$url = PATH.$service;

	if( ! is_null($params)){
		$params = join("/",$params);
		$url = $url . "/" . $params;
	}

	// var_dump($url);

	$oConsumo 	= new Consumo();
	$result = $oConsumo->getConsumo($url);
	// var_dump($result);
	$obj 	= json_decode($result);

	
	if($obj->errorCode == 0){
		$data = $obj->msg;
	}else if($obj->errorCode == 3){
		$data = "no_data";
	}else if($obj->errorCode == 2){
		$data = "no_data";
	}else{
		if($obj->errorCode == 12 && $service == "getTokendeSesionEstado"){
			$data = "valid";
		}else if($obj->errorCode == 13 && $service == "getTokendeSesionEstado"){
			$data = "invalid";
		}
	}
	return $data;
}
?>