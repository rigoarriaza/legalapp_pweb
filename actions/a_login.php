<?php
header('Access-Control-Allow-Origin: *');

if( ! isset($_SESSION)){
	session_start();
}

if ( ! defined("actionPATH")) {
	$actionPATH = realpath(__DIR__);
	define("actionPATH", $actionPATH);
}

include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "config.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "cConsumo.php");

if($_REQUEST){
	$data = isset($_REQUEST["data"]) ? $_REQUEST["data"] : false;
		if($data !== false){
			$data = 	json_decode(urldecode($data));

			$tipo = isset($_REQUEST["tipo"]) ? $_REQUEST["tipo"] : false;

			if( $tipo == 1 && (!isset($data->email) || $data->email == "")){
				echo "error_email";
			}else if( $tipo == 1 && (!isset($data->password) || $data->password == "") ){
				echo "error_pass";
			}else if( $tipo == 2 && (!isset($data->email2) || $data->email2 == "")){
				echo "error_email";
			}else if( $tipo == 2 && (!isset($data->password2) || $data->password2 == "") ){
				echo "error_pass";
			}else{
				$oConsumo 	= new Consumo();
				$tipo = isset($_REQUEST["tipo"]) ? $_REQUEST["tipo"] : false;

				// print_r($tipo);

				if($tipo == "1"){
					$selectedType = 1;
					$url = PATH."setLoginUsuarioApp";
				
					$body = array(
						"correo" => $data->email,
						"password" => $data->password,
						"tipologin" => 1
					);
				}else{
					$selectedType = 2;
					$url = PATH."setLoginProfesionalApp";
				
					$body = array(
						"correo" => $data->email2,
						"password" => $data->password2
					);
				}
				

				
				$body 	= json_encode($body);
				$result = $oConsumo->postConsumo($url,$body);
				$objt 	= json_decode($result); 
				
				if($objt->errorCode == 0){
					$data = $objt->msg;
					$_SESSION['lBo']['currentUserID'] 	= $data->token_session;
					$_SESSION['lBo']['userType']		= $selectedType;
					$_SESSION['lBo']['u_Data'] 			= $data;
					echo "OK";
				}else if($objt->errorCode == 3){
					echo "error_nodata";
				}else if($objt->errorCode == 2){
					echo "error_ws";
				}else{
					echo "error_ws";	
				}
			}
		}else{
			echo "nodata ";
		}
}else{
	echo "no requ";
}
?>