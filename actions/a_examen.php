<?php
header('Access-Control-Allow-Origin: *');

if( ! isset($_SESSION)){
	session_start();
}

if ( ! defined("actionPATH")) {
	$actionPATH = realpath(__DIR__);
	define("actionPATH", $actionPATH);
}
if ( ! defined("rootPATH")) {
	$rootPATH = realpath(__DIR__ . '/..');
	define("rootPATH", $rootPATH);
}

include_once(rootPATH . DIRECTORY_SEPARATOR . "global_variables.php");

include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "config.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "cConsumo.php");

if($_REQUEST){
	$data = isset($_REQUEST["data"]) ? $_REQUEST["data"] : false;
		if($data !== false){
			$data = 	json_decode(urldecode($data));
			if(!isset($data->title) || empty($data->title)){
				echo "error_title";
			}else if(!isset($data->description) || empty($data->description)){
				echo "error_description";
			}else if(!isset($data->start_date) || empty($data->start_date)){
                echo "error_startdate";
			}else if(!isset($data->end_date) || empty($data->end_date)){
                echo "error_endate";
			}else{
				date_default_timezone_set('America/La_Paz'); // CDT
				$s_date = \DateTime::createFromFormat('D M d Y H:i:s e+', $data->start_date);
				$e_date = \DateTime::createFromFormat('D M d Y H:i:s e+', $data->end_date);
				
				$now = date('Y-m-d');
				$start_date 	= 	$s_date->setTimezone(new DateTimeZone($timezone));
				$end_date 		= 	$e_date->setTimezone(new DateTimeZone($timezone));

				$start_date 	= $start_date->format("Y-m-d");
				$end_date 		= $end_date->format("Y-m-d");

				if(strtotime($start_date) > strtotime($end_date)){
					echo "error_endate_";
					// var_dump([$start_date, $end_date,$start_date > $end_date]);
				}else if(strtotime($start_date) < strtotime($now)){
					echo "error_startdate_";
				}else{
					$oConsumo 	= new Consumo();
					$url = PATH."setCotizacionUsuarioapp";
					
					$body = array(
						"token_session" => $_SESSION['lBo']['currentUserID'],
						"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
						"c_titulo" => $data->title,
						"c_descripcion" => $data->description,
						"idprofesional" => $data->ask_private_user,
						"c_iniciof" => $start_date,
						"c_finf" => $end_date,
						"id_nivelp"=> $data->priority
					);

					$body 	= json_encode($body);
					
					$result = $oConsumo->postConsumo($url,$body);
					$objt 	= json_decode($result); 
					
					if($objt->errorCode == 0){
						$data = $objt->msg;
						echo 'OK';
					}else if($objt->errorCode == 1){
						echo "error_already";
					}else if($objt->errorCode == 2){
						echo "error_datos";
					}else{
						echo "error_ws";	
					}
				}
			}
		}
}

?>