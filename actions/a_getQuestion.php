<?php
header('Access-Control-Allow-Origin: *');

if( ! isset($_SESSION)){
	session_start();
}

if ( ! defined("actionPATH")) {
	$actionPATH = realpath(__DIR__);
	define("actionPATH", $actionPATH);
}

if ( ! defined("elementPATH")) {
	$elementPATH = realpath(__DIR__ . '/../elements');
	define("elementPATH", $elementPATH);
}

include_once(actionPATH . DIRECTORY_SEPARATOR . "a_getdata.php");

if($_REQUEST){
    $data = isset($_REQUEST["data"]) ? $_REQUEST["data"] : false;
    
    if($data !== false){
        $data = 	json_decode(urldecode($data));
        $oConsumo 	= new Consumo();

        $questionID = $data->questionID;
        $questionType = $data->questionType;

        if($_SESSION['lBo']['userType'] == "normal"){
            if($questionType == "express"){
                $u_question = getServiceData('getDetalleExpressUsuarioApp', $userData->token_session, $userData->iduserapp, (array)[$questionID]);	
            }if($questionType == "private"){
               $d_question = getServiceData('getDetallePreguntaDirectaUApp', $userData->token_session, $userData->iduserapp, (array)[$questionID]);
            }
            
        }else if($_SESSION['lBo']['userType'] == "legal"){
            if($questionType == "express"){
               $u_question = getServiceData('getDetallePreguntaContestada', $current_user->id, $current_user->userData->idprofesional, [$questionID]);
            }if($questionType == "private"){
                $d_question = getServiceData('getDetallePreguntaDirectaProfesional', $current_user->id, $current_user->userData->idprofesional, [$questionID]);
                $nolimit = true;
            }
        }

        include_once(elementPATH . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "case.php");
    }
}

?>