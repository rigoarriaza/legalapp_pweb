<?php
header('Access-Control-Allow-Origin: *');

if( ! isset($_SESSION)){
	session_start();
}

if ( ! defined("actionPATH")) {
	$actionPATH = realpath(__DIR__);
	define("actionPATH", $actionPATH);
}

include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "config.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "cConsumo.php");

if($_REQUEST){
	$data = isset($_REQUEST["data"]) ? $_REQUEST["data"] : false;
		if($data !== false){
			$data = 	json_decode(urldecode($data));
			if(!isset($data->title) || empty($data->title)){
				echo "error_title";
			}else if(!isset($data->detalle) || empty($data->detalle)){
				echo "error_description";
			}else{
				
				if(!isset($data->anonimo) || empty($data->anonimo) || ($data->anonimo == false)){
					$privacidad = 0;
				}else{
					$privacidad = 1;
				}
				$oConsumo 	= new Consumo();
				$tipo = isset($_REQUEST["tipo"]) ? $_REQUEST["tipo"] : false;

				if( $tipo == 'express'){
					$url = PATH."setPreguntaExpress";

					$body = array(
						"token_session" => $_SESSION['lBo']['currentUserID'],
						"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
						"p_titulo" => $data->title,
						"p_contenido" => $data->detalle,
						"p_privacidad" => $privacidad
					);

				}else if( $tipo == 'expressm'){
					$url = PATH."setModificarExpress";

					$body = array(
						"token_session" => $_SESSION['lBo']['currentUserID'],
						"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
						"p_titulo" => $data->title,
						"p_contenido" => $data->detalle,
						"p_privacidad" => $privacidad,
						"idpreguntae" => $data->id
					);

				}else if( $tipo == 'directa'){
					$url = PATH."setPreguntaDirectaUsuarioApp";

					$body = array(
						"token_session" => $_SESSION['lBo']['currentUserID'],
						"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
						"pd_titulo" => $data->title,
						"pd_descripcion" => $data->detalle,
						"idprofesional" => $data->idprofesional,
						"privacidad" => $privacidad
					);

				}else if( $tipo == 'directam'){
					$url = PATH."setModificarPreguntaDirecta";

					$body = array(
						"token_session" => $_SESSION['lBo']['currentUserID'],
						"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
						"pd_titulo" => $data->title,
						"pd_descripcion" => $data->detalle,
						"idprofesional" => $data->idprofesional,
						"privacidad" => $data->anonimo,
						"idpdirecta" => $data->id
					);

				}else{
					$url = PATH."setCotizacionUsuarioapp";

					$body = array(
						"token_session" => $_SESSION['lBo']['currentUserID'],
						"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
						"c_titulo" => $data->title,
						"c_descripcion" => $data->detalle,
						"idprofesional" => $data->idprofesional,
						"privacidad" => $privacidad,
						"id_nivelp" => $data->prioridad,
						"c_iniciof" => $data->datestart,
						"c_finf" => $data->dateend,
						
					);
				}

				// var_dump($body);

				$body 	= json_encode($body);

				$result = $oConsumo->postConsumo($url,$body);
				$objt 	= json_decode($result); 
				
				if($objt->errorCode == 0){
					$data = $objt->msg;
					if($data == ""){
						echo "OK";
					}else{
						echo $data;
					}
					// echo $data;
				}else if($objt->errorCode == 1){
					echo "error_already";
				}else if($objt->errorCode == 2){
					echo "error_datos";
				}else{
					echo "error_ws";	
				}
			
			}
		}
}

?>