<?php
header('Access-Control-Allow-Origin: *');

if( ! isset($_SESSION)){
	session_start();
}

if ( ! defined("actionPATH")) {
	$actionPATH = realpath(__DIR__);
	define("actionPATH", $actionPATH);
}

include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "config.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "cConsumo.php");

if($_REQUEST){
	$data = isset($_REQUEST["data"]) ? $_REQUEST["data"] : false;
		if($data !== false){
			$data = 	json_decode(urldecode($data));

			$tipo = isset($_REQUEST["tipo"]) ? $_REQUEST["tipo"] : false;

			if(  (!isset($data->u_nombre) || $data->u_nombre == "")){
				echo "error_name";
			}else if( (!isset($data->u_apellido) || $data->u_apellido == "") ){
				echo "error_lastname";
			}else if(  (!isset($data->u_celular) || $data->u_celular == "") ){
				echo "error_celular";
			}else if( (!isset($data->u_carnet) || $data->u_carnet == "") ){
				echo "error_carnet";
			}else if( $tipo == 2 && (!isset($data->u_valorpregunta) || $data->u_valorpregunta == "0" || $data->u_valorpregunta < "35"  || $data->u_valorpregunta > "700" || $data->u_valorpregunta == "")){
				echo "error_valorpregunta";
			}else{
				$oConsumo 	= new Consumo();
					
					$d_nombre = $data->u_nombre;
					$d_apellido = $data->u_apellido;
					$d_celular = $data->u_celular;
					$d_carnet = $data->u_carnet;
			
				if($tipo == "1"){
					$selectedType = 1;
					$url = PATH."setInformacionUsuarioApp";
					
                    
					$body = array(
                        "token_session" => $_SESSION['lBo']['currentUserID'],
                        "iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
						"u_nombre" => $data->u_nombre,
						"u_apellido" => $data->u_apellido,
						"u_celular" => $data->u_celular,
						"u_carnet" => $data->u_carnet
					);
				}else{
					$selectedType = 2;
					$url = PATH."setModificarPerfilProfesional";
				
					$body = array(
                        "token_session" => $_SESSION['lBo']['currentUserID'],
                        "idprofesional" => $_SESSION['lBo']['u_Data']->idprofesional,
						"u_nombre" => $data->u_nombre,
						"u_apellido" => $data->u_apellido,
						"u_celular" => $data->u_celular,
						"u_carnet" => $data->u_carnet,
						"u_descripcion" => $data->u_descripcion,
						"u_valorpregunta" => $data->u_valorpregunta,
						"u_numero_registro" => $data->u_numero_registro
					);

					$esp = isset($_REQUEST["esp"]) ? $_REQUEST["esp"] : false;
					// var_dump($esp);
					$esp = 	json_decode(urldecode($esp));

					$especialidades = array();
					foreach($esp as $key => $id){
						$item = array(
							"especialidad_id" => $id->value,
							"principal" => 0
						);
						array_push($especialidades, $item);
					}
					
					$bodyesp = array(
                        "token_session" => $_SESSION['lBo']['currentUserID'],
                        "idprofesional" => $_SESSION['lBo']['u_Data']->idprofesional,
						"especialidades" => $especialidades
					);
				}
				

				
				$body 	= json_encode($body);
				$result = $oConsumo->postConsumo($url,$body);
				$objt 	= json_decode($result); 
				
				if($objt->errorCode == 0){
					$data = $objt->msg;
					$userData 	 = isset($_SESSION['lBo']['u_Data']) ? $_SESSION['lBo']['u_Data'] : null;
                    $userData->u_nombre = $d_nombre;
                    $userData->u_apellido = $d_apellido;
                    $userData->u_celular = $d_celular;
                    $userData->u_carnet = $d_carnet;
                    
					$_SESSION['lBo']['u_Data'] 		= $userData;
		
					if($tipo == "2"){
						$url = PATH."setModificarEspecialidadProfesional";
						$bodyesp 	= json_encode($bodyesp);
						$result = $oConsumo->postConsumo($url,$bodyesp);
						
					}
					echo "OK";

				}else if($objt->errorCode == 3){
					echo "error_nodata";
				}else if($objt->errorCode == 2){
					echo "error_ws";
				}else{
					echo "error_ws";	
				}
			}
		}else{
			echo "nodata ";
		}
}else{
	echo "no requ";
}
?>