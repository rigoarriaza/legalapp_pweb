<?php
class Consumo{
	
	function getConsumo($url){
	
		$headers = array( 		
			'Content-Type: application/json'
		);

		// Open connection
		$ch = curl_init();
		
		if (FALSE === $ch)
		throw new Exception('failed to initialize');
		
		
		// Set the url, number of POST vars, POST data
		curl_setopt( $ch, CURLOPT_URL, $url );

		curl_setopt( $ch, CURLOPT_HTTPGET, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		// Execute post
		$result = curl_exec($ch);
		
		if (FALSE === $result)
		 throw new Exception(curl_error($ch), curl_errno($ch));
		// Close connection
		curl_close($ch);
		return $result;
	}
	
	function postConsumo($url,$body){
	
		$headers = array( 		
			'Content-Type: application/json'
		);

		// Open connection
		$ch = curl_init();
		
		if (FALSE === $ch)
		throw new Exception('failed to initialize');
		
		
		// Set the url, number of POST vars, POST data
		curl_setopt( $ch, CURLOPT_URL, $url );

		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS,  $body  );
		// Execute post
		$result = curl_exec($ch);
		
		if (FALSE === $result)
		 throw new Exception(curl_error($ch), curl_errno($ch));
		// Close connection
		curl_close($ch);
		return $result;
	}
	
	function putConsumo($url,$body){
	
		$headers = array( 		
			'Content-Type: application/json'
		);

		// Open connection
		$ch = curl_init();
		
		if (FALSE === $ch)
		throw new Exception('failed to initialize');
		
		
		// Set the url, number of POST vars, POST data
		curl_setopt( $ch, CURLOPT_URL, $url );

		curl_setopt( $ch, CURLOPT_PUT, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS,  $body  );
		// Execute post
		$result = curl_exec($ch);
		
		if (FALSE === $result)
		 throw new Exception(curl_error($ch), curl_errno($ch));
		// Close connection
		curl_close($ch);
		return $result;
	}
}
?>