<?php
header('Access-Control-Allow-Origin: *');

if( ! isset($_SESSION)){
	session_start();
}

if ( ! defined("actionPATH")) {
	$actionPATH = realpath(__DIR__);
	define("actionPATH", $actionPATH);
}

include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "config.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "cConsumo.php");

if($_REQUEST){
	$tipo = isset($_REQUEST["tipo"]) ? $_REQUEST["tipo"] : false;

	if($tipo == "express"){
		$data = isset($_REQUEST["data"]) ? $_REQUEST["data"] : false;
			if($data !== false){
				$data = 	json_decode(urldecode($data));
				if(!isset($data->idpregunta) || empty($data->idpregunta)){
					echo "error_value";
				}else{
				
					$oConsumo 	= new Consumo();
					
					$accion = isset($_REQUEST["accion"]) ? $_REQUEST["accion"] : false;
				
					if($accion == 'cancelar'){
						$url = PATH."setCancelarExpress";
						$body = array(
							"token_session" => $_SESSION['lBo']['currentUserID'],
							"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
							"idpreguntae" => $data->idpregunta
						);
					}else if($accion == 'pagar'){
						$url = PATH."setPaymentPreguntaExpress";
						$body = array(
							"token_session" => $_SESSION['lBo']['currentUserID'],
							"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
							"idpreguntae" => $data->idpregunta
						);
					}else if($accion == 'responder'){
						$url = PATH."setRespuestaPreguntaExpressUApp";
						$body = array(
							"token_session" => $_SESSION['lBo']['currentUserID'],
							"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
							"idpreguntae" => $data->idpregunta,
							"idprofesional" => $data->idprofesional,
							"r_detalle" => $data->detalle
						);
					}else if($accion == 'calificar'){
						$url = PATH."setCalificarConsultaExpress";
						$body = array(
							"token_session" => $_SESSION['lBo']['currentUserID'],
							"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
							"idpregunta" => $data->idpregunta,
							"calificacion" => $data->calificacion,
							"comentario" => $data->comentario
						);
					}
					
										
					// var_dump($body);

					$body 	= json_encode($body);
					
					
					$result = $oConsumo->postConsumo($url,$body);
					$objt 	= json_decode($result); 
					
					if($objt->errorCode == 0){
						echo 'OK';
					}else{
						echo "error_ws";	
					}
				
			}
		}
	}else if($tipo == "directa"){
		$data = isset($_REQUEST["data"]) ? $_REQUEST["data"] : false;
			if($data !== false){
				$data = 	json_decode(urldecode($data));
				if(!isset($data->idpregunta) || empty($data->idpregunta)){
					echo "error_value";
				}else{

					$oConsumo 	= new Consumo();
					
					$accion = isset($_REQUEST["accion"]) ? $_REQUEST["accion"] : false;
				
					if($accion == 'cancelar'){
						$url = PATH."setCancelarPreguntaDirecta";
						$body = array(
							"token_session" => $_SESSION['lBo']['currentUserID'],
							"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
							"idpdirecta" => $data->idpregunta
						);
					}else if($accion == 'pagar'){
						$url = PATH."setPaymentPreguntaDirecta";
						$body = array(
							"token_session" => $_SESSION['lBo']['currentUserID'],
							"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
							"idpreguntad" => $data->idpregunta,
							"idprofesional" => $data->idprofesional
						);
					}else if($accion == 'responder'){
						$url = PATH."setRespuestaPreguntaDirectaUApp";
						$body = array(
							"token_session" => $_SESSION['lBo']['currentUserID'],
							"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
							"r_detalle" => $data->detalle,
							"idpdirecta" => $data->idpregunta,
							"idprofesional" => $data->idprofesional
						);
					}
					
					
					

					
					// var_dump($body);

					$body 	= json_encode($body);
					
					
					$result = $oConsumo->postConsumo($url,$body);
					$objt 	= json_decode($result); 
					
					if($objt->errorCode == 0){
						echo 'OK';
					}else if($objt->errorCode == 15){
						echo 'error_saldo';
					}else{
						echo "error_ws";	
					}
				
			}
		}
	}

}

?>