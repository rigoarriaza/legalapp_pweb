<?php
header('Access-Control-Allow-Origin: *');
if( ! isset($_SESSION)){
	session_start();
}
if ( ! defined("actionPATH")) {
	$actionPATH = realpath(__DIR__);
	define("actionPATH", $actionPATH);
}

include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "config.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "cConsumo.php");

if($_REQUEST){
	$data = isset($_REQUEST["data"]) ? $_REQUEST["data"] : false;
		if($data !== false){
			$data = 	json_decode(urldecode($data));
			if(!isset($data->description) || empty($data->description)){
				echo "error_description";
			}else{
				$oConsumo 	= new Consumo();
				$tipo = isset($_REQUEST["tipo"]) ? $_REQUEST["tipo"] : false;
				if(isset($tipo) && $tipo == 'express'){
					$url = PATH."setRespuestapreguntaExpress";
					$body = array(
						"token_session" => $_SESSION['lBo']['currentUserID'],
						"idprofesional" => $_SESSION['lBo']['u_Data']->idprofesional,
						"respuesta"		=> $data->description,
						"idpreguntae"  => $data->id,
						"iduserapp" => $data->iduser
					);
				}else if(isset($tipo) && $tipo == 'directa'){
					$url = PATH."setRespuestaPreguntaDirectaPApp";
					$body = array(
						"token_session" => $_SESSION['lBo']['currentUserID'],
						"idprofesional" => $_SESSION['lBo']['u_Data']->idprofesional,
						"r_detalle"		=> $data->description,
						"idpdirecta"  => $data->id,
						"iduserapp" => $data->iduser
					);
				}else if(isset($tipo) && $tipo == 'cotizacion'){
					$url = PATH."setRespuestaCotizacion";
					$body = array(
						"token_session" => $_SESSION['lBo']['currentUserID'],
						"idprofesional" => $_SESSION['lBo']['u_Data']->idprofesional,
						"respuesta"		=> $data->description,
						"idcotizacion"  => $data->id,
						"estado" => '1',
						"precio" => $data->precio
					);
				}else if(filter_var($data->private_question, FILTER_VALIDATE_BOOLEAN) == true){
					if($_SESSION["lBo"]["userType"] == "legal"){
						$url = PATH."setRespuestaPreguntaDirectaPApp";
						$body = array(
							"token_session" => $_SESSION['lBo']['currentUserID'],
							"idprofesional" => $_SESSION['lBo']['u_Data']->idprofesional,
							"r_detalle"		=> $data->description,
							"idpdirecta"  => $data->user_question_id
						);
					}else if($_SESSION["lBo"]["userType"] == "normal"){
						$url = PATH."setRespuestaPreguntaDirectaUApp";
						$body = array(
							"token_session" => $_SESSION['lBo']['currentUserID'],
							"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
							"r_detalle"		=> $data->description,
							"idpdirecta"  => $data->user_question_id
						);
					}
				}
				// var_dump($body);
				
				$body 	= json_encode($body);
				$result = $oConsumo->postConsumo($url,$body);
				// var_dump($result);
				$objt 	= json_decode($result);
				if($objt->errorCode == 0){
					$data = $objt->msg;
					echo 'OK';
				}else if($objt->errorCode == 4){
					echo "error_already";
				}else if($objt->errorCode == 2){
					echo "error_datos";
				}else{
					echo "error_ws";
				}
			}
		}
}
?>