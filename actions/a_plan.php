<?php
header('Access-Control-Allow-Origin: *');

if( ! isset($_SESSION)){
	session_start();
}

if ( ! defined("actionPATH")) {
	$actionPATH = realpath(__DIR__);
	define("actionPATH", $actionPATH);
}

include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "config.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "cConsumo.php");

if($_REQUEST){
	$data = isset($_REQUEST["data"]) ? $_REQUEST["data"] : false;
    if($data !== false){
        $data = 	json_decode(urldecode($data));
        $oConsumo 	= new Consumo();
        

        $body = array(
            "token_session" => $_SESSION['lBo']['currentUserID'],
            "idprofesional" => $_SESSION['lBo']['u_Data']->idprofesional,
            "costo_plan" => $data->costo,
            "id_plan" => $data->idplan,

        );

        $url = PATH."setCompraPlanProfesionalApp";

        $body 	= json_encode($body);
        
        $result = $oConsumo->postConsumo($url,$body);
        $objt 	= json_decode($result);
        if($objt->errorCode == 0){
            echo 'OK';
        }else if($objt->errorCode == 9){
            echo "error_already";
        }else if($objt->errorCode == 2){
            echo "error_datos";
        }else{
            echo "error_ws";	
        }
    }
}

?>