<?php
header('Access-Control-Allow-Origin: *');

if ( ! defined("actionPATH")) {
	$actionPATH = realpath(__DIR__);
	define("actionPATH", $actionPATH);
}

if ( ! isset($elementPATH)) {
	$elementPATH = realpath(__DIR__ . '/../elements');
	define("elementPATH", $elementPATH);
}

include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "config.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "cConsumo.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "a_getdata.php");

if($_REQUEST){
	$data = isset($_REQUEST["data"]) ? $_REQUEST["data"] : false;
		if($data !== false){
			$data = 	json_decode(urldecode($data));
			if(!isset($data->type) || empty($data->type)){
				echo "error_type";
			}else{
				switch ($data->type) {
					case 'favorites':
						$members = (object) ["favorites" => [] ]; 
						$fav_member_list = getServiceData('getAbogadosFavorito', $userData->token_session, $userData->iduserapp, (array)[$data->from, $data->to]);
						if($fav_member_list != false && count($fav_member_list) > 0 ){
							foreach ($fav_member_list as $key => $f_member) {
								$m_photo =   getUserPhoto($f_member->p_foto);
								$member = 	 array ("name" => $f_member->p_nombre . " " . $f_member->p_apellido,
														"id" => $f_member->idprofesional,
														"title"=> (isset($f_member->p_especialidad)) ? $f_member->p_especialidad : "",
														"rate"=> $f_member->calificacion,
														"user_url"=> "$page_url/user/" . $f_member->idprofesional,
														"answers" =>$f_member->n_contestadas,
														"photo"=>$m_photo,
														"favorite" =>1);
								$members->favorites[] = (object) $member;
							}
							include_once(elementPATH . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "favorite_list.php");
						}else{
							echo "error_nodata";
						}
						break;
					case 'express':
						$u_questions_list = getServiceData('getPreguntasExpressPendientes', $userData->token_session, $userData->idprofesional, (array)[$data->from, $data->to]);
						$user_questions = [];
						
						if($u_questions_list != false){
							foreach ($u_questions_list as $key => $u_question) {
								$q = [];
								$q["id"] 		= $u_question->idpreguntae;
								if($u_question->p_privacidad == 0){
									$q["name"] 	= $current_user->name;
									$q["photo"]	= $current_user->photo;
								}else{
									$q["photo"]	= $page_url . '/resources/img/default_user.png';
									$q["name"] 	= "Usuario Anónimo";
								}
								$q["date"]		= formatDate($u_question->p_fecha);
								$q["title"]		= $u_question->p_titulo;
								$q["description"]		= $u_question->p_contenido;
								$q["status"]	= $u_question->p_estado;
								$user_questions[] = (object) ["question" => $q, "answer"=>false];
							}
							include_once(elementPATH . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "user_answers.php");
						}
						break;
					case 'quotationU':
						$user_quotations = getServiceData('getHistorialCotizacionesUapp', $userData->token_session, $userData->iduserapp, (array)[$data->from, $data->to]);
						include_once(elementPATH . DIRECTORY_SEPARATOR . "partials" . DIRECTORY_SEPARATOR . "quotation_box.php");
						break;
					default:
						# code...
						break;
				}
			}
		}
}
?>