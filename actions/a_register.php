<?php
header('Access-Control-Allow-Origin: *');

if( ! isset($_SESSION)){
	session_start();
}

if ( ! defined("actionPATH")) {
	$actionPATH = realpath(__DIR__);
	define("actionPATH", $actionPATH);
}

include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "config.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "cConsumo.php");

if($_REQUEST){
	$data = isset($_REQUEST["data"]) ? $_REQUEST["data"] : false;
		if($data !== false){
			$data = 	json_decode(urldecode($data));

			$tipo = isset($_REQUEST["tipo"]) ? $_REQUEST["tipo"] : false;

			if( $tipo == 1 && (!isset($data->nombre) || empty($data->nombre)) ){
				echo "error_name";
			}else if( $tipo == 1 && (!isset($data->correo3) || empty($data->correo3)) ){
				echo "error_email";
			}else if( $tipo == 1 && (!isset($data->password3) || empty($data->password3)) ){
				echo "error_pass";
			}else if( $tipo == 1 && (!isset($data->terminosuser) || empty($data->terminosuser) || ($data->terminosuser == false)) ){
				echo "error_terms";
			}else if( $tipo == 2 && (!isset($data->nombre2) || empty($data->nombre2)) ){
				echo "error_name";
			}else if( $tipo == 2 && (!isset($data->correo4) || empty($data->correo4)) ){
				echo "error_email";
			}else if( $tipo == 2 && (!isset($data->password4) || empty($data->password4)) ){
				echo "error_pass";
			}elseif( $tipo == 2 && (!isset($data->terminospro) || empty($data->terminospro) || ($data->terminospro == false)) ){
				echo "error_terms";
			}else {
				$oConsumo 	= new Consumo();

				if($tipo == "1"){
					$url = PATH."setRegistroUsuarioApp";
					$body = array(
						"correo" => $data->correo3,
						"password" => $data->password3,
						"nombre" => $data->nombre,
						"apellido" => $data->apellido,
						"celular" => isset($data->celular) ? $data->celular : null,
					);
				}else {
					$url = PATH."setRegistroProfesionalApp";
					$body = array(
						"correo" => $data->correo4,
						"password" => $data->password4,
						"nombre" => $data->nombre2,
						"apellido" => $data->apellido2
					);
				}
				
				$selectedType = $tipo;
				

				
				$body 	= json_encode($body);
				$result = $oConsumo->postConsumo($url,$body);
				$objt 	= json_decode($result); 
				
				if($objt->errorCode == 0){
					$data = $objt->msg;
					echo 'OK';
				}else if($objt->errorCode == 1){
					echo "error_already";
				}else if($objt->errorCode == 2){
					echo "error_datos";
				}else{
					echo "error_ws";	
				}
				
			}
		}
}

?>