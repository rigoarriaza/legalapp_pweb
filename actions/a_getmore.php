<?php
header('Access-Control-Allow-Origin: *');

if( ! isset($_SESSION)){
	session_start();
}

if ( ! defined("actionPATH")) {
	$actionPATH = realpath(__DIR__);
	define("actionPATH", $actionPATH);
}

if ( ! defined("elementPATH")) {
	$elementPATH = realpath(__DIR__ . '/../elements');
	define("elementPATH", $elementPATH);
}

include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "config.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "cConsumo.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "a_getdata.php");

if($_REQUEST){
    $tipo = isset($_REQUEST["tipo"]) ? $_REQUEST["tipo"] : false;
    
    $oConsumo 	= new Consumo();
        
    if($tipo == "loadmore" ){

        $consulta = isset($_REQUEST["consulta"]) ? $_REQUEST["consulta"] : false;
    
        $contador = isset($_REQUEST["valor"]) ? $_REQUEST["valor"] : false;
        // var_dump($consulta);
            
        if($consulta == 0){
            $result = getServiceData('getAbogados', $userData->token_session, $userData->iduserapp, (array)[$contador,10,getRandom()]);	
            foreach ($result as $key => $pro) {
                // echo '<a href="#" class="list-group-item "><img src="" style="width:40px;"/>'.$h_esp->p_titulo.'</a>';
                $m_photo = (empty($pro->p_foto)) ? $page_url . '/assets/default_user.png' : $imageUrl.'/'.$pro->p_foto;
                $urlpro = ($pro->idplan !=0 )? $page_url. '/assets/premium.png' : "";					
                echo '<a class="card" href="'.$page_url.'/abogado/'.$pro->idprofesional.'">'.
                     '   <div class="row no-gutters">'.
                            '<div class="col-auto">'.
                               ' <img src="'.$m_photo.'" class="img-fluid imglist"  alt="">'.
                     '       </div>'.
                     '       <div class="col">'.
                     '           <div class="card-block px-2">'.
                      '              <h4 class="title-list">'.
                      '                  <img  src="'.$urlpro.'" >'.
                                     ' '.$pro->p_nombre.' '.$pro->p_apellido.'<label class="detail-list"><i class="fa fa-star"></i> '.$pro->calificacion.'</label></h4>'.   
                                    '<p class="card-text">'.$pro->p_descripcion.'</p>'.
                                '</div>'.
                            '</div>'.
                       ' </div>'.
                       ' <div class="card-footer w-100 text-muted">'.
                           ' <div class="row">'.
                            '<div class="col-6"><i class="fa fa-comments-o"></i> '.$pro->n_contestadas.' Respuestas</div>'.
                            '<div class="col-6"><i class="fa fa-comment"></i> '.$pro->n_comentarios.' Comentarios</div>'.
                            '</div>'.
                        '</div>'.
                    '</a>';               
            }
        }else if($consulta == 1){
            $result = getServiceData('getHistorialExpressUsuarioApp', $userData->token_session, $userData->iduserapp, (array)[$contador,5]);	
            foreach ($result as $key => $caso) {
                // echo '<a href="#" class="list-group-item "><img src="" style="width:40px;"/>'.$h_esp->p_titulo.'</a>';
    
                echo '     <a class="card" href="pregunta/'.$caso->idpreguntae.'/1">'.
                    '   <div class="row no-gutters">'.
                    '       <div class="col">'.
                    '           <div class="card-block px-2">'.
                    '               <div class="row">'.
                    '                   <div class="col-8">'.
                    '                      <h5 class="title-list">'.$caso->p_titulo .' </h5>'.
                    '                       <label class="detail-list">'.formatDate($caso->p_fecha).'</label>'.
                    '                   </div>'.
                    '                   <div class="col-4" style="padding: 5px;">'.
                    '                     <button type="button" class="btn btn-sm btnlg-l  '.getEstadoClase($caso->p_estado).'">'.
                    '									'.getEstado($caso->p_estado).'</button>'.
                    '                   </div>'.
                    '               </div>'.
                    '           </div>'.
                    '       </div>'.
                    '   </div>'.
                    '</a> ' ;               
            }
        }else if($consulta==2){
            $result = getServiceData('getHistorialExpressUsuarioApp', $userData->token_session, $userData->iduserapp, (array)[$contador,5]);	
        }else{
            $result = getServiceData('getHistorialExpressUsuarioApp', $userData->token_session, $userData->iduserapp, (array)[$contador,5]);	
        }
            

                  
    }
    else if($tipo == "setPayExpress" ){
        
        $url = PATH."setPaymentPreguntaExpress";
        $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : false;
        
		$body = array(
						"token_session" => $_SESSION['lBo']['currentUserID'],
						"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
						"idpreguntae" => $id
		);
				
        $body 	= json_encode($body);

		$result = $oConsumo->postConsumo($url,$body);
		$objt 	= json_decode($result); 
				
		if($objt->errorCode == 0){
					$data = $objt->msg;
					echo "OK";
		}else if($objt->errorCode == 1){
					echo "error_already";
		}else if($objt->errorCode == 2){
					echo "error_datos";
		}else if($objt->errorCode == 15){
					echo "error_saldo";
		}else{
					echo "error_ws";	
		}

    }else if($tipo == "setPayDirecta" ){
        
        $url = PATH."setPaymentPreguntaDirecta";
        $id = isset($_REQUEST["id"]) ? $_REQUEST["id"] : false;
        
		$body = array(
						"token_session" => $_SESSION['lBo']['currentUserID'],
						"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
						"idpreguntad" => $id
		);
				
        $body 	= json_encode($body);

		$result = $oConsumo->postConsumo($url,$body);
		$objt 	= json_decode($result); 
				
		if($objt->errorCode == 0){
					$data = $objt->msg;
					echo "OK";
		}else if($objt->errorCode == 1){
					echo "error_already";
		}else if($objt->errorCode == 2){
					echo "error_datos";
		}else if($objt->errorCode == 15){
					echo "error_saldo";
		}else{
					echo "error_ws";	
		}

    }
}

?>