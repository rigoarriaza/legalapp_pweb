<?php
header('Access-Control-Allow-Origin: *');

if( ! isset($_SESSION)){
	session_start();
}

if ( ! defined("actionPATH")) {
	$actionPATH = realpath(__DIR__);
	define("actionPATH", $actionPATH);
}

include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "config.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "cConsumo.php");

if($_REQUEST){
	$data = isset($_REQUEST["foto"]) ? $_REQUEST["foto"] : false;
    if($data !== false){
        $oConsumo 	= new Consumo();

        $tipo = isset($_REQUEST["tipo"]) ? $_REQUEST["tipo"] : false;
        if($tipo == 1){
            $url = PATH."setModificarFotoUsuario";
            $body = array(
                "token_session" => $_SESSION['lBo']['currentUserID'],
                "iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
                "u_foto" => $data
            );

        }else if($tipo == 2){
            $url = PATH."setModificarFotoProfesional";
            $body = array(
                "token_session" => $_SESSION['lBo']['currentUserID'],
                "idprofesional" => $_SESSION['lBo']['u_Data']->idprofesional,
                "p_foto" => $data
            );
        }else{
            $url = PATH."setImagenRequerimiento";
            $body = array(
                "token_session" => $_SESSION['lBo']['currentUserID'],
                "idprofesional" => $_SESSION['lBo']['u_Data']->idprofesional,
                "documento" => $data
            );
        }

        $body 	= json_encode($body);
        
        $result = $oConsumo->postConsumo($url,$body);
        $objt 	= json_decode($result); 
        
        if($objt->errorCode == 0){
            $data = $objt->msg;
            // var_dump($result);
            if($tipo !=3 ){
                $userData2 	 = isset($_SESSION['lBo']['u_Data']) ? $_SESSION['lBo']['u_Data'] : null;
                $userData2->u_foto = $data;

                $_SESSION['lBo']['u_Data'] 		= $userData2;
            }	
            echo "OK";
        }else if($objt->errorCode == 1){
            echo "error_already";
        }else if($objt->errorCode == 2){
            echo "error_datos_recovery";
        }else{
            echo "error_ws";	
        }
    }
}

?>