<?php
header('Access-Control-Allow-Origin: *');

if( ! isset($_SESSION)){
	session_start();
}

if ( ! defined("actionPATH")) {
	$actionPATH = realpath(__DIR__);
	define("actionPATH", $actionPATH);
}

include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "config.php");
include_once(actionPATH . DIRECTORY_SEPARATOR . "classes" . DIRECTORY_SEPARATOR . "cConsumo.php");

if($_REQUEST){
	$data = isset($_REQUEST["data"]) ? $_REQUEST["data"] : false;
		if($data !== false){
			$data = 	json_decode(urldecode($data));
			if(!isset($data->id) || empty($data->id)){
				echo "error_value";
			}else{
			
				
				$oConsumo 	= new Consumo();
				
				
				$url = PATH."setAbogadoFavorito";

				$body = array(
					"token_session" => $_SESSION['lBo']['currentUserID'],
					"iduserapp" => $_SESSION['lBo']['u_Data']->iduserapp,
                    "idprofesional" => $data->id
				);
				
				$body 	= json_encode($body);
				
				
				$result = $oConsumo->postConsumo($url,$body);
				$objt 	= json_decode($result); 
				
				if($objt->errorCode == 0){
					echo 'OK';
				}else{
					echo "error_ws";	
				}
			
			}
		}
}

?>